<?php

class login extends CI_Controller {
 
    function index() {
       
	  

	   if( $this->session->userdata('isLoggedIn') ) {
            redirect('/main/show_main');
        } else {
            $this->show_login_view(false);
        }
    }

    function login_user() {
        // Create an instance of the user model
        $this->load->model('user');

        // Grab the email and password from the form POST
        $email = $this->input->post('email');
        $pass  = $this->input->post('password');

        //Ensure values exist for email and pass, and validate the user's credentials
        if( $email && $pass && $this->user->validate_user($email,$pass)) {
            // If the user is valid, redirect to the main view
			
            redirect('dashboard');
        } else {
            // Otherwise show the login screen with an error message.
			echo "Login only";
            $this->show_login(true);
        }
    } //end login user
	
	function show_login( $show_error = false ) {
        $data['error'] = "<div class='error col-md-12 col-sm-12 col-xs-12'>Username or password is incorrect !</div>";
        $data['iserror'] = $show_error;

        $this->load->helper('form');
        $this->load->view('login',$data);
    }
    
    function show_login_view( $show_error = false ) {
    	$data['error'] = "<div class='error col-md-12 col-sm-12 col-xs-12'>Username or password is incorrect !</div>";
    	$data['iserror'] = $show_error;
    
    	$this->load->helper('form');
    	$this->load->view('login',$data);
    }

    function logout_user() {
      $this->session->sess_destroy();
	  
      $this->index();
    }

    function showphpinfo() {
        echo phpinfo();
    }

	

}
