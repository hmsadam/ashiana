<?php

class userController extends CI_Controller{

  public function __construct()
  {
    parent::__construct();

    if( !$this->session->userdata('isLoggedIn') ) {
        redirect('/login/show_login');
    }
  }

  
  function createNewUserViewShowForClients() {
    
	$this->load->model('hotelbranches');
	$branches = $this->hotelbranches->getHotelsByUser();
	$username = $this->session->userdata('name');	
	$data['branches'] = $branches;
	 $data['username'] = $username;
	
	
	$db = $this->session->userdata('db');
	$groupid = $this->session->userdata('groupid');
	
	$this->load->model('userroles');
	$roles = $this->userroles->getUserRoles();
	
	$data['roles'] = $roles;
	$data['db'] = $db;
	$data['groupid'] = $groupid;
	//$data['labs'] = $labs;
	
	$data['email'] = $this->session->userdata('email');
    $data['name'] = $this->session->userdata('name');
    
	$this->load->helper('form');
    $this->load->view('createNewUserForClient',$data);
  } //end function create New School
   
   function getUsersToUpdate()
  {
		
		//$groups = array();
		$this->load->model('user');
		$users = $this->user->getAllGroupUsers();
		
		echo json_encode(array('data' => $users));
  } //end function
  
  function getClientUsersToUpdate()
  {
		
		//$groups = array();
		$db = $this->session->userdata('db');
		$this->load->model('user');
		$users = $this->user->getClientUsers($db);
		
		echo json_encode(array('data' => $users));
  } //end function
   
   function getAllClientGroups()
   {
	   $this->load->model('clientgroups');
	$clientGroups = $this->clientgroups->getAllClientGroups();
	echo json_encode($clientGroups);
   } //
   
   
   
   
   
   
   
   
   
   function saveUser() {
    $email = $this->input->post('email');
	$pw = $this->input->post('pw');
	$firstname = $this->input->post('firstname');
	$lastname = $this->input->post('lastname');
	$role = $this->input->post('role');
	$groupid = $this->input->post('groupid');
	$clientdb = $this->input->post('db');
	$isclientuser = $this->input->post('isClientUser');
	$branchid = $this->input->post('branchid');
	
	
	/*$email = "a@a.com";
	$pw = "shayan09";
	$firstname = "Dolat";
	$lastname = "Ali";
	$role = "1";
	$groupid = "1";
	$clientdb = "hmskhandb";
	$isclientuser = "1";*/
	
	
	$this->load->model('user');
      $NewUserId = $this->user->createNewUserInParentDB($email, $pw, $firstname, $lastname, $role, $clientdb, $groupid, $isclientuser);
      
      $this->load->model('usershashotelbranches');
      $rslt = $this->usershashotelbranches->saveHotelUser($NewUserId,  $branchid, $clientdb);
      
      echo json_encode($rslt);
	  /*if($isclientuser){
		  $this->load->model('user');
      $result = $this->user->createNewUserInClientDB($NewUserId, $email, $pw, $firstname, $lastname, $role, $clientdb, $groupid);
	
		echo json_encode("Parent and Child");
	  } else {
		  echo json_encode("Parent Only");
	  }*/
	  
	 
  } //end function
  
  
   function updateUser() {
    $userid = $this->input->post('userid');
	$firstname = $this->input->post('firstname');
	$lastname = $this->input->post('lastname');
    $email = $this->input->post('email');
	$pw = $this->input->post('password');
	$role = $this->input->post('role');
	$clientdb = $this->input->post('db');
	$isClientUser = $this->input->post('isClientUser');
	
	
	/*$email = "a@a.com";
	$pw = "shayan09";
	$firstname = "Dolat";
	$lastname = "Ali";
	$role = "1";
	$groupid = "1";
	$clientdb = "hmskhandb";*/
	
	
	$this->load->model('user');
      $result = $this->user->updateUser($userid, $firstname, $lastname, $email, $pw, $role, $clientdb, $isClientUser);
	  
	 
	
	 echo json_encode($result);
	 
  } //end function
  
  function getRoles()
  {
	  
	  
	  $this->load->model('userroles');
	  $roles = $this->userroles->getUserRoles();
	  echo json_encode($roles);
  }
  //end function
  
  
  function getUsersByRole()
  {
	  $roleId = $this->input->post('role');
	  
	  $this->load->model('user');
	  $users = $this->user->getUsersByRole($roleId);
	  echo json_encode($users);
  }
  
  
 
  
  
  
  } //end class
