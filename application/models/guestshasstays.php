<?php

class guestshasstays extends CI_Model {

  
	
  
  function saveGuestStayRecord($guestid, $guestStayId, $rsvId, $userid, $db)
  {
	$data['guest_guestsid'] = $guestid;
	$data['gueststays_gueststaysid'] = $guestStayId;
	$data['reservation_reservationid'] = $rsvId;
    $data['createdon'] = date("Y-m-d H:i:s" ,time());
    
    $data['createdby'] = $userid; 
    
	
	$this->$db->trans_start();	
	$this->$db->insert('guestshasstays',$data);
	$insert_id = $this->$db->insert_id();
	$this->$db->trans_complete();
	
	if ($this->$db->trans_status() === FALSE) {
    # Something went wrong.
    $this->$db->trans_rollback();
    return false;
	
	} 
	else {
		# Everything is Perfect. 
		# Committing data to the database.
		$this->$db->trans_commit();
		return $insert_id;
	}
  } //end function
  
  function getGuestData($reservationId, $db)
  {
	  $this->$db->trans_start();
	  $this->$db->select('guestshasstaysid, guestsstaysid, reservation_reservationid, guestsid, firstname, lastname, cnic, passportNo, cellNo, landLineNo, email, gender, age, ethnicity, city, district, province, country, isAFamilyMember, guests.guests_guestsid, guestsstaysid, checkindatetime, hotelroomsid, roomno, hotalrooms.noofbeds, hotelfloorsid, floorname, roomcategoriesid, categoryname');
	  
	$this->$db->from('guestshasstays');
	$this->$db->join('guests', 'guests.guestsid = guestshasstays.guest_guestsid','left');
	$this->$db->join('guestsstays', 'guestsstays.guestsstaysid = guestshasstays.gueststays_gueststaysid','left');
	$this->$db->join('hotalrooms', 'hotalrooms.hotelroomsid = guestsstays.hotelrooms_hotelroomsid','left');
	$this->$db->join('hotelfloors', 'hotelfloors.hotelfloorsid = hotalrooms.hotelfloors_hotelfloorsid','left');
	$this->$db->join('roomcategories', 'roomcategories.roomcategoriesid = hotalrooms.roomcategories_roomcategoriesid','left');
	
	$this->$db->where('guestshasstays.reservation_reservationid', $reservationId); 
	
	$result = $this->$db->get()->result_array();
	$this->$db->trans_complete();				  	
		return $result;
  } //end function
  
  function deleteGuest($id, $db)
  {
  	$this->$db->trans_start();
  	$this->$db->where('guest_guestsid', $id);
  	$this->$db->delete('guestshasstays');
  	$this->$db->trans_complete();
  
  
  	return true;
  } //end function
  
  function getGuestStayData($reservationId, $db)
  {
  	$this->$db->trans_start();
  	$this->$db->select('guestsstaysid, checkindatetime, checkoutdatetime');
  	 
  	$this->$db->from('guestshasstays');
  	
  	$this->$db->join('guestsstays', 'guestsstays.guestsstaysid = guestshasstays.gueststays_gueststaysid','left');
  	
  
  	$this->$db->where('guestshasstays.reservation_reservationid', $reservationId);
  
  	$result = $this->$db->get()->result_array();
  	$this->$db->trans_complete();
  	return $result;
  } //end function
  
  
 
  
} //end model class
