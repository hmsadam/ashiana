<?php

class guests extends CI_Model {

  
	//this function is just for reference and not being used anywhere in the application
  
  function saveParentGuestData($fname, $lname, $cellno, $landline, $email, $gender, $age, $street, $city, $county, $postcode, $country, $isFamilyMember, $comments, $userid, $db)
  {
	$data['firstname'] = $fname;
    $data['lastname'] = $lname;
    $data['cellNo'] = $cellno;
    $data['landLineNo'] = $landline;
    $data['email'] = $email;
    $data['gender'] = $gender;
    $data['age'] = $age;
    $data['street'] = $street;
    $data['city'] = $city;
    $data['county'] = $county;
    $data['postcode'] = $postcode;
    $data['country'] = $country;
    $data['isAFamilyMember'] = $isFamilyMember;
    $data['comments'] = $comments;
    $data['guests_guestsid'] = 0;
    
    
   $data['createdon'] = date("Y-m-d H:i:s" ,time());
   $data['createdby'] = $userid; 
    
	
	$this->$db->trans_start();	
	$this->$db->insert('guests',$data);
	$insert_id = $this->$db->insert_id();
	$this->$db->trans_complete();
	
	$parent['guests_guestsid'] = $insert_id;    
	$this->$db->trans_start();
	$this->$db->where('guestsid', $insert_id);
	$this->$db->update('guests', $parent);
	$this->$db->trans_complete();
	
	if($isFamilyMember == 0)
	   {
		   $this->session->set_userdata(array('parentgstid'=>$insert_id));
	   } 
	//this method should be called for parent paying guest
	
	
	return $insert_id;
  } //end function
  
  function saveChildGuestData($parentid, $fname, $lname, $cellno, $landline, $email, $gender, $age, $street, $city, $county, $postcode, $country, $isFamilyMember, $comments, $userid, $db)
  {
	$data['firstname'] = $fname;
    $data['lastname'] = $lname;
    
    $data['cellNo'] = $cellno;
    $data['landLineNo'] = $landline;
    $data['email'] = $email;
    $data['gender'] = $gender;
    $data['age'] = $age;
     $data['street'] = $street;
    $data['city'] = $city;
    $data['county'] = $county;
    $data['postcode'] = $postcode;
    $data['country'] = $country;
    $data['isAFamilyMember'] = $isFamilyMember;
    $data['comments'] = $comments;
    $data['guests_guestsid'] = $parentid;
    
    
   $data['createdon'] = date("Y-m-d H:i:s" ,time());
   $data['createdby'] = $userid; 
    
	
	$this->$db->trans_start();	
	$this->$db->insert('guests',$data);
	$insert_id = $this->$db->insert_id();
	$this->$db->trans_complete();
	
		
	return $insert_id;
  } //end function
  
  
  
  
  
  function checkIsParent($guestid, $db)
  {
  	$this->$db->trans_start();
  	$this->$db->from('guests');
  	$this->$db->where('guestsid', $guestid);
  	$this->$db->where('guests_guestsid', $guestid);
    $guest = $this->$db->get()->result_array();
	$this->$db->trans_complete();
	  return $guest;
  
  } //end function
  
  function deleteGuest($id, $db)
  {
  	$this->$db->trans_start();
  	$this->$db->where('guestsid', $id);
  	$this->$db->delete('guests');
  	$this->$db->trans_complete();
  
  
  	return true;
  } //end function
  
  function updateGuestData($guestId, $fname, $lname, $cellno, $landline, $email, $gender, $age, $street, $city, $county, $postcode, $country,  $comments, $userid, $db)
  {
  	$data['firstname'] = $fname;
  	$data['lastname'] = $lname;
  	
  	$data['cellNo'] = $cellno;
  	$data['landLineNo'] = $landline;
  	$data['email'] = $email;
  	$data['gender'] = $gender;
  	$data['age'] = $age;
  	$data['street'] = $street;
  	$data['city'] = $city;
  	$data['county'] = $county;
  	$data['postcode'] = $postcode;
  	$data['country'] = $country;
  	$data['comments'] = $comments;
  	
  
  
  	$data['lastmodifiedon'] = date("Y-m-d H:i:s" ,time());
  	$data['lastmodifiedby'] = $userid;
  
  
  $this->$db->trans_start();	
	$this->$db->where('guestsid', $guestId);
	$this->$db->update('guests', $data);
	$this->$db->trans_complete();
	
	if ($this->$db->trans_status() === FALSE) {
    # Something went wrong.
    $this->$db->trans_rollback();
    return false;
	
	} 
	else {
		# Everything is Perfect. 
		# Committing data to the database.
		$this->$db->trans_commit();
		return true;
	}
  } //end function
  
  function getGuestsData($guestid, $db)
  {
  	$this->$db->trans_start();
  	
  	$this->$db->from('guests');
  	$this->$db->where( array('guestsid'=>$guestid));
  	$guest = $this->$db->get()->result_array();
  	$this->$db->trans_complete();
  	return $guest;
  } //end function
  
  function findguestbyfname($firstname, $db)
  {
  	$this->$db->trans_start();
  	 
  	$this->$db->from('guests');
  	$this->$db->where( array('firstname'=>$firstname));
  	$guest = $this->$db->get()->result_array();
  	$this->$db->trans_complete();
  	return $guest;
  } //end function
  
  function findguestbylname($lastname, $db)
  {
  	$this->$db->trans_start();
  
  	$this->$db->from('guests');
  	$this->$db->where( array('lastname'=>$lastname));
  	$guest = $this->$db->get()->result_array();
  	$this->$db->trans_complete();
  	return $guest;
  } //end function
  
  function findguestbyemail($email, $db)
  {
  	$this->$db->trans_start();
  
  	$this->$db->from('guests');
  	$this->$db->where( array('email'=>$email));
  	$guest = $this->$db->get()->result_array();
  	$this->$db->trans_complete();
  	return $guest;
  } //end function
  
  function findguestbypostcode($postcode, $db)
  {
  	$this->$db->trans_start();
  
  	$this->$db->from('guests');
  	$this->$db->where( array('postcode'=>$postcode));
  	$guest = $this->$db->get()->result_array();
  	$this->$db->trans_complete();
  	return $guest;
  } //end function
  
  
} //end model class
