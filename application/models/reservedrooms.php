<?php

class reservedrooms extends CI_Model {

  
	
  
  function saveReservedRoom($rsvid, $roomid, $status, $startdate, $enddate, $roomrate, $roomcost, $userid, $db)
  {
	$data['reservation_reservationid'] = $rsvid;
    $data['hotelrooms_hotelroomsid'] = $roomid;
    $data['reservedroomstatus'] = $status;
    $data['startdate'] = $startdate;
    $data['enddate'] = $enddate;
    $data['roomrate'] = $roomrate;
    $data['cost'] = $roomcost;
   
    
   $data['createdon'] = date("Y-m-d H:i:s" ,time());
    
    $data['createdby'] = $userid; 
    
	
	$this->$db->trans_start();	
	$this->$db->insert('reservedrooms',$data);
	$insert_id = $this->$db->insert_id();
	$this->$db->trans_complete();
	
	if ($this->$db->trans_status() === FALSE) {
    # Something went wrong.
    $this->$db->trans_rollback();
    return false;
	
	} 
	else {
		# Everything is Perfect. 
		# Committing data to the database.
		$this->$db->trans_commit();
		return true;
	}
  } //end function
  
  function getRoomsByBranch($branchid, $db)
  {
	
	  $this->$db->trans_start();
	  $this->$db->select('hotelroomsid, roomno, currentstatus, roomrate, hotalrooms.noofbeds, roomcategoriesid, categoryname, hotelfloorsid, floorname');
	  //$this->$db->select('hotelroomsid, roomno');
	$this->$db->from('hotalrooms');
	$this->$db->join('roomcategories', 'roomcategories.roomcategoriesid = hotalrooms.roomcategories_roomcategoriesid','left');
	$this->$db->join('hotelfloors', 'hotelfloors.hotelfloorsid = hotalrooms.hotelfloors_hotelfloorsid','left');
	
	$this->$db->where('hotalrooms.hotelbranches_hotelbranchesid', $branchid); 
	
	$result = $this->$db->get()->result_array();
	$this->$db->trans_complete();				  	
		return $result;
	  
	  
  } //end function
  
  //this function is used to get rooms for getAvailable rooms 
  function getRoomsByBranchForReservation($branchid, $db)
  {
	
	  $this->$db->trans_start();
	  $this->$db->select('hotelroomsid, roomno, currentstatus, roomrate, hotalrooms.noofbeds, roomcategoriesid, categoryname, hotelfloorsid, floorname');
	  //$this->$db->select('hotelroomsid, roomno');
	$this->$db->from('hotalrooms');
	$this->$db->join('roomcategories', 'roomcategories.roomcategoriesid = hotalrooms.roomcategories_roomcategoriesid','left');
	$this->$db->join('hotelfloors', 'hotelfloors.hotelfloorsid = hotalrooms.hotelfloors_hotelfloorsid','left');
	
	$this->$db->where('hotalrooms.hotelbranches_hotelbranchesid', $branchid); 
	$this->$db->where('hotalrooms.currentstatus', 'ready'); 
	$result = $this->$db->get()->result_array();
	$this->$db->trans_complete();				  	
		return $result;
	  
	  
  } //end function
  
  
  
  
	function getRsvdRoom($branchid, $startdate, $enddate, $db)
  {
	$status = "active";
	$this->$db->trans_start();
	$this->$db->select('reservedrooms.reservation_reservationid, hotelrooms_hotelroomsid, startdate, enddate, hotelbrancheshasreservations.status');
	$this->$db->from('reservedrooms');
	
	$this->$db->where('( "'. $startdate . '" BETWEEN startdate AND enddate  ) OR ( "'. $enddate . '" BETWEEN startdate AND enddate)');
	$this->$db->where('(startdate BETWEEN "' . $startdate . '" AND "' . $enddate . '") OR (enddate BETWEEN "' . $startdate . '" AND "' . $enddate . '")');
	$this->$db->join('hotelbrancheshasreservations', 'hotelbrancheshasreservations.reservations_reservationsid = reservedrooms.reservation_reservationid');
	$this->$db->where('(hotelbrancheshasreservations.status = '."'".$status."')");
	
	$this->$db->where('(hotelbrancheshasreservations.hotelbranches_hotelbranchesid ='."'".$branchid."')");
	
	$result = $this->$db->get()->result_array();
	
	
	$this->$db->trans_complete();				  	
		return $result;
  } //
  
  function getRsvdRoomForGrid($branchid, $startdate, $enddate, $db)
  {
  	$status = "active";
  	$this->$db->trans_start();
  	$this->$db->select('reservedrooms.reservation_reservationid, hotelrooms_hotelroomsid, startdate, enddate, hotelbrancheshasreservations.status, roomno');
  	$this->$db->from('reservedrooms');
    	
  	$this->$db->where('(startdate BETWEEN "' . $startdate . '" AND "' . $enddate . '") OR (enddate BETWEEN "' . $startdate . '" AND "' . $enddate . '")');
  	$this->$db->join('hotelbrancheshasreservations', 'hotelbrancheshasreservations.reservations_reservationsid = reservedrooms.reservation_reservationid');
  	$this->$db->join('hotalrooms', 'hotalrooms.hotelroomsid = reservedrooms.hotelrooms_hotelroomsid', 'left');
  	$this->$db->where('(hotelbrancheshasreservations.status = '."'".$status."')");
  
  	$this->$db->where('(hotelbrancheshasreservations.hotelbranches_hotelbranchesid ='."'".$branchid."')");
  
  	$result = $this->$db->get()->result_array();
  
  
  	$this->$db->trans_complete();
  	return $result;
  } //
  
  function getrsvdromsformaingrph($branchid, $startdate, $enddate, $db)
  {
  	$status = "active";
  	$this->$db->trans_start();
  	$this->$db->select('reservedrooms.reservation_reservationid, hotelrooms_hotelroomsid, startdate, enddate, hotelbrancheshasreservations.status as rsrvtnstatus, reservedrooms.reservedroomstatus, roomno');
  	$this->$db->from('reservedrooms');
  	 
  	$this->$db->where('(startdate BETWEEN "' . $startdate . '" AND "' . $enddate . '") OR (enddate BETWEEN "' . $startdate . '" AND "' . $enddate . '")');
  	$this->$db->join('hotelbrancheshasreservations', 'hotelbrancheshasreservations.reservations_reservationsid = reservedrooms.reservation_reservationid');
  	$this->$db->join('hotalrooms', 'hotalrooms.hotelroomsid = reservedrooms.hotelrooms_hotelroomsid', 'left');
  	$this->$db->where('(hotelbrancheshasreservations.status = '."'".$status."')");
  
  	$this->$db->where('(hotelbrancheshasreservations.hotelbranches_hotelbranchesid ='."'".$branchid."')");
  
  	$result = $this->$db->get()->result_array();
  
  
  	$this->$db->trans_complete();
  	return $result;
  } //
  
  function getRsvdRomByRsvId($reservationId, $db)
  {
	  $this->$db->trans_start();
	  $this->$db->select('reservedroomsid, reservedrooms.reservation_reservationid, hotelrooms_hotelroomsid, startdate, enddate, hotelroomsid, roomno, hotelfloors_hotelfloorsid, 	guestcheckedout, floorname, roomcategories_roomcategoriesid, categoryname, roomrate, reservedroomstatus');
	$this->$db->from('reservedrooms');
	
	$this->$db->join('hotalrooms', 'hotalrooms.hotelroomsid = reservedrooms.hotelrooms_hotelroomsid', 'left');
	$this->$db->join('roomcategories', 'roomcategories.roomcategoriesid = hotalrooms.roomcategories_roomcategoriesid','left');
	$this->$db->join('hotelfloors', 'hotelfloors.hotelfloorsid = hotalrooms.hotelfloors_hotelfloorsid','left');
	
	$this->$db->where('reservedrooms.reservation_reservationid', $reservationId); 
	
	$result = $this->$db->get()->result_array();
	$this->$db->trans_complete();				  	
		return $result;
	  
	  
  } //end function
  
  function updatrsvdromocptionstatus($reservationId, $roomid, $status, $db)
  {
  
  	$this->$db->trans_start();
  	$data['reservedroomstatus'] = $status;
  
  
  	$this->$db->where('reservedrooms.hotelrooms_hotelroomsid', $roomid);
  	$this->$db->where('reservedrooms.reservation_reservationid', $reservationId);
  	$this->$db->update('reservedrooms', $data);
  	$this->$db->trans_complete();
  
  
  
  } //end function


  function deleteReservedRoomFromReservation($resvId, $db)
  {
		$this->$db->trans_start();
		$this->$db->where('reservation_reservationid', $resvId);
		//$this->$db->where('hotelrooms_hotelroomsid', $roomid);
		$this->$db->delete('reservedrooms');
		$this->$db->trans_complete();
		
		
		return true;
  } //end function
  
  
  
} //end model class
