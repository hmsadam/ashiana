<?php

class contactusenquiry extends CI_Model {

  
  function getenquiries()
  {
  	$db = 'mrkslrptdb';
  	$formid = 1484;
  	
	$this->$db->trans_start();
	$this->$db->from('wp8w_wpforms_entries');
	$this->$db->where('form_id', $formid);
    $enqs = $this->$db->get()->result_array();
	$this->$db->trans_complete();
	  return $enqs;
	  
  } //end function
  
  function deleteenquiry($id)
  {
  	$db = 'mrkslrptdb';
  	
  	
  	$this->$db->trans_start();
  	$this->$db->where('entry_id', $id);
  	$this->$db->delete('wp8w_wpforms_entries');
  	$this->$db->trans_complete();
  	return true;
  
  } //end function
  
 
  
} //end model class
