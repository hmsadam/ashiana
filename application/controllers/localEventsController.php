<?php

class localEventsController extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
	

    if( !$this->session->userdata('isLoggedIn') ) {
        redirect('/login/show_login');
    }
  }

	function getlocalevents()
	{
		$branchid = $this->input->post('branchid');
		
		$result = $this::deleteeventfrmserver();
		//$branchid = 1;
		$db = $this->session->userdata('db');
		
		$this->load->model('localevents');
		$localevents = $this->localevents->getlocaleventsbybranch($branchid, $db);
	echo json_encode(array('data' => $localevents));
	}
  
  function localeventsView() {
    
	$this->load->model('hotelbranches');
	$branches = $this->hotelbranches->getHotelsByUser();
		
	$user_id = $this->session->userdata('id');
	$db = $this->session->userdata('db');
	
	$data['branches'] = $branches;
    $data['username'] = $this->session->userdata('name');
   ///log_message('info', 'The purpose of some variable is to provide some value.');
	$this->load->helper('form');
    $this->load->view('localeventsview',$data);
  }

    
  function createlocalevent()
  {
	  $headline = $this->input->post('headline');
	  $evntdate = $this->input->post('evntdate');
	  $evnttext = $this->input->post('evnttext');
	  $evntposition = $this->input->post('evntposition');
	  $branchid = $this->input->post('branchid');
	  
	  /*$headline = "Birth Day";
	  $evntdate = "2019-08-20 20:00:00";
	  $evnttext = "Hello";
	  $evntposition = 2;
	  $branchid = 1;*/
	  
	  $db = $this->session->userdata('db');
	  
	  $userid = $this->session->userdata('id');
	 
	 
	
	  $this->load->model('localevents');
	  $result = $this->localevents->createlocalevent($headline, $evntdate, $evnttext,  $evntposition, $branchid, $db);
	  
	 if($result == true)
	  {
		  //echo json_encode($result);
		  echo json_encode(true);
	  } else {
		  echo json_encode(false);
		  
	  }
	  
  }   //end function
      
  function updatelocalevent(){
  	
  	$eventid = $this->input->post('eventid');
  	$headline = $this->input->post('headline');
  	$evntdate = $this->input->post('evntdate');
  	$evnttext = $this->input->post('evnttext');
  	$evntposition = $this->input->post('evntposition');
  	
  	 
  	/*$headline = "Birth Day";
  	 $evntdate = "2019-08-20 20:00:00";
  	 $evnttext = "Hello";
  	 $evntposition = 2;
  	 $branchid = 1;*/
  	 
  	$db = $this->session->userdata('db');
  	 
  	$userid = $this->session->userdata('id');
  	
  	
  	
  	$this->load->model('localevents');
  	$result = $this->localevents->updatelocalevent($eventid, $headline, $evntdate, $evnttext,  $evntposition, $db);
  	 
  	if($result == true)
  	{
  		//echo json_encode($result);
  		echo json_encode(true);
  	} else {
  		echo json_encode(false);
  	
  	}
  	
  } //end function
  
  
  function insertevntidinnsess()
  {
  	$eventid = $this->input->post('eventid');
  
  	$this->session->set_userdata(array('eventid'=>$eventid));
  
  	//return true;
  	echo json_encode(true);
  } //end function
  
  function getevntimage()
  {
  	$eventid = $this->input->post('eventid');
  	$db = $this->session->userdata('db');
  	 
  	//$eventid = 1;
  	$this->load->model('localevents');
  	$imgname = $this->localevents->getimgname($eventid, $db);
  	 
  	 
  	echo json_encode($imgname);
  } //end function
  
  function uploadeventimage()
  {
  	$result = false;
  	$data;
  	$picname;
  	$response;
  	$error;
  	$db = $this->session->userdata('db');
  	$this->load->library('upload');
  
  	$incomingimgname = $_FILES['evntimg']['name'];
  		
  	$this->load->model('localevents');
  	$alleventimgs = $this->localevents->getevntimages($db);
  
  	foreach($alleventimgs as $evntimg){
  		if($evntimg['image'] == $incomingimgname) {
  			$rslt = "Image already uploaded";
  			echo json_encode($rslt);
  			exit;
  		}
  
  	} //end foreach
  	 
  	$eventid = $this->session->userdata('eventid');
  	 
  	$this->load->model('localevents');
  	$oldimgnames = $this->localevents->getimgname($eventid, $db);
  
  	$oldimgname = $oldimgnames[0]['image'];
  
  	if($oldimgname != ""){
  		 
  		$path_to_file = './assets/images/'.$oldimgname.'';
  		$result = unlink($path_to_file);
  	}
  	 
  	 
  	if (isset($_FILES['evntimg']) && !empty($_FILES['evntimg']))
  	{
  		if ($_FILES['evntimg']['error'] != 4)
  		{
  			// Image file configurations
  			$config['upload_path'] = './assets/images/';
  			$config['allowed_types'] = 'jpg|jpeg|png|gif';
  			$config['remove_spaces'] = false;
  			$config['max_size']             = 100000; //100mb
  				
  			$this->upload->initialize($config);
  			$result = $this->upload->do_upload('evntimg');
  			if($result)
  			{
  				$data = $this->upload->data(); //get data about uploaded image/file
  				//$roomid = $this->session->userdata('roomid');
  				$userId = $this->session->userdata('id');
  
  				$imgname = $data['file_name'];
  				$this->load->model('localevents');
  				$isimgsaved = $this->localevents->uploadimagepath($eventid, $imgname, $db);
  				$response = array(
  						'picname' => $imgname,
  						'isimagesaved' => $isimgsaved,
  						'income' => $incomingimgname
  				);
  				echo json_encode('Image uploaded');
  			} else {
  				$error = $this->upload->display_errors();
  				echo json_encode('Image size issue');
  			}
  		}
  	} //end out if
  
  } //end function
  
  function deleteeventfrmclnt() 
  {
  	$eventid = $this->input->post('eventid');
  	$imagename = $this->input->post('imagename');
  	
  	$result = $this::deleteevent($eventid, $imagename);
  	
  	echo json_encode($result);
  } //end function
  
  function deleteeventfrmserver(){
  	
  	$db = $this->session->userdata('db');
  	$hotelId = $this->session->userdata('hotelId');
  	
  	$this->load->model('localevents');
  	$localevents = $this->localevents->getlocaleventsbybranch($hotelId, $db);
  	
  	$cmpltd = false;
  	
  	$expiredevnts = array();
  	if(sizeof($localevents) > 0) {
  		foreach($localevents as $le){
  			$cmpltd = $this::determineExpiryOfReservation($le['eventdate']);
  			  			
  			if($cmpltd){
  				$expiredevnts[] = $le;
  			} // end inner if
  		} //endforeach 
  	} //end if
  	
  	$eventsdeleted = array();
  	if(sizeof($expiredevnts) > 0) {
	  	foreach($expiredevnts as $le){
	  		$eventsdeleted[] = $this::deleteevent($le['localeventid'], $le['image']);
	  	
	  	} //endforeach
  	}
  	
  	return $eventsdeleted;
  	
  } //end function
  
    
  function deleteevent($eventid, $imagename)
  {
  	
  	$db = $this->session->userdata('db');
  
  	if($imagename == ""){
  		$imagename = "Empty";
  	} else {
  		$path_to_file = './assets/images/'.$imagename.'';
  		unlink($path_to_file); //delete old pic
  	}
  	
  	//$eventid = 4;
  	
  	$this->load->model('localevents');
  	$eventresult = $this->localevents->deleteevent($eventid, $db);
  
  	$result = array();
  	$result['imagename'] = $imagename;
  	 $result['eventdeleted'] = $eventresult;
  
  	return $result;
  } //end function
  
  function determineExpiryOfReservation($expirydate){
  	$this->load->helper('date');
  	$eventdate = strtotime($expirydate);
  	$time2 = date('Y-m-d');
  	$currenttime = strtotime($time2);
  		
  	if($currenttime <= $eventdate){
  			
  		return false;
  
  	} else {
  			
  		return true;
  
  	}
  } //end function
  
  
} //end class
