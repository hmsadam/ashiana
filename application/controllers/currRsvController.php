<?php

class currRsvController extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
	

    if( !$this->session->userdata('isLoggedIn') ) {
        redirect('/login/show_login');
    }
  } 

   function currentReservations() {
   	$this->load->model('hotelbranches');
   	$branches = $this->hotelbranches->getHotelsByUser();
   
   	$data['branches'] = $branches;
   	$data['username'] = $this->session->userdata('name');
   
   	$user_id = $this->session->userdata('id');
   	$db = $this->session->userdata('db');
   	$data['userid'] = $user_id;
   
   
   	$this->load->helper('form');
   	$this->load->view('currentReservationsView',$data);
   }
   
   function getCurrentReservations()
   {
   	
   	$branchid = $this->input->post('branchid');
   	$status = $this->input->post('status');
   	
   	//$branchid = 1;
   	//$status = 'active';
   
   	$db = $this->session->userdata('db');
   	$currentReservations;
   	
   	if($status == "all") {
   		$this->load->model('hotelbrancheshasrsvr');
   		$currentReservations = $this->hotelbrancheshasrsvr->getAllCurrentRsvs($branchid, $db);
   	} else {
   		$this->load->model('hotelbrancheshasrsvr');
   		$currentReservations = $this->hotelbrancheshasrsvr->getCurrentRsvs($branchid, $status, $db);
   	}
   	//getstaydata($guestid)
   	//$currentReservations[0]['newatt'] = 'newattrb';
   	
   	/*for($i = 0; $i < sizeof($currentReservations); $i++){
   		$currentReservations[$i]['stays'] = $this::getstaydata($currentReservations[$i]['guestsid']);
   	} //end foreach
   */
   	echo json_encode(array('data' => $currentReservations));
   	//echo json_encode($currentReservations);
   
   } //end function
   
   
   function getReservationData(){
   	
   	$reservationId = $this->input->post('rsvid');
   	//$reservationId = "FRZMNBR2019Q3-1";
   	$rsvdrooms = $this::getRsvdRomsForDataTable($reservationId);
   	$guestdata = $this::getGuestsData($reservationId);
   	$staydata = $this::getstaydata($guestdata[0]['guestsid']);
   	$guestbill = $this::getGuestsBill($reservationId);
   	$result = array(
   			'rsvdrooms' => array('data' => $rsvdrooms),
   			'guestdata' => array('data' => $guestdata),
   			'staydata' => array('data' => $staydata),
   			'guestbill' => array('data' => $guestbill),
   			);
   	echo json_encode($result);
   } //end function
   
   function getstaydata($guestid)
   {
   	
   	//$guestid = "2";
   	$db = $this->session->userdata('db');
   	$this->load->model('guestsstays');
   	$gstsrsvns = $this->guestsstays->getgeuststays($guestid, $db);
   	return $gstsrsvns;
   	//echo json_encode($gstsrsvns);
   
   } //end function
      
   private function getRsvdRomsForDataTable($reservationId)
   {
   	
   	$db = $this->session->userdata('db');
   
   	$this->load->model('reservedrooms');
   	$allRooms = $this->reservedrooms->getRsvdRomByRsvId($reservationId, $db);
   
   	return $allRooms;
   
   
   } //end function
   
   private function getGuestsData($rsvid)
   {
   	
   	$db = $this->session->userdata('db');
   	$this->load->model('guesthasreservations');
   	$gstsrsvns = $this->guesthasreservations->getGuestsDataByRsvId($rsvid, $db);
   return $gstsrsvns;
   	
   } //end function
   
   private function getGuestsBill($rsvid)
   {
   	
   	$db = $this->session->userdata('db');
   	$this->load->model('guestbill');
   	$bill = $this->guestbill->getGuestsBill($rsvid, $db);
   	return $bill;
   	
   } //end function
   
  function updateReservationStatus(){
  	$branchid = $this->input->post('hotelId');
  	//get reserved rooms seperately and then determine their expiry
  	//$branchid = "1";
  	$db = $this->session->userdata('db');
  	$status = "confirmed";
  	
  	$userid = $this->session->userdata('id');
  	$rsvdRooms = array();
  	 
  	$this->load->model('hotelbrancheshasrsvr');
  	$currentReservations = $this->hotelbrancheshasrsvr->getCurrentRsvsForStatusUpdate($branchid, $status, $db);
  	$noofconfirmedrsvns = sizeof($currentReservations);
  	$noofcnfrmdrsvnsupdtd = 0;
  	
  	
  	if(sizeof($currentReservations) > 0) {
		  	foreach($currentReservations as $rv){
		  		$cmpltd = $this::determineExpiryOfReservation($rv['gestdeparturedate']);
		  		
		  		if($cmpltd){
		  			$noofcnfrmdrsvnsupdtd++;
		  			$reservationstatus = "complete";
		  			$this->load->model('hotelbrancheshasrsvr');
		  			$rsvupdrslt = $this->hotelbrancheshasrsvr->updateReservationStatus($rv['reservationid'], $reservationstatus, $db);
		  			
		  		} 
		  		
		  	} //end foreach
  	
  	} //end if
  		
  	$actvstatus = "active";
  	$this->load->model('hotelbrancheshasrsvr');
  	$activcrntrsvn = $this->hotelbrancheshasrsvr->getCurrentRsvsForStatusUpdate($branchid, $actvstatus, $db);
  	$noofactiversvns = sizeof($activcrntrsvn);
  	$noofactvrsvnsupdtd = 0;
  	if(sizeof($activcrntrsvn) > 0) {
  		foreach($activcrntrsvn as $rv){
  			$cmpltd = $this::determineExpiryOfReservation($rv['gestdeparturedate']);
  	
  			if($cmpltd){
  				$noofactvrsvnsupdtd++;
  				$reservationstatus = "complete";
  				$this->load->model('hotelbrancheshasrsvr');
  				$rsvupdrslt = $this->hotelbrancheshasrsvr->updateReservationStatus($rv['reservationid'], $reservationstatus, $db);
  	
  			}
  		} //end foreach
  		
  	} //end if 
  	 	 	
  	$resultary = array();
  	
  	$resultary['noofconfirmedrsvns'] = $noofcnfrmdrsvnsupdtd;
  	$resultary['noofactiversvns'] = $noofactiversvns;
  	$resultary['noofcnfrmdrsvnsupdtd'] = $noofcnfrmdrsvnsupdtd;
  	$resultary['noofactvrsvnsupdtd'] = $noofactvrsvnsupdtd;
  	
  	echo json_encode($resultary);
  	
  } //end function 
  
  function determineExpiryOfReservation($expirytime){
  	$this->load->helper('date');
  	$reservationendtime = strtotime($expirytime);
  	$time2 = date('Y-m-d H:i:s');
  	$currenttime = strtotime($time2);
  	 
  	 if($currenttime < $reservationendtime){
  	 	
  	 	return false;
  	 
  	 } else {
  	 	
  	 	return true;
  	 
  	 }
  }
  
  function updtRsvSttusCnclPmntRcvd() {
  	
  	$rsvid = $this->input->post('rsvid');
  	$status = $this->input->post('status');
  	$db = $this->session->userdata('db');
  	
  	$this->load->model('hotelbrancheshasrsvr');
  	$rsvupdrslt = $this->hotelbrancheshasrsvr->updateReservationStatus($rsvid, $status, $db);
  	
  	
  	
  	
  	echo json_encode(true);
  	
  } //end function
  
  function markpaymentrcvd() {
  	$rsvid = $this->input->post('rsvid');
  	
  	//$rsvid = "FRZMNBR2019Q3-41";
  	
  	$db = $this->session->userdata('db');
  	$userid = $this->session->userdata('id');
  	
  	
  	$this->load->model('guestbill');
  	$bill = $this->guestbill->getGuestsBill($rsvid, $db);
  	
  	$grandtotal = (int)$bill[0]['grandtotal'];
  	$advance = (int)$bill[0]['paymentpaid'];
  	$balance = (int)$bill[0]['balance'];
  	
  	if($balance > 0 ){
  		$this->load->model('guestbill');
  		$bill = $this->guestbill->paybalance($rsvid, $grandtotal, 0, $userid, $db);
  		echo json_encode($bill);
  	} else {
  		echo json_encode('bill already paid');
  	}
  	
  	
  	
  } //end function
    
   
} //end class


