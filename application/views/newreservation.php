
<?php ?>

<?php include("header.php")?>



<?php include("menu.php")?>

<script src="<?php echo base_url();?>assets/js/jquery-ui.js"></script>
  
  <link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery-ui.css">
<script src="<?php echo base_url();?>assets/js/hmsappscripts/mainscript.js" type="text/javascript"></script> 
 
<script src="<?php echo base_url();?>assets/js/hmsappscripts/reservationscript.js" type="text/javascript"></script>

<script src="<?php echo base_url();?>assets/js/hmsappscripts/dtableforreservationscript.js" type="text/javascript"></script> 
<script src="<?php echo base_url();?>assets/js/hmsappscripts/guestdatatablescript.js" type="text/javascript"></script>


<script src="<?php echo base_url();?>assets/js/hmsappscripts/findguestscript.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/js/hmsappscripts/billingcalculations.js" type="text/javascript"></script> 
<script src="<?php echo base_url();?>assets/js/hmsappscripts/customnotificationsscript.js" type="text/javascript"></script> 
 
<script src="<?php echo base_url();?>assets/js/timepickadon/jquery-ui-timepicker-addon.js" type="text/javascript"></script> 
<link rel="stylesheet" href="<?php echo base_url();?>assets/js/timepickadon/jquery-ui-timepicker-addon.css"> 
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/hmscssfiles/dtablescolaps.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/hmscssfiles/jquery.dataTables.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/hmscssfiles/rsvwizard.css">

<!-- select2 -->
  <link href="<?php echo base_url();?>assets/css/select/select2.min.css" rel="stylesheet">

<script>

/*	if(!alldatasaved){
		window.onbeforeunload = function() {
			console.log('onbeforeunload called');
		   return false;
		   
		};
	}*/
window.onunload = function() {

	if(!alldatasaved){
		cancelRsv();
	}
};


</script>


<body class="nav-md">

 
      
      <!-- page content -->
      <div class="right_col"  role="main">
        <div class="">

          <div class="page-title">
            <div class="title_left">
              <h2>New Booking</h2>
            </div>
            
          </div>
          <div class="clearfix"></div>
          

          


          <div class="row">
            <div class="col-md-12 col-xs-12">
              

			  <!--Start New Reservation Button Div -->
				
				<!--End New Reservation Button Div -->
				
				<div class="wrapper">
	<div class="inline">
	
	</div>
	<!-- End SmartWizard Content -->          
	</div>
				
				
				        

			  
              

            </div>
			
			
          </div>
        </div>
        <!-- /page content -->
        
       <!--Start Wizard Div -->
				<div class="x_panel" id="rsvwzrd">
                <div class="x_title">
                  <h3>Create New Booking / Phone Booking</h3>
                  
                </div>
				<div class="x_content">
					 <!-- Smart Wizard -->
                  
                  <div id="wizard" class="form_wizard wizard_horizontal">
                    <ul class="wizard_steps">
                      <li class="col-md-3 col-sm-3  col-xs-3  padding-right-0 padding-left-0 ">
                        <a href="#step-1">
                          <span class="step_no"></span>
                         
                        </a>
                      </li>
                      <li class="col-md-3 col-sm-3  col-xs-3 padding-right-0 padding-left-0 ">
                        <a href="#step-2">
                          <span class="step_no"></span>
                          
                        </a>
                      </li>
                      <li class="col-md-3 col-sm-3  col-xs-3 padding-right-0 padding-left-0 ">
                        <a href="#step-3">
                          <span class="step_no"></span>
                          
                        </a>
                      </li>
					  <li class="col-md-3 col-sm-3  col-xs-3  padding-left-0 padding-right-0 ">
                        <a href="#step-4">
                          <span class="step_no"></span>
                         
                        </a>
                      </li>
                      
                    </ul>
                    <div id="step-1">
                      <form autocomplete="off" id="roomCreationForm" data-parsley-validate class="form-horizontal form-label-left" autocomplete="off">
						<h3 class="head">1. Search Room Availability</h3>
					<div class="form-group">
                      
                      <div class="col-md-3 col-sm-3 col-xs-12 padding-left-0">
                      <label class="control-label " >Check-in </label>
                        <input required type="text" id="startdate" name="startdate" placeholder="dd/mm/yyyy" class="form-control col-md-7 col-xs-12">
						
                      </div>
                      <div class=" col-md-3 col-sm-3 col-xs-12 pl0">
                      	<label class="control-label" >Checkout </label>
                        <input required type="text" id="enddate" name="enddate" placeholder="dd/mm/yyyy" class="form-control col-md-7 col-xs-12">
                      </div>
                    </div>
					
                    <div class="form-group">
                      
                      <div class="col-md-3 col-sm-3 col-xs-12 padding-left-0">
                      <label class="control-label ">Adults</label>
                        <select class="form-control" id="adults" required="required" >
                        	<option value="" disabled selected hidden>Select Adults</option>
                           <option value="1">1 Adult</option>
                           <option value="2">2 Adults</option>
                           <option value="3">3 Adults</option>
                           <option value="4">4 Adults</option>
                           <option value="5">5 Adults</option>
                           <option value="6">6 Adults</option>
                           <option value="7">7 Adults</option>
                           <option value="8">8 Adults</option>
                           <option value="9">9 Adults</option>
                           <option value="10">10 Adults</option>
                        </select>
                      </div>
                      <div class="col-md-3 col-sm-3 col-xs-12 pl0">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12">Children</label>
                        <select class="form-control infachild" id="childrens" >
                        	
                           <option selected value="0">0 Children</option>
                           <option value="1">1 Child</option>
                           <option value="2">2 Children</option>
                           <option value="3">3 Children</option>
                           <option value="4">4 Children</option>
                           <option value="5">5 Children</option>
                           <option value="6">6 Children</option>
                           <option value="7">7 Children</option>
                           <option value="8">8 Children</option>
                           <option value="9">9 Children</option>
                           <option value="10">10 Children</option>
                        </select>
                      </div>
                      <div class="col-md-3 col-sm-3 col-xs-12 pl0">
                      	<label class="control-label col-md-3 col-sm-3 col-xs-12">Infants</label>
                        <select class="form-control" id="infants" >
                        
                           <option selected value="0">0 Infants</option>
                           <option value="1">1 Infant</option>
                           <option value="2">2 Infants</option>
                           <option value="3">3 Infants</option>
                           <option value="4">4 Infants</option>
                           <option value="5">5 Infants</option>
                           <option value="6">6 Infants</option>
                           <option value="7">7 Infants</option>
                           <option value="8">8 Infants</option>
                           <option value="9">9 Infants</option>
                           <option value="10">10 Infants</option>
                        </select>
                      </div>
                    </div>
                    
					
					
                    <div class="form-group" >
                      <div class="col-md-12 col-sm-12 col-xs-12 padding-left-0">
                       <input type="button" value=" Search Availability" id="avbltbtn"  class="btn btn-success col-md-12 col-sm-12 col-xs-12">
                      </div>
                    </div>
                    <div id="romfndnot" class="notfound" style="display: none;">
                       <div class="clearfix "></div>
                      	<div class="col-md-12 col-sm-12 col-xs-12">
                          
                            <div class="head">No Results On Those Dates</div>
                            <div class="text">We are sorry, but we are fully reserved for the dates you have chosen.</div>
                            <div class="text">Please enter a different start and end date and search again.</div>
                          </div>
                      </div>
					<div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12 padding-left-0">
                      <label class="control-label ">Available Rooms</label>	
                        <select class="form-control availableRooms disableded" id="availableRooms"  >
                           <option value="" disabled="disabled" selected="selected" hidden="" >Choose Your Room</option>
                        </select>
						
                      </div>
                     
                      
                      
					  
					  
                    </div>
                    <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 padding-left-0">
                        <input type="button" value=" Select Room(s) "  onClick="selectRooms()" class="btn btn-success col-xs-12">
                        
						
                      </div>
					</div>
                      <div class="clearfix padding40"></div>
					<div class="form-group col-md-12 col-sm-12 col-xs-12 padding-left-0">
                    
                    	<div style="width: 150px;" class="col-md-3 col-sm-3 col-xs-12 padding-left-0">
                      	<label class="control-label " for="slctdsrms">Selected Rooms: <span class="required">*</span>
                        </label>
                        </div>
                      	<div class="col-md-3 col-sm-3 col-xs-12 pl0">
                      	    <label class="control-label " id="slctdsrms"></label>
                         </div>
                        </div>
                         <div class="form-group col-md-12 col-sm-12 col-xs-12 padding-left-0">
                         <div style="width: 150px;" class="col-md-3 col-sm-3 col-xs-12 padding-left-0">
                           <label class="control-label total" for="roomcostlbl">Total for the Stay : </label>
                         </div>
                         <div class="col-md-6 col-sm-6 col-xs-12 totalinfo pl0" style="font-weight:bold;">
                              <span id="rmcstpnd" style="display: none;">&pound;<label class="control-label totalinfo" id="roomcostlbl" name="roomcostlbl" ></label></span>
                              <span id="rmrtpnd" style="display: none;">(&pound;<label class="control-label totalinfo"  id="roomrate" name="roomrate" > </label>)</span>
                                
                         </div>
                     
                    </div>
                    
					
                    
					
					
                  </form>
                  
                  <div class="actionBar actions"><a onClick="cancelRsv()" class="btn btn-default">Cancel</a><a href="#" onClick="gotosteptwo()" class="btn btn-success">Next</a></div>

                    </div>
                    <div id="step-2">
                      
                      <form autocomplete="off" id="findguestfrm" onSubmit="return false" data-parsley-validate class="form-horizontal form-label-left">
                      <h3 class="head">2. Enter Guest Details</h3>
                      <div class="searcharea">
                          <div class="form-group col-md-6 col-sm-6 col-xs-12 padding-left-0">
                              <label class="control-label " for="srchpstcstmr">Search Past Customers <span class="required">*</span></label>
                              <input type="text" id="srchpstcstmr" name="srchpstcstmr" value="" required class="form-control" placeholder="Search: First Name, Last Name, Post Code or Email">
                          </div>
                          <div class="form-group col-md-6 col-sm-6 col-xs-12 padding-right-0 pl0">
                          <label class="control-label ">Prefill</label>
                          
                            <select class="form-control" id="prefill" name="prefill" >
                                
                               
                            </select>
                           </div>
	                  </div> 
                    
                      </form>
                      
                      
                      <form autocomplete="off" id="customerDataForm" data-parsley-validate class="form-horizontal form-label-left">
					
					<div class="form-group col-md-3 col-sm-3 col-xs-12 padding-left-0">
                      <label class="control-label " for="fname">First Name <span class="required">*</span></label>
                      
                        <input type="text" id="fname" name="fname" value="" required class="form-control " placeholder="First Name">
						
                     
                    </div>
					<div class="form-group  col-md-3 col-sm-3 col-xs-12 pl0">
                      <label class="control-label"  for="lname">Last Name <span class="required">*</span></label>
                     
                        <input type="text" id="lname" name="lname" required class="form-control " value="" placeholder="Last Name">
                      
                    </div>
					<div class="form-group  col-md-3 col-sm-3 col-xs-12 pl0">
                      <label class="control-label"  for="cellno">Mobile Number <span class="required">*</span></label>
                     
                        <input value="" type="text" id="cellno" pattern="/^(?=.*[0-9])[- +()0-9]+$/"  name="cellno" required class="form-control " placeholder="Your mobile number" >
                    </div>
					<div class="form-group col-md-3 col-sm-3 col-xs-12 padding-right-0 pl0">
                      <label class="control-label "  for="landline">Telephone Number </label>
                        <input value="" type="text" id="landline" name="landline" class="form-control col-md-7 col-xs-12" placeholder="Telephone Number">
                    </div>
                    <div class="clearfix"></div>
					<div class="form-group col-md-6 col-sm-6 col-xs-12 padding-left-0">
                      <label class="control-label "  for="email">Email<span class="required">*</span> </label>
                      
                        <input value="" type="email" id="email" name="email" required class="form-control " placeholder="Email">
                      
                    </div>
                    
					<div class="form-group col-md-3 col-sm-3 col-xs-12 pl0">
                      <label class="control-label ">Gender</label>
                        <select class="form-control" id="gender"  >
							<option selected value="Prefer not to say">Prefer not to say</option>
							<option value="Male">Male</option>
							
							<option value="Female">Female</option>
                           
                        </select>
                      
                    </div>
					
					<div class="form-group col-md-3 col-sm-3 col-xs-12 padding-right-0 pl0">
                      <label class="control-label "  for="age">Age </label>
                      <input value="" type="text" id="age" name="age" class="form-control col-md-7 col-xs-12" placeholder="Age">
                     
                    </div>
					<div class="clearfix"></div>
					<div class="form-group col-md-6 col-sm-6 col-xs-12 padding-left-0">
                      <label class="control-label "  for="age">Address </label>
                     
                        <input value="" type="text" id="street" name="street" required class="form-control allinput" placeholder="Street and street number">
                      	<input value="" type="text" id="city" name="city" required class="form-control allinput" placeholder="Town/City">
                        <input value="" type="text" id="county" name="county" required class="form-control allinput" placeholder="County">
                        <input value="" type="text" id="postcode" name="postcode" required class="form-control allinput " placeholder="Post Code">
                         <input value="" type="text" id="country" name="country" required class="form-control allinput " placeholder="Country">
                    </div>
					
					<div class="form-group col-md-6 col-sm-6 col-xs-12 padding-right-0 pl0">
                      <label class="control-label "  for="comments">Guest Booking Comments </label>
                      
                        <textarea class="resizable_textarea form-control" style="width: 100%; overflow: hidden; word-wrap: break-word; resize: horizontal; height: 268px;" id="comments" name="comments" placeholder="Any comments to your booking?" ></textarea> 
                     
                    </div>
                   
                    
										
					<div class="form-group col-md-6 col-sm-6 col-xs-12 padding-right-0" >
                      
                        
                           
                     
                    </div>
                   
                    
					 <div class="clearfix"></div>
					
                  </form>

              <div class="actionBar actions"><a onClick="cancelRsv()" class="btn btn-default">Cancel</a><a onClick="gotostepone()" href="#" class="btn btn-primary" style="display: inline-block;">Previous</a><button  id="saveCustomer" class="btn btn-success "> Save & Continue</button></div>
                    </div>
                    <div id="step-3">
                    
                    <h3 class="head borderrd">3. Reservation Details</h3>
                   
                    <h2 class="sechead">Timing & Guests</h2>
                    
                      <form id="reservationDataForm" data-parsley-validate class="form-horizontal form-label-left">
                        <div class="form-group " >
                            <div class="form-group col-md-3 col-sm-3 col-xs-12 padding-left-0">
                              <label class="control-label " for="checkinin3">Check-in</label>
                              
                                <input type="text" id="checkinin3" name="checkinin3" disabled="disabled" class="form-control col-md-7 col-xs-12">
                                
                             
                            </div>
                            <div class="form-group col-md-3 col-sm-3 col-xs-12 pl0">
                              <label class="control-label " for="checkout3">Checkout</label>
                              <input type="text" id="checkout3" name="checkout3" disabled="disabled" class="form-control col-md-7 col-xs-12">
                                
                            </div>
                        
                        </div>
                        <div class="form-group  searcharea" >
                            <div class="form-group col-md-3 col-sm-3 col-xs-12 padding-left-0">
                              <label class="control-label " for="adults3">Adults</label>
                              
                                <input type="text" id="adults3" name="adults3" disabled="disabled" class="form-control">
                            
                            </div>
                            <div class="form-group col-md-3 col-sm-3 col-xs-12 pl0">
                              <label class="control-label " for="children3">Children</label>
                                <input type="text" id="children3" name="children3" disabled="disabled" class="form-control">
                             </div>
                              <div class="form-group col-md-3 col-sm-3 col-xs-12 pl0">
                              <label class="control-label " for="infants3">Infants</label>
                                <input type="text" id="infants3" name="infants3" disabled="disabled" class="form-control ">
                              </div>
                          
                        </div>
                        <div class="form-group  searcharea" >
                          <div class="form-group col-md-12 col-sm-12 col-xs-12 padding-left-0">
                              <h2 class="sechead">Selected Rooms</h2>
                              
                                <div  class="bgclear col-md-3 col-sm-3 col-xs-12  " id="selectedrooms3">                        	
                                </div>
                            
                            </div>
                            <div class="form-group col-md-12 col-sm-12 col-xs-12 padding-left-0">
                                <label style="width: 150px;" class="control-label col-md-3 col-sm-3 col-xs-12" >Total for your Stay:</label>
                                  <div style="font-weight: bold; margin-top: 9px;" class="col-md-6 col-sm-6 col-xs-12 pl0">
                                   <span id="rmcstpndstpth" style="display: none;">&pound;<label  id="ttlofsty3" name="ttlofsty3" > </label></span>
                                    <span id="rmrtpndstpth" style="display: none;">(&pound;<label  id="roomrate3" name="roomrate3" > </label>)</span>
                                  </div>
                             </div>
                         </div>
                        
                        <h2 class="sechead">Guest Details</h2>
                        <!-- guestdetails starts here -->
                        <div class="clearfix"></div>	
                        <div class="form-group col-md-3 col-sm-3 col-xs-12 padding-left-0">
                          <label class="control-label " for="fname3">First Name </label>
                            <input type="text" id="fname3" name="fname3"  disabled class="form-control">
                        
                        </div>
                        <div class="form-group  col-md-3 col-sm-3 col-xs-12 pl0">
                          <label class="control-label"  for="lname3">Last Name </label>
                            <input type="text" id="lname3" name="lname3" disabled class="form-control col-md-7 col-xs-12">
                        </div>
                        <div class="form-group col-md-3 col-sm-3 col-xs-12 pl0">
                          <label class="control-label "  for="cellno3">Mobile Number</label>
                            <input type="text" pattern="/^(?=.*[0-9])[- +()0-9]+$/" id="cellno3" name="cellno3" disabled class="form-control col-md-7 col-xs-12" >
                         </div>
                        <div class="form-group  col-md-3 col-sm-3 col-xs-12 padding-right-0 pl0">
                          <label class="control-label"  for="landline3">Telephone Number </label>
                            <input  type="text" id="landline3" name="landline3" disabled class="form-control col-md-7 col-xs-12" >
                        </div>
                        <div class="clearfix"></div>	
                        <div class="form-group col-md-6 col-sm-6 col-xs-12 padding-left-0">
                          <label class="control-label "  for="email3">Email</label>
                            <input type="email" id="email3" name="email3" disabled class="form-control col-md-7 col-xs-12"  >
                        </div>
                        
                        <div class="form-group col-md-3 col-sm-3 col-xs-12 pl0">
                          <label class="control-label ">Gender</label>
                            <select class="form-control" id="gender3" disabled >
                                <option value="Prefer not to say">Prefer not to say</option>
                                <option value="Male">Male</option>
                                <option value="Female">Female</option>
                               
                            </select>
                         </div>
                        
                        <div class="form-group col-md-3 col-sm-3 col-xs-12 padding-right-0 pl0">
                          <label class="control-label "  for="age3">Age </label>
                            <input type="text" id="age3" name="ag3e" disabled class="form-control">
                        </div>
                        
                        <div class="form-group col-md-6 col-sm-6 col-xs-12 padding-left-0">
                          <label class="control-label "  for="age3">Address </label>
                            <input  type="text" id="street3" name="street3" disabled class="form-control allinput" >
                            <input  type="text" id="city3" name="city3" disabled class="form-control allinput" >
                              <input type="text" id="county3" name="county3" disabled class="form-control allinput" >
                              <input  type="text3" id="postcode3" name="postcode3" disabled class="form-control allinput"  >
                              <input  type="text" id="country3" name="country3" disabled class="form-control allinput" > 
                        </div>
                        <div class="form-group col-md-6 col-sm-6 col-xs-12 padding-right-0 pl0">
                          <label for="comments3" class="control-label ">Guest Booking Comments</label>
                         <textarea class="resizable_textarea form-control" style="width: 100%; overflow: hidden; word-wrap: break-word; resize: horizontal; height: 268px;" id="comments3" name="comments3" disabled ></textarea> 
                         </div>
                    
                    
                    <!-- guestdetails ends here  -->
					
                    
					
					
                  </form>
                  
                  <div class="actionBar actions"><a onClick="cancelRsv()" class="btn btn-default">Cancel</a><a onClick="gotostepfour()" href="#" class="btn btn-success">Next</a></div>
                    </div>
					<div id="step-4">
                    
                      <form autocomplete="off" id="billingDataForm" data-parsley-validate class="form-horizontal form-label-left">
					 <h3 class="head borderrd">4. Payment</h3>
                	<div class="form-group col-md-6 col-sm-6 col-xs-12 padding-left-0">
                      <label class="control-label " for="roomCost">Total for Stay &#163;</label>
                        <input type="text" id="roomCost" name="roomCost" disabled="disabled" class="form-control ">
					</div>
                    <div class="clearfix"></div>
                    <div class="form-group col-md-6 col-sm-6 col-xs-12 padding-left-0">
                      <label class="control-label" for="tax">VAT &#163;</label>
                        <input type="text" id="tax" name="tax" class="form-control ">
					
                    </div>
                    <div class="clearfix"></div>
                    <div class="form-group col-md-6 col-sm-6 col-xs-12 padding-left-0">
                      <label class="control-label" for="otherChrgs">Other Charges &#163;</label>
                        <input type="text" id="otherChrgs" name="otherChrgs" class="form-control">
					    </div>
                        <div class="clearfix"></div>
                     <div class="form-group col-md-6 col-sm-6 col-xs-12 padding-left-0">
                      <label class="control-label" for="subTotal">Sub Total &#163;</label>
                          <input type="text" id="subTotal" name="subTotal" disabled="disabled" class="form-control ">
					
                    </div>
                    <div class="clearfix"></div>
                    <div class="form-group col-md-5 col-sm-6 col-xs-12 padding-left-0 ">
                      <label class="control-label " for="rebatePerc">Discount(%)</label>
                         <input type="text" id="rebatePerc" name="rebatePerc" class="form-control">
					  </div>
                      <div class="clearfix"></div>
                     <div class="searcharea">
                    <div class="form-group col-md-6 col-sm-6 col-xs-12  padding-left-0">
                      <label class="control-label" for="rebateAmount">Discount &#163;</label>
                        <input type="text" id="rebateAmount" name="rebateAmount" class="form-control">
		         	</div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="form-group col-md-6 col-sm-6 col-xs-12 padding-left-0">
                      <label class="control-label sechead" for="grandTotal">Grand Total &#163;</label>
                        <input type="text" id="grandTotal" name="grandTotal" disabled="disabled" class="form-control">
					
                    </div>
                    <div class="clearfix"></div>
                    <div class="form-group col-md-6 col-sm-6 col-xs-12 padding-left-0">
                      <label class="control-label">Payment Method</label>
                        <select class="form-control" id="payMethod" >
                        	<option value="Cash">Cash</option>
                        	<option value="Checque">Cheque</option>
                        	<option value="Credit Card">Credit Card</option>
                           
                        </select>
                      
                    </div>
                    <div class="clearfix"></div>
                    <div class="form-group col-md-6 col-sm-6 col-xs-12 padding-left-0">
                      <label class="control-label" for="payPaid">Payment Paid &#163;<span class="required">*</span></label>
                         <input type="text" id="payPaid" name="payPaid" required class="form-control">
						
                     
                    </div>
                    <div class="clearfix"></div>
                    <div class="form-group col-md-6 col-sm-6 col-xs-12 padding-left-0">
                      <label class="control-label" for="balance">Balance &#163;<span class="required">*</span></label>
                        <input type="text" id="balance" name="balance" disabled="disabled" class="form-control ">
					
                    </div>
                    
                    
					
					
                  </form>
                  
                  <div class="actionBar actions"><a onClick="cancelRsv()" class="btn btn-default">Cancel</a><a onClick="finishRsv()" class="btn btn-default" style="display: block;">Save Payment &amp; Show Invoice</a><a href="#" onClick="gotostepthree()" class="btn btn-primary" style="display: inline-block;">Previous</a></div>
                    </div>
                    

                  </div>
                  </div>
                  <!-- End SmartWizard Content -->
                
              </div>
			  <!--End Wizard Div -->  
								 
        
        
        
        <!-- Large modal -->
                
                <div class="modal fade bs-example-modal-lg" id="guestUpdationForm"tabindex="-1" role="dialog" aria-hidden="true">
                  <div class="modal-dialog modal-lg">
                    <div class="modal-content col-xs-12">

                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                        </button>
                        <h4 class="modal-title" id="myModalLabel">Update Guest Data</h4>
                      </div>
                      <div class="modal-body">
								 <form id="hotelFloorUpdationForm" data-parsley-validate class="form-horizontal form-label-left">
								<input type="hidden" id="guestId" value="W3Schools">
								
								
					<div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="checkIn">Guest Check-in <span class="required">*</span></label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" id="checkIn" name="checkIn" required class="form-control col-md-7 col-xs-12">
						
                      </div>
                    </div>
					<div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="fname1">First Name <span class="required">*</span></label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" id="fname1" name="fname1" required class="form-control col-md-7 col-xs-12">
						
                      </div>
                    </div>
					<div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12"  for="lname1">Last Name <span class="required">*</span></label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" id="lname1" name="lname1" required class="form-control col-md-7 col-xs-12">
                      </div>
                    </div>
					
					
					<div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12"  for="cellno1">Mobile Number <span class="required">*</span></label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" id="cellno1" pattern="/^(?=.*[0-9])[- +()0-9]+$/" name="cellno1" required class="form-control col-md-7 col-xs-12">
                      </div>
                    </div>
					<div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12"  for="landline1">Telephone Number </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" id="landline1" name="landline1" class="form-control col-md-7 col-xs-12">
                      </div>
                    </div>
					<div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12"  for="email1">Email </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="email" id="email1" name="email1" class="form-control col-md-7 col-xs-12">
                      </div>
                    </div>
					<div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12">Gender</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <select class="form-control" id="gender1" >
                        <option value="Prefer not to say">Prefer not to say</option>
							<option value="Male">Male</option>
							<option value="Female">Female</option>
                           
                        </select>
                      </div>
                    </div>
					<div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12"  for="age1">Age </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" id="age1" name="age1" class="form-control col-md-7 col-xs-12">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12"  for="street1">Street</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" id="street1" name="street1" class="form-control col-md-7 col-xs-12">
                      </div>
                    </div>
					
					<div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12"  for="city1">Town/City <span class="required">*</span></label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" id="city1" name="city1" required class="form-control col-md-7 col-xs-12">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12"  for="county1">County<span class="required">*</span></label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" id="county1" name="county1" required class="form-control col-md-7 col-xs-12">
                      </div>
                    </div>
					<div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12"  for="postcode1">Post Code<span class="required">*</span></label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" id="postcode1" name="postcode1" required class="form-control col-md-7 col-xs-12">
                      </div>
                    </div>
					
					<div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12"  for="country1">Country <span class="required">*</span></label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" id="country1" name="country1" required class="form-control col-md-7 col-xs-12">
                      </div>
                    </div>
					<div class="form-group">
                      <label for="comments1" class="control-label col-md-3 col-sm-3 col-xs-12">Comments</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <textarea class="resizable_textarea form-control" style="width: 100%; overflow: hidden; word-wrap: break-word; resize: horizontal; height: 87px;" id="comments1" name="comments1" placeholder="Any comments to your booking?" ></textarea> 
                      </div>
                    </div>
					<div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12">Assign Reserved Rooms</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <select class="form-control" id="cstmRsvdRoms1" >
                           
                        </select>
                    </div>
                    </div>
					
								
						  </form>
                
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" onClick="updateGuestInfo()"class="btn btn-primary" data-dismiss="modal">Save changes</button>
                      </div>

                    </div>
                  </div>
                </div>
		   <!--end modal -->

        <!-- footer content -->
        <footer>
          <div class="copyright-info">
            <p class="pull-right">All Rights Reserved by <a href="http://agiledevstorm.com">Agile Dev Storm</a>
            </p>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->

      </div>

    </div>
  </div>
  
  <!-- Large modal Rooms Updation Form -->
                
                <div class="modal fade bs-example-modal-lg" id="roomsUpdationForm"tabindex="-1" role="dialog" aria-hidden="true">
                  <div class="modal-dialog modal-lg">
                    <div class="modal-content col-xs-12">

                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                        </button>
                        <h4 class="modal-title" id="myModalLabel">Update Room Category</h4>
                      </div>
                      <div class="modal-body">
								 <form id="roomUpdationForm" data-parsley-validate class="form-horizontal form-label-left">
								<input type="hidden" id="roomId" value="W3Schools">
								<div class="form-group">
									<label class="control-label col-md-3 col-sm-3 col-xs-12">Room Category</label>
									<div class="col-md-6 col-sm-6 col-xs-12">
										<select class="form-control" id="roomcategory1" >
										   
										</select>
								  </div>
								</div>
								<div class="form-group">
								  <label class="control-label col-md-3 col-sm-3 col-xs-12">Hotel Floor</label>
								  <div class="col-md-6 col-sm-6 col-xs-12">
									<select class="form-control" id="hotelfloor1" >
									   
									</select>
								  </div>
								</div>
								<div class="form-group">
								  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="roomno1">Room No. <span class="required">*</span>
								  </label>
								  <div class="col-md-6 col-sm-6 col-xs-12">
									<input type="text" id="roomno1" name="roomno1" required class="form-control col-md-7 col-xs-12">
								  </div>
								</div>
								<div class="form-group">
								  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="beds1">No. of Beds <span class="required">*</span>
								  </label>
								  <div class="col-md-6 col-sm-6 col-xs-12">
									<input type="text" id="beds1" name="beds1" required class="form-control col-md-7 col-xs-12">
								  </div>
								</div>
								<div class="form-group">
								  <label class="control-label col-md-3 col-sm-3 col-xs-12">Room Status</label>
								  <div class="col-md-6 col-sm-6 col-xs-12">
									<select class="form-control" id="roomStatus1" >
									   <option value="ready">Ready</option>
									   <option value="maintenance">Under Maintenance</option>
									</select>
								  </div>
								</div>
								<div class="form-group">
								  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="roomrate1">Room Rate <span class="required">*</span>
								  </label>
								  <div class="col-md-6 col-sm-6 col-xs-12">
									<input type="text" id="roomrate1" name="roomrate1" required class="form-control col-md-7 col-xs-12">
								  </div>
								</div>
								
						  </form>
                
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" onClick="updateRoomInfo()"class="btn btn-primary" data-dismiss="modal">Save changes</button>
                      </div>

                    </div>
                  </div>
                </div>
		   <!--end modal -->
  
  <!-- Large modal Room Cetegory Updation Form -->
                
                <div class="modal fade bs-example-modal-lg" id="catgUpdationForm"tabindex="-1" role="dialog" aria-hidden="true">
                  <div class="modal-dialog modal-lg">
                    <div class="modal-content col-xs-12">

                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                        </button>
                        <h4 class="modal-title" id="myModalLabel">Update Room Category</h4>
                      </div>
                      <div class="modal-body">
								 <form id="hotelFloorUpdationForm" data-parsley-validate class="form-horizontal form-label-left">
								<input type="hidden" id="catgId" value="W3Schools">
								<div class="form-group">
								  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="catName1">Category Name<span class="required">*</span>
								  </label>
								  <div class="col-md-6 col-sm-6 col-xs-12">
									<input type="text" id="catName1" required class="form-control col-md-7 col-xs-12">
								  </div>
								</div>
								<div class="form-group">
								  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="catBeds1">No. of Beds<span class="required">*</span>
								  </label>
								  <div class="col-md-6 col-sm-6 col-xs-12">
									<input type="text" id="catBeds1" required class="form-control col-md-7 col-xs-12">
								  </div>
								</div>
								<div class="form-group">
								  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="catDesc1">Description <span class="required">*</span>
								  </label>
								  <div class="col-md-6 col-sm-6 col-xs-12">
									<input type="text" id="catDesc1" name="catDesc1" required class="form-control col-md-7 col-xs-12">
								  </div>
								</div>
								
						  </form>
                
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" onClick="updateCatgInfo()"class="btn btn-primary" data-dismiss="modal">Save changes</button>
                      </div>

                    </div>
                  </div>
                </div>
		   <!--end modal -->

  <div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
  </div>

  <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>

  <!-- bootstrap progress js -->
  <script src="<?php echo base_url();?>assets/js/progressbar/bootstrap-progressbar.min.js"></script>
  <script src="<?php echo base_url();?>assets/js/nicescroll/jquery.nicescroll.min.js"></script>
  <!-- icheck -->
  <script src="<?php echo base_url();?>assets/js/icheck/icheck.min.js"></script>
  <!-- tags -->
  <script src="<?php echo base_url();?>assets/js/tags/jquery.tagsinput.min.js"></script>
  <!-- switchery -->
  <script src="<?php echo base_url();?>assets/js/switchery/switchery.min.js"></script>
  <!-- daterangepicker -->
  <script type="text/javascript" src="<?php echo base_url();?>assets/js/moment/moment.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>assets/js/datepicker/daterangepicker.js"></script>
  <!-- richtext editor -->
  <script src="<?php echo base_url();?>assets/js/editor/bootstrap-wysiwyg.js"></script>
  <script src="<?php echo base_url();?>assets/js/editor/external/jquery.hotkeys.js"></script>
  <script src="<?php echo base_url();?>assets/js/editor/external/google-code-prettify/prettify.js"></script>
  <!-- select2 -->
  <script src="<?php echo base_url();?>assets/js/select/select2.full.js"></script>
  <!-- form validation -->
  <script type="text/javascript" src="<?php echo base_url();?>assets/js/parsley/parsley.min.js"></script>
  <!-- textarea resize -->
  <script src="<?php echo base_url();?>assets/js/textarea/autosize.min.js"></script>
  
  <!-- pace -->
  <script src="<?php echo base_url();?>assets/js/pace/pace.min.js"></script>
  
  <script src="<?php echo base_url();?>assets/js/custom.js"></script>
<!-- Datatables -->
  <script src="<?php echo base_url();?>assets/js/datatables/js/jquery.dataTables.js"></script>
  <script src="<?php echo base_url();?>assets/js/datatables/tools/js/dataTables.tableTools.js"></script>

  <!-- PNotify -->
  <script type="text/javascript" src="<?php echo base_url();?>assets/js/notify/pnotify.core.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>assets/js/notify/pnotify.buttons.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>assets/js/notify/pnotify.nonblock.js"></script>

  <!-- form wizard -->
  <script type="text/javascript" src="<?php echo base_url();?>assets/js/wizard/jquery.smartWizard.js"></script>
  
  
 <script>
 $('#childrens').change(function () {
	//alert("new");
   // $(this).find('option').css('color', '#ffffff');
   $(this).addClass('classOne');
   
})
 $('#infants').change(function () {
	
   $(this).addClass('classOne');
   
})

 </script>
 <style>
 #childrens, #infants{color:#767676 !important}
 #childrens.classOne, #infants.classOne{color:#000 !important;}
 </style>
 
  
  
</body>

</html>
