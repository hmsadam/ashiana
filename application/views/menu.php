
<?php ?>
  <div class="container body">


    <div class="main_container">

      <div class="col-md-3 left_col">
        <div class="left_col scroll-view">

          <div class="navbar nav_title" style="border: 0;">
            <a href="<?php echo page_url;?>dashboard" class="site_title"><img src="<?php echo base_url();?>assets/images/hms/msr_logo_rgb.png" alt="" class="logo"  /><img src="<?php echo base_url();?>assets/images/hms/logo_sm.png" alt="" class="logo logo2"  /></a>
            <h2 class="bookmg">Booking Manager</h2>
          </div>
          <div class="clearfix"></div>

          <!-- menu prile quick info -->
          <div class="profile">
            <div class="profile_pic">
              <img src="<?php echo base_url();?>/assets/images/blank-avatar.png" alt="..." class="img-circle profile_img">
            </div>
            <div class="profile_info">
              <span>Welcome,</span>
              <h2><?php echo $username; ?></h2>
              <h3>Admin</h3>
            </div>
          </div>
          <!-- /menu prile quick info -->

          <br />

          <!-- sidebar menu -->
          <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

            <div class="menu_section">
              
              <ul class="nav side-menu">
              
              	<li><a href="<?php echo page_url;?>dashboard"><i class="fa fa-tachometer"></i>Dashboard</a></li>
                <li><a href="<?php echo page_url;?>enquiries"><i class="fa fa-comments"></i>Enquiries</a></li>
                <li><a><i class="fa fa-edit"></i> Bookings <span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu" style="display: none">
                     <li><a href="<?php echo page_url;?>allreservations">Current Bookings</a>
                    </li>
                    <li><a href="<?php echo page_url;?>reserve">New Booking</a>
                    </li>                   
                    <li><a href="<?php echo page_url;?>grid">Calendar</a>
                    </li>
                    
                  </ul>
                </li>
                
                <li><a><i class="fa fa-building"></i> My Hotel <span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu" style="display: none">
                    <li><a href="<?php echo page_url;?>rooms">Rooms</a>
                    </li>
                    <li><a href="<?php echo page_url;?>floors">Floors</a>
                    </li>
                    <li ><a href="<?php echo page_url;?>newuser">Users</a>
                    </li>
                    
                  </ul>
                </li>
                
                <li ><a href="<?php echo page_url;?>localevents"><i class="fa fa-calendar"></i>Local Events</a></li>
               
              </ul>
            </div>
            
          </div>
          <!-- /sidebar menu -->

          <!-- /menu footer buttons -->
          <div class="sidebar-footer hidden-small" style="display: none;">
            <a  data-toggle="tooltip" data-placement="top" title="Settings">
              <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="FullScreen">
              <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Lock">
              <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Logout">
              <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
            </a>
          </div>
          <!-- /menu footer buttons -->
        </div>
      </div>

      <!-- top navigation -->
      <div class="top_nav">

        <div class="nav_menu">
          <nav class="" role="navigation">
            <div class="nav toggle">
              <a id="menu_toggle"><i class="fa fa-bars"></i></a>
            </div>

            <ul class="nav navbar-nav navbar-right">
              <li class="">
                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                  <img src="<?php echo base_url();?>assets/images/blank-avatar.png" alt=""><?php echo $username; ?>
                  <span class=" fa fa-angle-down"></span>
                </a>
                <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                  <li style="display: none;"><a href="javascript:;">  Profile</a>
                  </li>
                  <li style="display: none;">
                    <a href="javascript:;">
                      <span class="badge bg-red pull-right">50%</span>
                      <span>Settings</span>
                    </a>
                  </li>
                  <li style="display: none;">
                    <a href="javascript:;">Help</a>
                  </li >
                  <li><a onclick="clearlclstrg()" href="<?php echo page_url;?>login/logout_user"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                  </li>
                </ul>
              </li>

              <li role="presentation" class="dropdown" style="display: none;">
                <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                  <i class="fa fa-envelope-o"></i>
                  <span class="badge bg-green">6</span>
                </a>
                <ul id="menu1" class="dropdown-menu list-unstyled msg_list animated fadeInDown" role="menu">
                  <li>
                    <a>
                      <span class="image">
                                        <img src="<?php echo base_url();?>/assets/images/img.jpg" alt="Profile Image" />
                                    </span>
                      <span>
                                        <span>John Smith</span>
                      <span class="time">3 mins ago</span>
                      </span>
                      <span class="message">
                                        Film festivals used to be do-or-die moments for movie makers. They were where...
                                    </span>
                    </a>
                  </li>
                  <li>
                    <a>
                      <span class="image">
                                        <img src="<?php echo base_url();?>/assets/images/img.jpg" alt="Profile Image" />
                                    </span>
                      <span>
                                        <span>John Smith</span>
                      <span class="time">3 mins ago</span>
                      </span>
                      <span class="message">
                                        Film festivals used to be do-or-die moments for movie makers. They were where...
                                    </span>
                    </a>
                  </li>
                  <li>
                    <a>
                      <span class="image">
                                        <img src="<?php echo base_url();?>/assets/images/img.jpg" alt="Profile Image" />
                                    </span>
                      <span>
                                        <span>John Smith</span>
                      <span class="time">3 mins ago</span>
                      </span>
                      <span class="message">
                                        Film festivals used to be do-or-die moments for movie makers. They were where...
                                    </span>
                    </a>
                  </li>
                  <li>
                    <a>
                      <span class="image">
                                        <img src="<?php echo base_url();?>/assets/images/img.jpg" alt="Profile Image" />
                                    </span>
                      <span>
                                        <span>John Smith</span>
                      <span class="time">3 mins ago</span>
                      </span>
                      <span class="message">
                                        Film festivals used to be do-or-die moments for movie makers. They were where...
                                    </span>
                    </a>
                  </li>
                  <li>
                    <div class="text-center">
                      <a href="inbox.html">
                        <strong>See All Alerts</strong>
                        <i class="fa fa-angle-right"></i>
                      </a>
                    </div>
                  </li>
                </ul>
              </li>
			  
			  
              <li class="" style="display: none;">
                <a  href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                  Branches
                  <span class=" fa fa-angle-down"></span>
                </a>
                <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                  <li><a href="javascript:;">Main Branch Jain Mandar</a></li>
				  <li><a href="javascript:;">
					Gulberg Branch MM Alam Road</a>
                  </li>
				  </ul>
				  </li>
				  
				  <li class="">
							<select id="hotelId" name="select" style="display: none;">
						
						
						
                                    <?php foreach($branches as $key => $value) {?>
									<?php if($key == $this->session->userdata('hotelId')) {?>
									<option selected="selected" value="<?php echo $key;?>"><?php echo $value;?></option>
                                    <?php }
									else 
									{ ?>
									<option value="<?php echo $key;?>"><?php echo $value;?></option>
									<?php }}?>
                                </select>
				  </li>

            </ul>
          </nav>
        </div>

      </div>
      <!-- /top navigation -->
	  
	  

<script type="text/javascript">

function clearlclstrg(){
	localStorage.clear();
}


</script>