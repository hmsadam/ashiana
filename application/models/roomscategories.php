<?php

class roomscategories extends CI_Model {

  
	//this function is just for reference and not being used anywhere in the application
  
  function createRoomCatg($catgname, $beds, $desc, $branchid, $db, $userid)
  {
	$data['categoryname'] = $catgname;
    $data['noofbeds'] = $beds;
    $data['description'] = $desc;
    $data['hotelbranches_hotelbranchesid'] = $branchid;
    
    
   $data['createdon'] = date("Y-m-d H:i:s" ,time());
    
    $data['createdby'] = $userid; 
    
	
	$this->$db->trans_start();	
	$this->$db->insert('roomcategories',$data);
	$insert_id = $this->$db->insert_id();
	$this->$db->trans_complete();
	
	if ($this->$db->trans_status() === FALSE) {
    # Something went wrong.
    $this->$db->trans_rollback();
    return false;
	
	} 
	else {
		# Everything is Perfect. 
		# Committing data to the database.
		$this->$db->trans_commit();
		return true;
	}
  } //end function
  
  function getFloorByBranch($branchid, $db)
  {
	$this->$db->trans_start();
	$this->$db->from('roomcategories');
	$this->$db->where('hotelbranches_hotelbranchesid', $branchid);
    $catg = $this->$db->get()->result_array();
	$this->$db->trans_complete();
	  return $catg;
	  
  } //end function
  
  function updateRoomCatg($catgid, $catgname, $beds, $desc, $userid, $db)
  {
	$data['categoryname'] = $catgname;
    $data['noofbeds'] = $beds;
    $data['description'] = $desc;
    $data['lastmodifiedon'] = date("Y-m-d H:i:s" ,time());
     
   $data['lastmodifiedby'] = $userid; 
    
	
	$this->$db->trans_start();	
	$this->$db->where('roomcategoriesid', $catgid);
	$this->$db->update('roomcategories', $data);
	$this->$db->trans_complete();
	
	if ($this->$db->trans_status() === FALSE) {
    # Something went wrong.
    $this->$db->trans_rollback();
    return false;
	
	} 
	else {
		# Everything is Perfect. 
		# Committing data to the database.
		$this->$db->trans_commit();
		return true;
	}
	
  } //end function
  
 
  
} //end model class
