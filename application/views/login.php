<!DOCTYPE html>
<?php ?>
<!--[if gt IE 8]><!-->
<html class="no-js"><!--<![endif]--><head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Hotel Management System</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">		
        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
        <link href="<?php echo base_url();?>assets/css/css.css" rel="stylesheet" type="text/css">
        <!-- Include Fonts from the theme fonts directory -->
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/font-awesome.css">
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/weather-icons.css">      
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.css">        <!-- Optional theme -->
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-theme.css">
        <!-- Theme's own CSS file -->
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/main.css">
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/admin.css">

</head>
    <body class="ng-scope body-special" data-ng-app="app" id="app" data-custom-background="" data-off-canvas-nav="" data-ng-controller="AdminAppCtrl">
            <div class="view-container login">

                
                <!-- ngView:  --><section data-ng-view="" id="content" class="animate-fade-up ng-scope"><div class="page-signin ng-scope">
	

    <div class="main-body">
        <div class="container">
            <div class="form-container col-md-7 col-sm-7 col-xs-9 centerd loginp">
				<div class="col-md-12 col-sm-12 col-xs-12">
    				<a href="#"><img src="<?php echo base_url();?>assets/images/hms/msr_logo_rgb.png" alt="" class="logo" /></a>
    			</div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <h1 class="logohead">
                        Booking Manager
                    </h1>
                    <h3 class="logintitle">Login</h3>
       			</div>
                <!-- <form class="form-horizontal ng-pristine ng-valid"> -->
				<?php echo form_open('login/login_user') ?>
                    
                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                <label >
                                    Email Address
                                </label>
                                <input class="form-control" placeholder="Email" type="email" name="email">
                        </div>
                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                 <label>
                                    Password
                                </label>
                                <input class="form-control" placeholder="Password" type="password" name="password">
                        </div>
                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
							<button type="submit" name="submit" class="btn btn-success btn-block">Log in</button>                           
                        </div>
                                    
                                    
                                    <?php if($iserror) {
                                    	echo $error;
                                    }?>

                <section>
                    <p class="text-center"><a href="<?php echo page_url;?>forgotpassword">Forgot your password?</a></p>
                </section>
               
            </div>
        </div>
    </div>

</div></section>
            </div>
        <!--Uncomment for deployment using Grunt-->
    </body></html>