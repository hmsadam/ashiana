<?php

class clientgroups extends CI_Model {

  
	//this function is just for reference and not being used anywhere in the application
  
  function createNewClientGroup($groupname, $ownername, $ownercellno, $ownerlandline, $ownerntn, $ownercnic, $owneraddess, $dbname, $startdate, $servicetype, $rate)
  {
	  $data['groupname'] = $groupname;
    $data['ownername'] = $ownername;
    $data['ownercellno'] = $ownercellno; 
	$data['ownerlandline'] = $ownerlandline;
	$data['ownerntn'] = $ownerntn;
	$data['ownercnic'] = $ownercnic;
	
	$data['owneraddess'] = $owneraddess;
	$data['dbname'] = $dbname;
	$data['startdate'] = $startdate;
	
	$data['servicetype'] = $servicetype;
	$data['rate'] = $rate;
	
	$this->db1->trans_start();
	 $this->db1->insert('clientgroups',$data);
	 $insert_id = $this->db1->insert_id();
	 $this->db1->trans_complete();
	return $insert_id;
	  
  } //end function
  
  
  
  function getAllClientGroups()
  {
	 //$this->db1->select('clientgroupsid, groupname, dbname');
	$this->db1->trans_start();
	$this->db1->from('clientgroups');
    $clientGroups = $this->db1->get()->result_array();
	$this->db1->trans_complete();
	return $clientGroups;
	  
  } //end function
  
  function getLabShortName($labid)
  {
		$this->db->select('shortname');
		$this->db->from('labbranches');
		$this->db->where('labbranchid', $labid);
      $shortname = $this->db->get()->result_array();
	  return $shortname;
	  
  } //end function
  
  function getLabTitle($labid)
  {
		$this->db->select('labTitle');
		$this->db->from('labbranches');
		$this->db->where('labbranchid', $labid);
      $title = $this->db->get()->result_array();
	  return $title;
	  
  }

		
  
 

  
	function updateClientGroup($groupid, $groupname, $ownername, $ownercellno, $ownerlandline, $ownerntn, $ownercnic, $owneraddess, $dbname, $startdate, $enddate, $servicetype, $rate)
  {
	  $data['groupname'] = $groupname;
    $data['ownername'] = $ownername;
    $data['ownercellno'] = $ownercellno; 
	$data['ownerlandline'] = $ownerlandline;
	$data['ownerntn'] = $ownerntn;
	$data['ownercnic'] = $ownercnic;
	$data['owneraddess'] = $owneraddess;
	$data['dbname'] = $dbname;
	$data['startdate'] = $startdate;
	$data['enddate'] = $enddate;
	$data['servicetype'] = $servicetype;
	$data['rate'] = $rate;
	$this->db1->trans_start();
	$this->db1->where('clientgroupsid', $groupid);
	$this->db1->update('clientgroups', $data);
	$this->db1->trans_complete();
	return true;
	 
  } //end function
  
  
 
  
  
} //end model class
