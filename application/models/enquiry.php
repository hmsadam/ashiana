<?php

class enquiry extends CI_Model {

  
	//this function is just for reference and not being used anywhere in the application
  
  function createenquiry($name, $email, $teleno, $message, $branchid, $db)
  {
	$data['name'] = $name;
    $data['email'] = $email;
    $data['phone'] = $teleno;
    $data['message'] = $message;
    $data['hotelbranches_hotelbranchesid'] = $branchid;
    
   
    
	
	$this->$db->trans_start();	
	$this->$db->insert('enquiry',$data);
	$insert_id = $this->$db->insert_id();
	$this->$db->trans_complete();
	
	if ($this->$db->trans_status() === FALSE) {
    # Something went wrong.
    $this->$db->trans_rollback();
    return false;
	
	} 
	else {
		# Everything is Perfect. 
		# Committing data to the database.
		$this->$db->trans_commit();
		return true;
	}
  } //end function
  
  function getenquiries($branchid, $db)
  {
	$this->$db->trans_start();
	$this->$db->from('enquiry');
	$this->$db->where('hotelbranches_hotelbranchesid', $branchid);
    $floors = $this->$db->get()->result_array();
	$this->$db->trans_complete();
	  return $floors;
	  
  } //end function
  
  function deleteenquiry($id, $db)
  {
  	$this->$db->trans_start();
  	$this->$db->where('enquiryid', $id);
  	$this->$db->delete('enquiry');
  	$this->$db->trans_complete();
  	return true;
  
  } //end function
  
 
  
} //end model class
