<?php

class guesthasreservations extends CI_Model {

  function saveGuestReservation($rsvrid, $guestid, $userid, $db) {

    $data['guests_guestsid'] = $guestid;
    $data['reservation_reservationid'] = $rsvrid;
    
	
	$data['createdon'] = date("Y-m-d H:i:s" ,time());
   $data['createdby'] = $userid; 
	
	
	 $this->$db->trans_start();
	 $this->$db->insert('guesthasreservations',$data);
	 $insert_id = $this->$db->insert_id();
	 $this->$db->trans_complete();
	return $insert_id;

    
  } //end function

  function deleteGuest($id, $db)
  {
  	$this->$db->trans_start();
  	$this->$db->where('guests_guestsid', $id);
  	$this->$db->delete('guesthasreservations');
  	$this->$db->trans_complete();
  
  
  	return true;
  } //end function
  
  function checkParentGuestReservation($rsvid, $db) {
    // start building a query
    $this->$db->trans_start();
	//$this->$db->select('reservations_reservationsid');
	$this->$db->from('guesthasreservations');
     $this->$db->where( array('reservation_reservationid'=>$rsvid));
	$reservations = $this->$db->get()->result_array();
	$this->$db->trans_complete();
	return $reservations;
	
	
  } //end functioin
  
 
  function getGuestsDataByRsvId($reservationId, $db)
  {
  	$this->$db->trans_start();
  	$this->$db->select('guestsid, firstname, lastname, cellNo, landLineNo, email, gender, age, street, city, county, postcode, country, comments, isAFamilyMember,  guesthasreservations.reservation_reservationid');
  	$this->$db->from('guesthasreservations');
  	
  	//guestsstaysid, checkindatetime, checkoutdatetime, hotelroomsid, roomno,
  	
  
  	$this->$db->join('guests', 'guests.guestsid = guesthasreservations.guests_guestsid', 'left');
  	//$this->$db->join('guestshasstays', 'guestshasstays.guest_guestsid = guests.guestsid', 'left');
  	//$this->$db->join('guestsstays', 'guestsstays.guestsstaysid = guestshasstays.gueststays_gueststaysid', 'left');
  	//$this->$db->join('hotalrooms', 'hotalrooms.hotelroomsid = guestsstays.hotelrooms_hotelroomsid', 'left');
  
  	$this->$db->where('guesthasreservations.reservation_reservationid', $reservationId);
  
  	$result = $this->$db->get()->result_array();
  	$this->$db->trans_complete();
  	return $result;
  	 
  	 
  } //end function
  
  

  
} //end class
