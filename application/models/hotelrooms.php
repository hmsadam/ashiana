<?php

class hotelrooms extends CI_Model {

  
  
  function createNewHotelRoom($roomcategory, $hotelfloor, $roomno, $roomstatus, $adultbeds,  $childbeds, $infantbeds, $portablebeds, $branchid, $description, $db, $userid)
  {
	$data['hotelfloors_hotelfloorsid'] = $hotelfloor;
    $data['roomno'] = $roomno;
    $data['currentstatus'] = $roomstatus;
    $data['hotelbranches_hotelbranchesid'] = $branchid;
    
    $data['roomcategories_roomcategoriesid'] = $roomcategory;
    $data['adultbeds'] = $adultbeds;
    $data['childbeds'] = $childbeds;
    $data['infantbeds'] = $infantbeds;
    $data['portablebeds'] = $portablebeds;
    $data['description'] = $description;
    
   $data['createdon'] = date("Y-m-d H:i:s" ,time());
    
    $data['createdby'] = $userid; 
    
	
	$this->$db->trans_start();	
	$this->$db->insert('hotalrooms',$data);
	$insert_id = $this->$db->insert_id();
	$this->$db->trans_complete();
	
	if ($this->$db->trans_status() === FALSE) {
    # Something went wrong.
    $this->$db->trans_rollback();
    return false;
	
	} 
	else {
		# Everything is Perfect. 
		# Committing data to the database.
		$this->$db->trans_commit();
		return true;
	}
  } //end function
  
  function getRoomsByBranch($branchid, $db)
  {
	
	  $this->$db->trans_start();
	  $this->$db->select('hotelroomsid, roomno, currentstatus, hotalrooms.adultbeds, hotalrooms.childbeds, hotalrooms.infantbeds, hotalrooms.portablebeds, roomcategoriesid, categoryname, hotelfloorsid, hotalrooms.description, floorname');
	  //$this->$db->select('hotelroomsid, roomno');
	$this->$db->from('hotalrooms');
	$this->$db->join('roomcategories', 'roomcategories.roomcategoriesid = hotalrooms.roomcategories_roomcategoriesid','left');
	$this->$db->join('hotelfloors', 'hotelfloors.hotelfloorsid = hotalrooms.hotelfloors_hotelfloorsid','left');
	
	$this->$db->where('hotalrooms.hotelbranches_hotelbranchesid', $branchid); 
	
	$result = $this->$db->get()->result_array();
	$this->$db->trans_complete();				  	
		return $result;
	  
	  
  } //end function
  
  //this function is used to get rooms for getAvailable rooms 
  function getRoomsByBranchForReservation($branchid, $db)
  {
	
	  $this->$db->trans_start();
	  $this->$db->select('hotelroomsid, roomno, currentstatus, hotalrooms.adultbeds, roomcategoriesid, categoryname, hotelfloorsid, hotalrooms.description, floorname, roomvideo');
	  //$this->$db->select('hotelroomsid, roomno');
	$this->$db->from('hotalrooms');
	$this->$db->join('roomcategories', 'roomcategories.roomcategoriesid = hotalrooms.roomcategories_roomcategoriesid','left');
	$this->$db->join('hotelfloors', 'hotelfloors.hotelfloorsid = hotalrooms.hotelfloors_hotelfloorsid','left');
	
	
	$this->$db->where('hotalrooms.hotelbranches_hotelbranchesid', $branchid); 
	$this->$db->where('hotalrooms.currentstatus', 'ready'); 
	$result = $this->$db->get()->result_array();
	$this->$db->trans_complete();				  	
		return $result;
	  
	  
  } //end function
  
  function updateRoom($roomid, $catgid, $floorid, $roomno, $adultbeds, $childbeds, $infantbeds, $portablebeds, $status, $description, $userid, $db)
  {
	$data['hotelfloors_hotelfloorsid'] = $floorid;
    $data['roomno'] = $roomno;
    $data['currentstatus'] = $status;
   
    $data['roomcategories_roomcategoriesid'] = $catgid;
    $data['adultbeds'] = $adultbeds;
    $data['childbeds'] = $childbeds;
    $data['infantbeds'] = $infantbeds;
    $data['portablebeds'] = $portablebeds;
    $data['description'] = $description;
	
	
    $data['lastmodifiedon'] = date("Y-m-d H:i:s" ,time());
     
   $data['lastmodifiedby'] = $userid; 
    
	
	$this->$db->trans_start();	
	$this->$db->where('hotelroomsid', $roomid);
	$this->$db->update('hotalrooms', $data);
	$this->$db->trans_complete();
	
	if ($this->$db->trans_status() === FALSE) {
    # Something went wrong.
    $this->$db->trans_rollback();
    return false;
	
	} 
	else {
		# Everything is Perfect. 
		# Committing data to the database.
		$this->$db->trans_commit();
		return true;
	}
	
  } //end function
  
  function uploadvideopath($roomid, $videopath, $db)
  {
  	$data['roomvideo'] = $videopath;
  	
  	$this->$db->trans_start();
  	$this->$db->where('hotelroomsid', $roomid);
  	$this->$db->update('hotalrooms', $data);
  	$this->$db->trans_complete();
  
  	if ($this->$db->trans_status() === FALSE) {
  		# Something went wrong.
  		$this->$db->trans_rollback();
  		return false;
  
  	}
  	else {
  		# Everything is Perfect.
  		# Committing data to the database.
  		$this->$db->trans_commit();
  		return true;
  	}
  
  } //end function
  
  function getvideonames($db)
  {
  	$this->$db->trans_start();
  	 
  	$this->$db->select('roomvideo');
  	$this->$db->from('hotalrooms');
  	 
  	$roomimgs = $this->$db->get()->result_array();
  	$this->$db->trans_complete();
  	return $roomimgs;
  
  } //end function
  
  function getvideoname($roomid, $db)
  {
  	$this->$db->trans_start();
  
  	$this->$db->select('roomvideo');
  	$this->$db->from('hotalrooms');
  	$this->$db->where('hotelroomsid', $roomid);
  	$roomimgs = $this->$db->get()->result_array();
  	$this->$db->trans_complete();
  	return $roomimgs;
  
  } //end function
 
  
  //this function is to be used in reservation view where available rooms will be shown for reservation
  function getRoomsByCatg($catgid, $db)
  {
	
	  $this->$db->trans_start();
	  //$this->$db->select('hotelroomsid, roomno, currentstatus, roomrate, hotalrooms.noofbeds, roomcategoriesid, categoryname, hotelfloorsid, floorname');
	$this->$db->from('hotalrooms');
	//$this->$db->join('reservedrooms', 'reservedrooms.hotelrooms_hotelroomsid = hotalrooms.hotelroomsid','left');
	//$this->$db->join('guestsstays', 'guestsstays.hotelrooms_hotelroomsid != hotalrooms.hotelroomsid','left');
	//$this->$db->join('roomcategories', 'roomcategories.roomcategoriesid = hotalrooms.roomcategories_roomcategoriesid','left');
	//$this->$db->join('hotelfloors', 'hotelfloors.hotelfloorsid = hotalrooms.hotelfloors_hotelfloorsid','left');
	
	//$where = "hotalrooms.roomcategories_roomcategoriesid =".$catgid." AND hotalrooms.currentstatus='ready' ";
	
	//$this->$db->where('hotalrooms.roomcategories_roomcategoriesid', $catgid); 
	//$this->$db->where($where); 
	$result = $this->$db->get()->result_array();
	$this->$db->trans_complete();				  	
		return $result;
	  
	  
  } //end function
  
  function countNoOfRooms($branchid, $db)
  {
  
  	return $this->$db
  	->where('hotelbranches_hotelbranchesid', $branchid)
  	
  	->count_all_results('hotalrooms');
  	
  	 
  	 
  } //end function
 
	function getAllRooms($branchid, $db){
		//this function is used
		$this->$db->trans_start();
		$this->$db->select('hotelroomsid, roomno, currentstatus, hotalrooms.adultbeds, roomcategoriesid, categoryname, hotelfloorsid, floorname');
		
		$this->$db->from('hotalrooms');
		$this->$db->join('roomcategories', 'roomcategories.roomcategoriesid = hotalrooms.roomcategories_roomcategoriesid','left');
		$this->$db->join('hotelfloors', 'hotelfloors.hotelfloorsid = hotalrooms.hotelfloors_hotelfloorsid','left');
		
		$this->$db->where('hotalrooms.hotelbranches_hotelbranchesid', $branchid);
		
		$result = $this->$db->get()->result_array();
		$this->$db->trans_complete();
		return $result;
		
	} //end function
	
	
	
	
	function getFaultyRooms($branchid, $db)
	{
	//this function is used
		$this->$db->trans_start();
	
		$this->$db->select('categoryname, currentstatus, floorname, hotelroomsid, hotalrooms.adultbeds, roomno');
		$this->$db->from('hotalrooms');
	
		$this->$db->join('roomcategories', 'roomcategories.roomcategoriesid = hotalrooms.roomcategories_roomcategoriesid','left');
		$this->$db->join('hotelfloors', 'hotelfloors.hotelfloorsid = hotalrooms.hotelfloors_hotelfloorsid','left');
		$this->$db->where('currentstatus', 'maintenance');
		$this->$db->where('hotalrooms.hotelbranches_hotelbranchesid', $branchid);
	
		$result = $this->$db->get()->result_array();
		$this->$db->trans_complete();
		return $result;
			
			
	} //end function

	
	
	
	
} //end model class
