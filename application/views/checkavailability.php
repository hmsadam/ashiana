<?php ?>

<?php include("header.php")?>
<? //php include("menuforlandingpage.php")?>

<link rel="stylesheet" href="<?php echo base_url();?>assets/css/publicrsv.css">  
<link rel="stylesheet" href="<?php echo base_url();?>assets/js/timepickadon/jquery-ui-timepicker-addon.css"> 
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/hmscssfiles/dtablescolaps.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/hmscssfiles/jquery.dataTables.min.css">
 <script type="text/javascript" src="<?php echo base_url();?>assets/js/wizard/jquery.smartWizardfrpbl.js"></script>
  <script src="<?php echo base_url();?>assets/js/hmsappscripts/customnotificationsscript.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/js/hmsappscripts/publicrsrvtnscript.js" type="text/javascript"></script>
<link href="https://fonts.googleapis.com/css?family=Raleway&display=swap" rel="stylesheet">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/hmscssfiles/rsvwizard.css">  

  
  <link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery-ui.css">

 <link href="<?php echo base_url();?>assets/css/datepicker.css" rel="stylesheet">
<style>
#slctdromdiv3{width:100%;float:left;}
</style>
<body class="reservation">
<div class="maincont">
  <div class="cont">
  
<div class="header row public_head">
    <div class="col-md-3 col-sm-3 col-xs-12 ">
    		<a href="http://ashiana.net/"><img src="<?php echo base_url();?>assets/images/hms/msr_logo_rgb.png" alt="" class="logo"  /></a>
    </div>
    <div class="col-md-9 col-sm-9 col-xs-12  txt-right links">
    	<a class="linkback" href="http://ashiana.net/">BACK TO MAIN SITE</a>
    </div><!--
    <div class="nav_client_menu">
          <nav class="" role="navigation">
            <div class="nav toggle">
              <a id="menu_toggle"><i class="fa fa-bars"></i></a>
            </div>

            <ul class="nav navbar-nav navbar-right">
				  
				  <li class="">
						<a href="#" class="linkback" >BACK TO MAIN SITE</a>	
				  </li>

            </ul>
          </nav>
        </div>
-->

<nav class="navbar navbar-expand-lg navbar-light bg-light row">
  <button class="linkbutton" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler navbar-toggler-icon"><i class="fa fa-bars"></i></span>
  </button>

  <div class="collapse navbar-collapse row" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
       <div class="txt-right links_nav">
    	<a href="http://ashiana.net/" class="linkback" >BACK TO MAIN SITE</a>
        </div>
      </li>
    </ul>
  </div>
</nav>
</div>

      
      <!-- page content -->
    <!--div class="right_col" role="main" -->  
    <div role="main">
          <!-- start of data table -->
          <div class="row">

              
       <!--Start Wizard Div -->
			<div class="col-md-12 m-auto puplic_wiz" id="rsvwzrd">
               <div class="x_content">
					 <!-- Smart Wizard -->
                  <div id="wizard" class="col-md-8 col-sm-8 col-xs-11 m-auto form_wizard wizard_horizontal">
                    <ul class="wizard_steps ">
                      <li class="col-md-3 col-sm-3  col-xs-3  padding-right-0 ">
                        <a href="#step-1">
                          <span class="step_no"></span>
                          <span class="step_descr">
                                            
                                        </span>
                        </a>
                      </li>
                      <li class="col-md-3 col-sm-3  col-xs-3 padding-right-0 padding-left-0 ">
                        <a href="#step-2">
                          <span class="step_no"></span>
                          <span class="step_descr">
                                            
                                        </span>
                        </a>
                      </li>
                      <li class="col-md-3 col-sm-3  col-xs-3 padding-right-0 padding-left-0 ">
                        <a href="#step-3">
                          <span class="step_no"></span>
                          <span class="step_descr">
                                            
                                        </span>
                        </a>
                      </li>
					  <li class="col-md-3 col-sm-3  col-xs-3  padding-left-0 ">
                        <a href="#step-4">
                          <span class="step_no"></span>
                          <span class="step_descr">
                                            
                                        </span>
                        </a>
                      </li>
                      
                    </ul>
                     
                    <div id="step-1">
                      <h1 class="col-md-12 col-sm-12 col-xs-12">Check Room Availability</h1>
                      
                      <form id="chckavbtyfrmpblc" data-parsley-validate class="form-horizontal form-label-left" autocomplete="off">
					
					<div class="form-group">
                      
                      <div class="col-md-4 col-sm-4 col-xs-12 ">
                      <label class="control-label " >Check-in </label>
                        <input required type="text" id="startdate" name="startdate" placeholder="dd/mm/yyyy" class="form-control col-md-7 col-xs-12">
						 <label style="display: none;" id="chckinoterr">Cannot select same dates for Check-in and Checkout</label>
                      </div>
                      <div class="col-md-4 col-sm-4 col-xs-12 ">
                      	<label class="control-label" >Checkout </label>
                        <input required type="text" id="enddate" name="enddate" placeholder="dd/mm/yyyy" class="form-control col-md-7 col-xs-12">
                      </div>
                      
                     
                      
                    </div>
					<div class="form-group">
                      
                      <div class="col-md-4 col-sm-4 col-xs-12 ">
                        <label class="control-label col-md-12 col-sm-12 col-xs-12">Adults</label>
                        <select class="form-control" id="adults" required="required">
                        <option value="" disabled selected hidden>Select Adults</option>
                           <option value="1">1 Adult</option>
                           <option value="2">2 Adults</option>
                           <option value="3">3 Adults</option>
                           <option value="4">4 Adults</option>
                           <option value="5">5 Adults</option>
                           <option value="6">6 Adults</option>
                           <option value="7">7 Adults</option>
                           <option value="8">8 Adults</option>
                           <option value="9">9 Adults</option>
                           <option value="10">10 Adults</option>
                        </select>
                      </div>
                      <div class="col-md-4 col-sm-4 col-xs-6">
                      <label class="control-label col-md-12 col-sm-12 col-xs-12">Children</label>
                        <select class="form-control infachild" id="childrens">
                        
                           <option selected value="0">0 Children</option>
                           <option value="1">1 Child</option>
                           <option value="2">2 Children</option>
                           <option value="3">3 Children</option>
                           <option value="4">4 Children</option>
                           <option value="5">5 Children</option>
                           <option value="6">6 Children</option>
                           <option value="7">7 Children</option>
                           <option value="8">8 Children</option>
                           <option value="9">9 Children</option>
                           <option value="10">10 Children</option>
                        </select>
                      </div>
                      <div class="col-md-4 col-sm-4 col-xs-6">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Infants</label>
                        <select class="form-control" id="infants" >
                        
                           <option selected value="0">0 Infants</option>
                           <option value="1">1 Infant</option>
                           <option value="2">2 Infants</option>
                           <option value="3">3 Infants</option>
                           <option value="4">4 Infants</option>
                           <option value="5">5 Infants</option>
                           <option value="6">6 Infants</option>
                           <option value="7">7 Infants</option>
                           <option value="8">8 Infants</option>
                           <option value="9">9 Infants</option>
                           <option value="10">10 Infants</option>
                        </select>
                        
                      </div>
                    </div>
                    
                    <div class="form-group" >
                      <div class="col-md-12 col-sm-12 col-xs-12 ">
                        <input type="button" value=" View Availability"  id="viwavbtbtn" class="btn btn-success w100">
                                              </div>
                    </div>
                    <div class="rooms col-md-12 col-sm-12 col-xs-12" id="rooms">
                    	<h2 id="searchrslts" class='result col-md-12 col-sm-12 col-xs-12'>Search Results</h2>
                        <div class="" id="roomsuidiv"></div>
                    </div>
                
                    
                    
					
					
                  </form>

                    </div>
                    <div id="step-2">
                      <div id="slctdromdiv"></div>
                      <h1 class=" col-md-12 col-sm-12 col-xs-12 ">Enter Your Details</h1>
                      <form id="customerDataForm" data-parsley-validate class="form-horizontal form-label-left">
					
					<div class="form-group">
                      
                      <div class="col-md-6 col-sm-6 col-xs-12 ">
                      <label class="control-label col-md-6 col-sm-6 col-xs-12 " for="fname">First Name<span class="required">*</span></label>
                        <input type="text" id="fname" placeholder="Your First Name" name="fname" value="" required class="form-control col-md-6 col-xs-12 padding-right-0">
						
                      </div>
                      <div class="col-md-6 col-sm-6 col-xs-12  ">
                      <label class="control-label col-md-6 col-sm-6 col-xs-12"  for="lname">Last Name<span class="required">*</span></label>
                        <input type="text" id="lname" name="lname" required class="form-control col-md-6 col-xs-12 " placeholder="Your Last Name" value="">
                      </div>
                    </div>
                    
                    
					
					<div class="form-group">
                      
                      <div class="col-md-6 col-sm-6 col-xs-12 ">
	                    <label class="control-label  col-md-6 col-sm-6 col-xs-12 "  for="cellno">Mobile Number<span class="required">*</span></label>
                        <input pattern="/^(?=.*[0-9])[- +()0-9]+$/" value="" type="text" id="cellno" name="cellno" required class="form-control col-md-6 col-xs-12 " placeholder="Your mobile number" minlength="5" maxlength="15" >
                      </div>
                     
                      <div class="col-md-6 col-sm-6 col-xs-12 ">
                      	<label class="control-label  col-md-6 col-sm-6 col-xs-12"  for="landline">Telephone Number </label>
                        <input pattern="/^(?=.*[0-9])[- +()0-9]+$/" value="" type="text" id="landline" name="landline" class="form-control col-md-6 col-xs-12 " placeholder="Your telephone number" minlength="5" maxlength="15">
                      </div>
                    </div>
					<div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <label class="control-label  col-md-6 col-sm-6 col-xs-12 "  for="email">Email<span class="required">*</span> </label>
                        <input pattern="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{1,63}$" value="" type="email" id="email" name="email" required class="form-control col-md-6 col-xs-12 " placeholder="Your email address">
                      </div>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                      	<label class="control-label  col-md-6 col-sm-6 col-xs-12">Gender</label>
                        <select class="form-control " id="gender"  >
                        <option value="" disabled selected hidden>Select Gender</option>
                       		<option value="Prefer not to say">Prefer not to say</option>
							<option value="Male">Male</option>
							<option value="Female">Female</option>
                            
                           
                        </select>
                      </div>
                    </div>
					<div class="form-group">
                      
                      <div class="col-md-6 col-sm-6 col-xs-12 ">
						<label class="control-label  col-md-6 col-sm-6 col-xs-12 "  for="age">Age </label>
                        <input value="" type="text" id="age" name="age" class="form-control col-md-6 col-xs-12 " placeholder="Your age">
                      </div>
                    </div>
					<div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12 ">
                      <label class="control-label  col-md-6 col-sm-6 col-xs-12 "  for="street">Street<span class="required">*</span> </label>
                        <input required value="" type="text" id="street" name="street" class="form-control col-md-6 col-xs-12" placeholder="Street and street number">
                      </div>
                    
                      <div class="col-md-6 col-sm-6 col-xs-12 ">
                      <label class="control-label  col-md-6 col-sm-6 col-xs-12 "  for="city">Town/City<span class="required">*</span></label>
                        <input value="" pattern="/^[A-Za-z][A-Za-z\s]*$/" type="text" id="city" name="city" required class="form-control col-md-6 col-xs-12" placeholder="Town/City">
                      </div>
                    </div>
					<div class="form-group">

                      <div class="col-md-6 col-sm-6 col-xs-12 ">
                      <label class="control-label  col-md-6 col-sm-6 col-xs-12"  for="count">County<span class="required">*</span></label>
                        <input value="" type="text" pattern="/^[A-Za-z][A-Za-z\s]*$/" id="county" name="county" required class="form-control col-md-6 col-xs-12" placeholder="County">
                      </div>
                    
                      <div class="col-md-6 col-sm-6 col-xs-12 ">
                         <label class="control-label  col-md-6 col-sm-6 col-xs-12"  for="postcode">Post Code<span class="required">*</span></label>
                        <input value="" type="text" id="postcode" name="postcode" required class="form-control col-md-6 col-xs-12" placeholder="Post Code">
                      </div>
                    </div>
					<div class="form-group">

                      <div class="col-md-6 col-sm-6 col-xs-12 ">
                      <label class="control-label  col-md-6 col-sm-6 col-xs-12"  for="country">Country<span class="required">*</span></label>
                         <input value="" pattern="/^[A-Za-z][A-Za-z\s]*$/" type="text" id="country" name="country" required class="form-control col-md-6 col-xs-12" placeholder="Country"> 
                      </div>
                    </div>
					<input type="hidden" id="cstmRsvdRoms" value="n">
					
					<div class="form-group">

                      <div class="col-md-12 col-sm-12 col-xs-12  ">
                      <label for="comments" class="control-label col-md-12 col-sm-12 col-xs-12">Comments</label>
                        <textarea class="resizable_textarea form-control" style="width: 100%; overflow:min-height:100px; hidden; word-wrap: break-word; resize: horizontal; height: 87px;" id="comments" name="comments" placeholder="Any comments to your booking?" ></textarea> 
                      </div>
                    </div>
					  <div class="col-md-12 col-sm-12 col-xs-12 ">
                        <a href="javascript:void(0);" id="saveCustomer" class="btn btn-success col-md-12 col-sm-12 col-xs-12"> Submit My Details</a>
                           
                        
                    </div>
                    
                    </form>
					
					
                  

             
                    </div>
                    <div id="step-3">
                     <div id="slctdromdiv2" class="col-md-12 col-sm-12 col-xs-12"></div>
                     <div id="gstdatadiv" class="col-md-12 col-sm-12 col-xs-12"></div>
                    </div>
					<div id="step-4">
                      <h1 class="col-md-12 col-sm-12 col-xs-12">Confirmation</h1>
                     <div class="col-md-12 col-sm-12 col-xs-12">
                      
                      <div class="confmsg">
                      	<p>Thank you for your reservation, looking forward to seeing you.</p>
                      	<p>Please, print this page for your reference.<br /><br />
                        If you've got any questions, please <a href="http://ashiana.net/contact/">contact us</a>.</p>
                        <br /><br /><br />
                      	
                      </div>
                      
                      <div id="slctdromdiv3"></div>
                      <br /><br />
                      </div>
                    </div>
                    

                  </div>
                  </div>
                  <!-- End SmartWizard Content -->
                
              </div>
			  <!--End Wizard Div -->  
								 

          </div>
		  
		  <!-- end of data table -->
          <div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
  </div>
	</div>
	
		<!-- end page content -->
		
  
  
<div class="header col-md-12 footer">
<div class="col-md-6 col-sm-6 col-xs-12 no-padding txt-right fweight" style="font-size:16px;float:right">
    	<a href="http://ashiana.net/" >BACK TO MAIN SITE</a>
    </div>
<div class="col-md-6 col-sm-6 col-xs-12 no-padding cright " style="float:left;">
    		<label>&copy; 2019 <a href="https://marketsellrepeat.co.uk/">Market Sell Repeat.</a> All Rights Reserved.</label>
    </div>


    

</div>
</div>
  <script src="<?php echo base_url();?>/assets/js/bootstrap.min.js"></script>

  <!-- bootstrap progress js -->
  <script src="<?php echo base_url();?>assets/js/progressbar/bootstrap-progressbar.min.js"></script>
  <script src="<?php echo base_url();?>assets/js/nicescroll/jquery.nicescroll.min.js"></script>
  <!-- icheck -->
  <script src="<?php echo base_url();?>assets/js/icheck/icheck.min.js"></script>
  <!-- tags -->
  <script src="<?php echo base_url();?>assets/js/tags/jquery.tagsinput.min.js"></script>
  <!-- switchery -->
  <script src="<?php echo base_url();?>assets/js/switchery/switchery.min.js"></script>
  <!-- daterangepicker -->
  <script type="text/javascript" src="<?php echo base_url();?>assets/js/moment/moment.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>assets/js/datepicker/daterangepicker.js"></script>
  <!-- richtext editor -->
  <script src="<?php echo base_url();?>assets/js/editor/bootstrap-wysiwyg.js"></script>
  <script src="<?php echo base_url();?>assets/js/editor/external/jquery.hotkeys.js"></script>
  <script src="<?php echo base_url();?>assets/js/editor/external/google-code-prettify/prettify.js"></script>
  <!-- select2 -->
  <script src="<?php echo base_url();?>assets/js/select/select2.full.js"></script>
  <!-- form validation -->
  <script type="text/javascript" src="<?php echo base_url();?>assets/js/parsley/parsley.min.js"></script>
  <!-- textarea resize -->
  <script src="<?php echo base_url();?>assets/js/textarea/autosize.min.js"></script>
  <script>
    autosize($('.resizable_textarea'));
  </script>
  <!-- Datatables -->
  <script src="<?php echo base_url();?>assets/js/datatables/js/jquery.dataTables.js"></script>
  <script src="<?php echo base_url();?>assets/js/datatables/tools/js/dataTables.tableTools.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery-ui.js"></script>
  <!-- pace -->
  <script src="<?php echo base_url();?>assets/js/pace/pace.min.js"></script>
  
  <script src="<?php echo base_url();?>assets/js/custom.js"></script>
  
  <!-- form wizard -->
 
  
   
<script src="<?php echo base_url();?>assets/js/timepickadon/jquery-ui-timepicker-addon.js" type="text/javascript"></script> 

 <script>
 $('#childrens').change(function () {
	//alert("new");
   // $(this).find('option').css('color', '#ffffff');
   $(this).addClass('classOne');
   
})
 $('#infants').change(function () {
	
   $(this).addClass('classOne');
   
})

 </script>
 <style>
 #childrens, #infants{color:#767676 !important}
 #childrens.classOne, #infants.classOne{color:#000 !important;}
 </style>
</body>

</html>
