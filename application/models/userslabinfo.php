<?php

class userslabinfo extends CI_Model {

  

  function saveUsersLabInfo($jobTitle, $joiningDate, $resignDate, $db) {

	$data['jobtitle'] = $jobTitle;
    $data['joiningdate'] = $joiningDate;
    $data['resigndate'] = $resignDate;
		$this->$db->trans_start();
	 $this->$db->insert('userslabinfo',$data);
	 $insert_id = $this->$db->insert_id();
	 $this->$db->trans_complete();
			if($insert_id)
			{
				return $insert_id;
			} else 
			{
				return false;
			} //end else
  } //end function 
  
  function getUsersLabInfo($id) {
	$this->$db->trans_start();
    $this->db->from('userslabinfo');
    $this->db->where('userslabinfoid',$id );
		
	$info = $this->db->get()->result_array();
	$this->$db->trans_complete();
	return $info;
			
  } //end function
  
  function updateUsersLabInfo($id, $jobTitle, $joiningDate, $resignDate, $db) {

	$data['jobtitle'] = $jobTitle;
    $data['joiningdate'] = $joiningDate;
    $data['resigndate'] = $resignDate;
	$this->$db->trans_start();
	$this->$db->where('userslabinfoid', $id);
	$this->$db->update('userslabinfo', $data);
	$this->$db->trans_complete();
				return true;
		
  } //end function 
  
  
  
} //end class
