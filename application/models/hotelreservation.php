<?php

class hotelreservation extends CI_Model {

  
  function createReservation($branchid, $db) {
  
  //$labid = 1;
	$q1 = "Q1";
	$q2 = "Q2";
	$q3 = "Q3";
	$q4 = "Q4";
    
	//gettiing branch short name
	$this->load->model('hotelbranches');
	$hotelData = $this->hotelbranches->getHotelShortName($branchid, $db);
	$shortname;
	foreach($hotelData as $index => $arr)
	{
		foreach($arr as $key => $value){
		$shortname = $value;
		}//end inner loop
	} //end outer loop
	
	
	
	//generating auto incremented id
	
	$this->load->model('rsrvidgenerator');
	$generatedid = $this->rsrvidgenerator->generateId($db);
	
	$this->load->helper('date');
	$currentdate = date("Y-m-d",time());
	$updateEveryYear = date("Y",time());
	$quart1 = $updateEveryYear."-01-01";
	$quart2 = $updateEveryYear."-04-01";
	$quart3 = $updateEveryYear."-07-01";
	$quart4 = $updateEveryYear."-10-01";
	
	//$currentdate = "2015-12-31";
	$endyear = $updateEveryYear."-12-31";
	
	
	$customOrderId;
	if($currentdate >= $quart1 && $currentdate < $quart2)
	{
		$customOrderId = $shortname.$updateEveryYear.$q1."-".$generatedid;
	} else if($currentdate >= $quart2 && $currentdate < $quart3){
		
		$customOrderId = $shortname.$updateEveryYear.$q2."-".$generatedid;
	} else if($currentdate >= $quart3 && $currentdate < $quart4) {
		$customOrderId = $shortname.$updateEveryYear.$q3."-".$generatedid;
	} else if($currentdate >= $quart4 && $currentdate <= $endyear) {
		$customOrderId = $shortname.$updateEveryYear.$q4."-".$generatedid;
	}
	
	$data['reservationid'] = $customOrderId;
	
	$this->$db->trans_start();
	$this->$db->insert('reservation',$data);
	$this->$db->trans_complete();
	// $this->session->set_userdata(array('orderID'=>$customOrderId));
	return $customOrderId;

    //return $shortname;
  } //end function

  function deletereservation($rsvid, $db)
  {
		$this->$db->trans_start();
		$this->$db->where('reservationid', $rsvid);
		$this->$db->delete('reservation');
		$this->$db->trans_complete();			  	
		return true;
	  
  } //end function

 
 function getCompleteData($branchid, $db)
  {
	$startdate = '2016-05-16';
	$enddate = '2016-05-22';
	
	$this->$db->trans_start();
	/*$this->$db->select('reservationid, startdate, enddate');
	$this->$db->from('reservation');
	
	$this->$db->join('hotelbrancheshasreservations', 'hotelbrancheshasreservations.reservations_reservationsid = reservation.reservationid', 'left' );
	
	
	$this->$db->where('hotelbrancheshasreservations.hotelbranches_hotelbranchesid', $branchid); */
	/*$this->$db->query('SELECT DISTINCT reservationid
						FROM reservation
						WHERE reservationid IN
						(
						SELECT reservationid
						FROM reservation
						WHERE startdate <= "2016-05-16" AND enddate >= "2016-05-23"
						)');*/
	/*$this->$db->from('reservation');
	$this->$db->where("'reservationid' NOT IN (SELECT 'reservationid' FROM 'reservation' WHERE 'startdate' >= '" .$startdate. "' AND 'enddate' <= '" .$enddate."')");*/
	//$where = $startdate.' >= reservation.startdate AND '.$enddate.' <= reservation.enddate';
	/*$where = 'startdate >='.$startdate.' AND enddate <='.$enddate;
	$this->$db->from('reservation');
	$this->$db->where($where);*/
	//$query = "SELECT * FROM 'reservation' WHERE startdate >= '2016-05-16' AND enddate <= '2016-05-23'";
	//$result = $this->$db->query($query);
	//$result = $this->$db->get()->result_array();
	
	
	$this->$db->select('reservationid, startdate, enddate');
    $this->$db->from('reservation');
    $this->$db->where('(startdate BETWEEN "' . $startdate . '" AND "' . $enddate . '") OR (enddate BETWEEN "' . $startdate . '" AND "' . $enddate . '")');
	$this->$db->where('( "'. $startdate . '" BETWEEN startdate AND enddate  ) OR ( "'. $enddate . '" BETWEEN startdate AND enddate)');
	//$this->$db->where('reservation.hotelbranches_hotelbranchesid', $branchid);
    $query = $this->$db->get()->result_array();
	$this->$db->trans_complete();				  	
		return $query;
	  
  } //end function
  
  
  function getReservationBTWDates($start_date, $end_date, $db)
  {
	
	$start_date='2016-05-16';
	$end_date='2016-05-27';
	$this->$db->trans_start();
	$this->$db->select('*');
	$this->$db->from('reservation');
	
	$this->$db->where('date BETWEEN "'. date('Y-m-d', strtotime($start_date)). '" and "'. date('Y-m-d', strtotime($end_date)).'"');
	$result = $this->$db->get()->result_array();
	$this->$db->trans_complete();	
		
  }
  
  function getAllReservations()
  {
	$this->$db->trans_start();
	$this->$db->select('*');
	$this->$db->from('reservation');
	
	$result = $this->$db->get()->result_array();
	$this->$db->trans_complete();
	return $result;
		
  }
  
  function getActiveRsrvByDate($startdate, $enddate, $allActiveReservations, $db)
  {
		
	$this->$db->trans_start();
	
	foreach($allActiveReservations as $key => $value)
	{
		foreach($value as $key1 => $value1)
		{
		
			$this->$db->select('reservationid, startdate, enddate');
			$this->$db->from('reservation');
			$this->$db->where('status', "active");
			$this->$db->where('(startdate BETWEEN "' . $startdate . '" AND "' . $enddate . '") OR (enddate BETWEEN "' . $startdate . '" AND "' . $enddate . '")');
			$this->$db->where('( "'. $startdate . '" BETWEEN startdate AND enddate  ) OR ( "'. $enddate . '" BETWEEN startdate AND enddate)');
			
			/*$this->$db->where('((startdate BETWEEN "' . $startdate . '" AND "' . $enddate . '") OR (enddate BETWEEN "' . $startdate . '" AND "' . $enddate . '") AND reservationid ='.'"'.$value1.'")');

			$this->$db->where('(( "'. $startdate . '" BETWEEN startdate AND enddate  ) OR ( "'. $enddate . '" BETWEEN startdate AND enddate) AND reservationid ='.'"'.$value1.'")');*/

			
			$query = $this->$db->get()->result_array();
			
		} //end inner foreach
	} //end foreach
	
	$this->$db->trans_complete();				  	
		return $query;
	  
  } //end function
  
  
	
  function joinReserv($branchid, $startdate, $enddate, $db)
  {
	$status = "active";
	$this->$db->trans_start();
	$this->$db->select('reservationid, startdate, enddate, hotelbrancheshasreservations.status');
	$this->$db->from('reservation');
	
	$this->$db->where('( "'. $startdate . '" BETWEEN startdate AND enddate  ) OR ( "'. $enddate . '" BETWEEN startdate AND enddate)');
	$this->$db->where('(startdate BETWEEN "' . $startdate . '" AND "' . $enddate . '") OR (enddate BETWEEN "' . $startdate . '" AND "' . $enddate . '")');
	$this->$db->join('hotelbrancheshasreservations', 'hotelbrancheshasreservations.reservations_reservationsid = reservation.reservationid');
	$this->$db->where('(hotelbrancheshasreservations.status = '."'".$status."')");
	
	$this->$db->where('(hotelbrancheshasreservations.hotelbranches_hotelbranchesid ='."'".$branchid."')");
	
	$result = $this->$db->get()->result_array();
	
	
	$this->$db->trans_complete();				  	
		return $result;
  } //
  
  function updateReservationInfo($resvid, $stDate, $enDate, $duration, $noofrooms, $arrivaldate, $departdate, $totaladults, $totalchilds, $totalinfants, $totalcost, $userid, $db)
  {
  	$data['startdate'] = $stDate;
  	$data['enddate'] = $enDate;
  	$data['duration'] = $duration;
  	$data['noofroom'] = $noofrooms;
  	$data['currentlynoofrooms'] = $noofrooms;
  	$data['guestarrivaldate'] = $arrivaldate;
  	$data['gestdeparturedate'] = $departdate;
  	$data['totalreservedadults'] = $totaladults;
  	$data['totalreservedchilds'] = $totalchilds;
  	$data['totalinfants'] = $totalinfants;
  	$data['totalreservationcharges'] = $totalcost;
  	
  	
  	
  	$data['lastmodifiedon'] = date("Y-m-d H:i:s" ,time());
   	$data['lastmodifiedby'] = $userid;
  
  
  	$this->$db->trans_start();
  	$this->$db->where('reservationid', $resvid);
  	$this->$db->update('reservation', $data);
  	$this->$db->trans_complete();
  
  	if ($this->$db->trans_status() === FALSE) {
  		# Something went wrong.
  		$this->$db->trans_rollback();
  		return false;
  
  	}
  	else {
  		# Everything is Perfect.
  		# Committing data to the database.
  		$this->$db->trans_commit();
  		return true;
  	}
  } //end function
  
  function getResvData($rsvid, $db)
  {
  	
  	$this->$db->trans_start();
  	
  	$this->$db->from('reservation');
  
  	$this->$db->where('(reservation.reservationid ='."'".$rsvid."')");
  
  	$result = $this->$db->get()->result_array();
  
  	
  	$this->$db->trans_complete();
  	return $result;
  	
  	
  	
  } //
  
  function getnoofrooms($rsvid, $db)
  {
  	 
  	$this->$db->trans_start();
  	 
  	$this->$db->select('reservationid, noofroom, currentlynoofrooms');
  	$this->$db->from('reservation');
  
  	$this->$db->where('(reservation.reservationid ='."'".$rsvid."')");
  
  	$result = $this->$db->get()->result_array();
  
  	 
  	$this->$db->trans_complete();
  	return $result;
  	 
  	 
  	 
  } //
  
  function updatenoofrooms($rsrvid, $currentnoofrooms, $db){
  	
  	$data['currentlynoofrooms'] = $currentnoofrooms;
  	 
  	 
  	/*$data['lastmodifiedon'] = date("Y-m-d H:i:s" ,time());
  	$data['lastmodifiedby'] = $userid;*/
  	
  	
  	$this->$db->trans_start();
  	$this->$db->where('reservationid', $rsrvid);
  	$result = $this->$db->update('reservation', $data);
  	$this->$db->trans_complete();
  	
  	return $result;
  }
  
  
  function getphonebooking($rsvid, $db)
  {
  	 
  	$this->$db->trans_start();
  	$this->$db->select('lastmodifiedby');
  	$this->$db->from('reservation');
  
  	$this->$db->where('(reservation.reservationid ='."'".$rsvid."')");
  
  	$result = $this->$db->get()->result_array();
  
  	 
  	$this->$db->trans_complete();
  	return $result;
  	 
  	 
  	 
  } //
  
  
  
} //end class
