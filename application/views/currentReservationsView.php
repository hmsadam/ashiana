<?php ?>

<?php include("header.php")?>
<?php include("menu.php")?>

<script src="<?php echo base_url();?>assets/js/jquery-ui.js"></script>
  
  <link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery-ui.css">
 

<script src="<?php echo base_url();?>assets/js/timepickadon/jquery-ui-timepicker-addon.js" type="text/javascript"></script> 
<link rel="stylesheet" href="<?php echo base_url();?>assets/js/timepickadon/jquery-ui-timepicker-addon.css"> 

<link rel="stylesheet" href="<?php echo base_url();?>assets/css/hmscssfiles/dtablescolaps.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/hmscssfiles/jquery.dataTables.min.css">
<style>
table.dataTable.display tbody tr.odd>.sorting_1, table.dataTable.order-column.stripe tbody tr.odd>.sorting_1, table.dataTable.display tbody tr.odd>.sorting_1, table.dataTable.order-column.stripe tbody tr.even>.sorting_1{background-color:transparent;}
table.dataTable.stripe tbody tr.odd, table.dataTable.display tbody tr.odd{background-color:#fff;}
table.dataTable.stripe tbody tr.even, table.dataTable.display tbody tr.even{background-color:#f9f9f9;}
table.dataTable.hover tbody tr:hover, table.dataTable.display tbody tr:hover{background-color:#e3f2f7}
table.dataTable.display tbody tr:hover>.sorting_1, table.dataTable.order-column.hover tbody tr:hover>.sorting_1{background-color:transparent !important;}
table.trans tr:hover{background:#ffffff !important;}
</style>
<body class="nav-md">

  

      
      <!-- page content -->
      <div class="right_col" role="main">
        <div class="">

          <div class="page-title">
            <div class="title_left">
              <h2>Current Bookings</h2>
            </div>
            
          </div>
        
          <!-- start of data table -->
          <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h3>Manage Current Bookings</h3>
                </div> 
                <div class="filters fresults">
                	<div class="form-group ">
                      <label class="control-label col-md-3 col-sm-3 col-xs-3 " style="    padding-left: 0px;margin-top: 12px;">Filter Results</label>
                      <div class="col-md-5 col-sm-5 col-xs-5" style="  ">
                        <select class="form-control" id="status" onChange="updateReservationsTable()">
                          <option value="all">All</option>
                          <option value="active" selected>Active</option>
                          <option value="complete">Complete</option>
                          <option value="confirmed">Confirmed</option>
                          <option value="pending">Pending</option>
                          <option value="cancelled">Cancelled</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-4 updatereserv">
                    
                          <label id="rsvonoflbl">
                             Update Reservations Off
                          </label>
                          <input id="updatersvrtnschbx"  type="checkbox" class="js-switch" />
                        </div>
                </div>
                <div class="x_content">
                  <table id="rsvTable" class="display">
                    <thead>
                      <tr class="headings">
                        
                        <th></th>
                        <th>Booking ID</th>
                        <th>Booked</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Price</th>
                        <th>Status</th>
                        
                        
                        
                        
                      </tr>
                    </thead>
                  </table>
                </div>
              </div>
            </div>

          </div>
		  
		  <!-- end of data table -->
		  
	
		  
		  
		  <!-- Large modal -->
                
                <div class="modal fade bs-example-modal-lg" id="guestUpdationForm"tabindex="-1" role="dialog" aria-hidden="true">
                  <div class="modal-dialog modal-lg">
                    <div class="modal-content col-xs-12">

                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                        </button>
                        <h4 class="modal-title" id="myModalLabel">Update Guest Data</h4>
                      </div>
                      <div class="modal-body">
								 <form id="hotelFloorUpdationForm" data-parsley-validate class="form-horizontal form-label-left">
								<input type="hidden" id="guestId" value="W3Schools">
								<input type="hidden" id="guestStayId" value="W3Schools">
								<input type="hidden" id="rsvnsid" value="W3Schools">
								<input type="hidden" id="ttlnorms" value="W3Schools">
								<input type="hidden" id="crntnorms" value="W3Schools">
								
					<div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="checkIn">Guest Check-in <span class="required">*</span></label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" id="checkIn" name="checkIn" required class="form-control col-md-7 col-xs-12">
						
                      </div>
                    </div>
                    <input type="hidden" id="checkOut" value="W3Schools">
                    <div class="form-group" style="display: none;">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="asdf">Guest Checkout <span class="required">*</span></label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" id="checkOut" name="dfas" required class="form-control col-md-7 col-xs-12">
						
                      </div>
                    </div>
					<div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="fname1">First Name <span class="required">*</span></label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" id="fname1" name="fname1" required class="form-control col-md-7 col-xs-12">
						
                      </div>
                    </div>
					<div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12"  for="lname1">Last Name <span class="required">*</span></label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" id="lname1" name="lname1" required class="form-control col-md-7 col-xs-12">
                      </div>
                    </div>
					
					<div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12"  for="cellno1">Cell No <span class="required">*</span></label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" id="cellno1" name="cellno1" required class="form-control col-md-7 col-xs-12">
                      </div>
                    </div>
					<div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12"  for="landline1">Land Line No. </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" id="landline1" name="landline1" class="form-control col-md-7 col-xs-12">
                      </div>
                    </div>
					<div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12"  for="email1">E-Mail </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="email" id="email1" name="email1" class="form-control col-md-7 col-xs-12">
                      </div>
                    </div>
					<div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12">Gender</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <select class="form-control" id="gender1" >
							<option value="Male">Male</option>
							<option value="Female">Female</option>
							<option value="Female">Female</option>
							<option value="Prefer not to say">Prefer not to say</option>
                           
                        </select>
                      </div>
                    </div>
					<div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12"  for="age1">Age </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" id="age1" name="age1" class="form-control col-md-7 col-xs-12">
                      </div>
                    </div>
					<div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12"  for="street1">Street </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" id="street1" name="street1" class="form-control col-md-7 col-xs-12">
                      </div>
                    </div>
					<div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12"  for="city1">City/Town <span class="required">*</span></label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" id="city1" name="city1" required class="form-control col-md-7 col-xs-12">
                      </div>
                    </div>
					<div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12"  for="county1">County <span class="required">*</span></label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" id="county1" name="county1" required class="form-control col-md-7 col-xs-12">
                      </div>
                    </div>
					<div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12"  for="postalcode1">Postal Code <span class="required">*</span></label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" id="postalcode1" name="postalcode1" required class="form-control col-md-7 col-xs-12">
                      </div>
                    </div>
					<div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12"  for="country1">Country <span class="required">*</span></label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" id="country1" name="country1" required class="form-control col-md-7 col-xs-12">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12"  for="comments1">Guest Booking Comments </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <textarea class="resizable_textarea form-control" style="width: 100%; overflow: hidden; word-wrap: break-word; resize: horizontal, vertical; height: 87px;" id="comments1" name="comments1" placeholder="Any comments to your booking?" ></textarea> 
                      </div>
                    </div>
					<input type="hidden" id="cstmRsvdRoms" name="cstmRsvdRoms" value="">
					
					
								
						  </form>
                
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" onClick="updateGuestInfo()"class="btn btn-primary" data-dismiss="modal">Save changes</button>
                      </div>

                    </div>
                  </div>
                </div>
		   <!--end modal -->
		   


          <script type="text/javascript">
            $(document).ready(function() {
              
            });
          </script>


    </div>
  </div>

  <div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
  </div>

  <script src="<?php echo base_url();?>/assets/js/bootstrap.min.js"></script>

  <!-- bootstrap progress js -->
  <script src="<?php echo base_url();?>assets/js/progressbar/bootstrap-progressbar.min.js"></script>
  <script src="<?php echo base_url();?>assets/js/nicescroll/jquery.nicescroll.min.js"></script>
  <!-- icheck -->
  <script src="<?php echo base_url();?>assets/js/icheck/icheck.min.js"></script>
  <!-- tags -->
  <script src="<?php echo base_url();?>assets/js/tags/jquery.tagsinput.min.js"></script>
  <!-- switchery -->
  <script src="<?php echo base_url();?>assets/js/switchery/switchery.min.js"></script>
  <!-- daterangepicker -->
  <script type="text/javascript" src="<?php echo base_url();?>assets/js/moment/moment.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>assets/js/datepicker/daterangepicker.js"></script>
  <!-- richtext editor -->
  <script src="<?php echo base_url();?>assets/js/editor/bootstrap-wysiwyg.js"></script>
  <script src="<?php echo base_url();?>assets/js/editor/external/jquery.hotkeys.js"></script>
  <script src="<?php echo base_url();?>assets/js/editor/external/google-code-prettify/prettify.js"></script>
  <!-- select2 -->
  <script src="<?php echo base_url();?>assets/js/select/select2.full.js"></script>
  <!-- form validation -->
  <script type="text/javascript" src="<?php echo base_url();?>assets/js/parsley/parsley.min.js"></script>
  <!-- textarea resize -->
  <script src="<?php echo base_url();?>assets/js/textarea/autosize.min.js"></script>
  <script>
    autosize($('.resizable_textarea'));
  </script>
  <!-- Datatables -->
  <script src="<?php echo base_url();?>assets/js/datatables/js/jquery.dataTables.js"></script>
  <script src="<?php echo base_url();?>assets/js/datatables/tools/js/dataTables.tableTools.js"></script>
  

  
  <!-- pace -->
  <script src="<?php echo base_url();?>assets/js/pace/pace.min.js"></script>
  
  <script src="<?php echo base_url();?>assets/js/custom.js"></script>
  
  <!-- PNotify -->
  <script type="text/javascript" src="<?php echo base_url();?>assets/js/notify/pnotify.core.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>assets/js/notify/pnotify.buttons.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>assets/js/notify/pnotify.nonblock.js"></script>

<script src="<?php echo base_url();?>assets/js/hmsappscripts/customnotificationsscript.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/js/hmsappscripts/currentrsvscript.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/js/hmsappscripts/currresvdatatablescript.js" type="text/javascript"></script> 

 
  
 
</body>

</html>
