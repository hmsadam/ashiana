<?php

class localevents extends CI_Model {

  
	//this function is just for reference and not being used anywhere in the application
  
  function createlocalevent($headline, $evntdate, $evnttext,  $evntposition, $branchid, $db)
  {
	$data['headline'] = $headline;
	$data['eventdate'] = $evntdate;
	$data['eventtext'] = $evnttext;
	$data['eventposition'] = $evntposition;
	$data['branchid'] = $branchid;
    
    
    
	
	$this->$db->trans_start();	
	$this->$db->insert('localevent',$data);
	$insert_id = $this->$db->insert_id();
	$this->$db->trans_complete();
	
	if ($this->$db->trans_status() === FALSE) {
    # Something went wrong.
    $this->$db->trans_rollback();
    return false;
	
	} 
	else {
		# Everything is Perfect. 
		# Committing data to the database.
		$this->$db->trans_commit();
		return true;
	}
  } //end function
  
  function getlocaleventsbybranch($branchid, $db)
  {
	$this->$db->trans_start();
	$this->$db->from('localevent');
	$this->$db->where('branchid', $branchid);
    $events = $this->$db->get()->result_array();
	$this->$db->trans_complete();
	  return $events;
	  
  } //end function
  
  function updatelocalevent($eventid, $headline, $evntdate, $evnttext,  $evntposition, $db)
  {
	
  	$data['headline'] = $headline;
  	$data['eventdate'] = $evntdate;
  	$data['eventtext'] = $evnttext;
  	$data['eventposition'] = $evntposition;

  	
  	
	$this->$db->trans_start();	
	$this->$db->where('localeventid', $eventid);
  	$this->$db->update('localevent', $data);
	$this->$db->trans_complete();
	
	if ($this->$db->trans_status() === FALSE) {
    # Something went wrong.
    $this->$db->trans_rollback();
    return false;
	
	} 
	else {
		# Everything is Perfect. 
		# Committing data to the database.
		$this->$db->trans_commit();
		return true;
	}
	
  } //end function
  
  function getevntimages($db)
  {
  	$this->$db->trans_start();
  
  	$this->$db->select('image');
  	$this->$db->from('localevent');
  
  	$roomimgs = $this->$db->get()->result_array();
  	$this->$db->trans_complete();
  	return $roomimgs;
  
  } //end function
  
  function getimgname($eventid, $db)
  {
  	$this->$db->trans_start();
  
  	$this->$db->select('image');
  	$this->$db->from('localevent');
  	$this->$db->where('localeventid', $eventid);
  	$roomimgs = $this->$db->get()->result_array();
  	$this->$db->trans_complete();
  	return $roomimgs;
  
  } //end function
  
  function uploadimagepath($eventid, $imgpath, $db)
  {
  	$data['image'] = $imgpath;
  	 
  	$this->$db->trans_start();
  	$this->$db->where('localeventid', $eventid);
  	$this->$db->update('localevent', $data);
  	$this->$db->trans_complete();
  
  	if ($this->$db->trans_status() === FALSE) {
  		# Something went wrong.
  		$this->$db->trans_rollback();
  		return false;
  
  	}
  	else {
  		# Everything is Perfect.
  		# Committing data to the database.
  		$this->$db->trans_commit();
  		return true;
  	}
  
  } //end function
  
  function deleteevent($id, $db)
  {
  	$this->$db->trans_start();
  	$this->$db->where('localeventid', $id);
  	$this->$db->delete('localevent');
  	$this->$db->trans_complete();
  	return true;
  
  } //end function
  
  
 
  
} //end model class
