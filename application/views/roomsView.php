
<?php ?>

<?php include("header.php")?>
<?php include("menu.php")?>

<script src="<?php echo base_url();?>assets/js/jquery-ui.js"></script>
  
  <link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery-ui.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/js/timepickadon/jquery-ui-timepicker-addon.css"> 


<body class="nav-md">

 
      
      <!-- page content -->
      <div class="right_col"  role="main">
        <div class="">

          <div class="page-title">
            <div class="title_left">
              <h2>Hotel Rooms</h2>
            </div>
            
          </div>
          <div class="clearfix"></div>
          

          <script type="text/javascript">
            $(document).ready(function() {
              $('#birthday').daterangepicker({
                singleDatePicker: true,
                calender_style: "picker_4"
              }, function(start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
              });
            });
          </script>


          <div class="row">
            <div class="col-md-12 col-xs-12">
              
              

              <div class="x_panel">
                <div class="x_title">
                  <h3>Manage Hotel Rooms </h3>
                  
                </div>
                <div class="x_content">
                  <table id="roomsTbl" class="table table-striped responsive-utilities jambo_table">
                    <thead>
                      <tr class="headings">
                        
                        <th>Room No.</th>
                        <th>Floor</th>
                        <th>Room Category</th>
                        <th style="min-width:110px;">Adult Beds</th>
                        <th>Status</th>
                       
                        
                        <th>Action</th>
                      </tr>
                    </thead>
                  </table>
                </div>
              </div>
			  
			    <!--start of rooms form -->
				<div class="x_panel">
                <div class="x_title">
                  <h3>Create Room</h3>
                </div>

                <div class="x_content">
                
                  <form id="roomCreationForm" data-parsley-validate class="form-horizontal form-label-left">
					<div class="form-group col-md-6 col-sm-6 col-xs-12 padding-left-0">
                      <label class="control-label " for="roomno">Room No./Name <span class="required">*</span>
                      </label>
                    
                        <input type="text" id="roomno" name="roomno" required class="form-control col-md-7 col-xs-12" placeholder="Enter Room No. or Room Name">
                    </div>
					<div class="clearfix"></div>
					<div class="form-group  col-md-6 col-sm-6 col-xs-12 padding-left-0">
                      <label class="control-label ">Floor</label>
                        <select class="form-control" id="hotelfloor" >
                           
                        </select>
                    </div>
                    <div class="clearfix"></div>
                    <div class="form-group  col-md-6 col-sm-6 col-xs-12 padding-left-0">
                      <label class="control-label ">Category</label>
                        <select class="form-control" id="roomcategory" >
                           
                        </select>
                    </div>
                    <div class="clearfix"></div>
                    <div class="form-group  col-md-6 col-sm-6 col-xs-12 padding-left-0">
                        <div class="col-md-6 col-sm-6 col-xs-12 padding-left-0">
                          <label class="control-label " for="adultbeds">Number of Adult Beds <span class="required">*</span>
                          </label>
                            <input min="1" type="number" id="adultbeds" name="adultbeds" required class="form-control col-md-7 col-xs-12" placeholder="Number of Adult Beds">
                        </div>
                        <div class="form-group  col-md-6 col-sm-6 col-xs-12 padding-right-0 pl0">
                      <label class="control-label" for="childbeds">Number of Child Beds </label>
                       <input min="0" type="number" id="childbeds" name="childbeds" class="form-control col-md-7 col-xs-12" placeholder="Number of Child Beds">
                      
                    </div>
                    </div>
                    
                    <div class="clearfix"></div>
                    <div class="form-group  col-md-6 col-sm-6 col-xs-12 padding-left-0">
                    <div class="col-md-6 col-sm-6 col-xs-12 padding-left-0">
                      <label class="control-label" for="infantbeds">Number of Infant Beds </label>
                       <input min="0" type="number" id="infantbeds" name="infantbeds" class="form-control col-md-7 col-xs-12" placeholder="Number of Infant Beds">
                       </div>
                       <div class="form-group  col-md-6 col-sm-6 col-xs-12 padding-right-0 pl0">
                      <label class="control-label" for="portablebeds">Number of Portable Beds </label>
                        <input min="0" type="number" id="portablebeds" name="portablebeds" class="form-control col-md-7 col-xs-12" placeholder="Number of Portable Beds">
                     </div>
                    </div>
                    
                   
                     <div class="clearfix"></div>
					<div class="form-group  col-md-6 col-sm-6 col-xs-12 padding-left-0">
                      <label class="control-label">Status</label>
                        <select class="form-control" id="roomStatus" >
                           <option value="ready">Ready</option>
                           <option value="maintenance">Under Maintenance</option>
                           
                        </select>
                    </div>
                    <div class="clearfix"></div>
                    <div class="form-group  col-md-6 col-sm-6 col-xs-12 padding-left-0">
	                  <label class="control-label">Room Description</label>
	        <textarea class="resizable_textarea form-control" style="width: 100%; height: 87px;" id="description" required name="description" ></textarea>
	                 
                	</div>
                    <div class="clearfix"></div>
					
                    <div class="ln_solid"></div>
                    <div class="form-group">
                      <div class="col-md-12 col-sm-12 col-xs-12 ">
                        
                        <input type="reset" value="Cancel" class="btn btn-default"> 
                        <a href="javascript:void(0);" id="saveRoom" class="btn btn-success" style="float:right;"> Create New Room</a>
                      </div>
                    </div>

                  </form>
                </div>
              </div>
			  <!--start of rooms form -->          


            </div>
            </div>
			<!-- start of 2nd Column -->
			<div class="row">
            <div class="col-md-12 col-xs-12">
                 <div class="x_panel">
                <div class="x_title">
                  <h3>Manage Room Categories</h3>
                </div>
                <div class="x_content">
                  <table id="roomCetgTbl" class="table table-striped responsive-utilities jambo_table">
                    <thead>
                      <tr class="headings">
                        
                        <th>Category Name</th>
                        <th>No. of Beds</th>
                        <th>Description</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                  </table>
                </div>
              </div>
			  <!--start of rooms category form -->
			  <div class="x_panel">
                <div class="x_title">
                  <h3>Create New Room Category</h3>
                 
                </div>

                <div class="x_content">
                 
                  <form id="roomCatgForm" data-parsley-validate class="form-horizontal form-label-left">
					
                    <div class="form-group col-md-6 col-sm-6 col-xs-12 padding-left-0 ">
                      <label class="control-label " for="catName">Category Name. <span class="required">*</span>
                      </label>
                        <input type="text" id="catName" name="catName" required class="form-control col-md-7 col-xs-12">
                     </div>
                     <div class="clearfix"></div>
                    <div class="form-group col-md-6 col-sm-6 col-xs-12 padding-left-0">
                      <label class="control-label " for="catBeds">No. of Beds <span class="required">*</span>
                      </label>
                        <input type="text" id="catBeds" name="catBeds" required class="form-control col-md-7 col-xs-12">
                    </div>
                    <div class="clearfix"></div>
					<div class="form-group col-md-6 col-sm-6 col-xs-12  padding-left-0">
                      <label class="control-label " for="catDesc">Description <span class="required">*</span>
                      </label>
                         <input type="text" id="catDesc" name="catDesc" required class="form-control col-md-7 col-xs-12">
                     </div>
                     <div class="clearfix"></div>
                    <div class="ln_solid"></div>
                    <div class="form-group">
                      <div class="col-md-12 col-sm-12 col-xs-12 padding-left-0">
                        
                        <input type="reset" value="Cancel" class="btn btn-default"> 
                        <a href="javascript:void(0);" id="saveCat" class="btn btn-success" style="float:right;"> Create Room Category</a>
                      </div>
                    </div>

                  </form>
                </div>
              </div>
            </div>
			
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="copyright-info">
            <p class="pull-right">2019 All rights reserved by <a href="https://agiledevstorm.com">Agile Dev Storm</a>
            </p>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->

      </div>

    </div>
  </div>
  
  <!-- Room Rates Manage Modal -->
                
                <div class="modal fade bs-example-modal-lg" id="roomratesmodal"tabindex="-1" role="dialog" aria-hidden="true">
                  <div class="modal-dialog modal-lg">
                    <div class="modal-content col-xs-12">

                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                        </button>
                        <h4 class="modal-title" id="romsrtsmodllbl">Manage Room Rates</h4>
                      </div>
                      <div class="modal-body">
								 <form id="roomratesmngform" data-parsley-validate class="form-horizontal form-label-left">
								<input type="hidden" id="roomidfrrates" value="W3Schools">
								
								
								<div class="form-group">
			                      <label class="control-label col-md-3 col-sm-3 col-xs-12" >From <span class="required">*</span></label>
			                      <div class="col-md-6 col-sm-6 col-xs-12">
			                        <input required type="text" id="ratefrom" name="ratefrom" class="form-control col-md-7 col-xs-12">
									
			                      </div>
			                    </div>
								<div class="form-group">
			                      <label class="control-label col-md-3 col-sm-3 col-xs-12" >To <span class="required">*</span></label>
			                      <div class="col-md-6 col-sm-6 col-xs-12">
			                        <input required type="text" id="rateto" name="rateto" class="form-control col-md-7 col-xs-12">
			                      </div>
			                    </div>
			                    
			                    <div class="form-group">
			                      <label class="control-label col-md-3 col-sm-3 col-xs-12" >Room Rate <span class="required">*</span></label>
			                      <div class="col-md-6 col-sm-6 col-xs-12">
			                        <input required type="text" id="roomrate" name="roomrate" class="form-control col-md-7 col-xs-12">
			                      </div>
			                    </div>
								
						  </form>
                
                      </div>
                      
                      <div class="form-group" >
					   <div class="col-md-12 col-sm-12 col-xs-12">
			                  <table id="ratestble" class="table table-striped responsive-utilities jambo_table dataTable no-footer" >
			                    <thead>
			                      <tr class="headings">
			                        		                        
			                        <th>Rate</th>
			                        <th>From</th>
			                        <th>To</th>
			                        <th>Action</th>
			                      </tr>
			                    </thead>
			                    <tbody id="roomratesdttblelist">
									
									
								</tbody>
			                  </table>
	                	</div>
                    </div>
                      
                      <div class="modal-footer">
                        <button type="button" onClick="destroyratestable()" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" id="saveratebtn" class="btn btn-primary" >Save Rate</button>
                      </div>

                    </div>
                  </div>
                </div>
		   <!--end modal -->
  
  <!-- room images upload modal -->
                
                <div class="modal fade bs-example-modal-lg" id="roomsimagesupload"tabindex="-1" role="dialog" aria-hidden="true">
                  <div class="modal-dialog modal-lg">
                    <div class="modal-content col-xs-12">

                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                        </button>
                        <h4 class="modal-title" id="romimgslbl">Add Room Images</h4>
                      </div>
                      <div class="modal-body">
                      
                      <!-- Media Gallery Starts Here -->
                      
                       <div class="row">
            <div class="col-md-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Rooms Gallery <small> All Images </small></h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                  <div class="row" id="imagesrow">

                    

                    
                    
                    

                  </div>

                </div>
              </div>
            </div>
          </div>
      
                      
                      
                      <!-- Media Gallery Ends Here -->
								 <form id="roomimgupldform" data-parsley-validate class="form-horizontal form-label-left"  method="get" action="" enctype="multipart/form-data">
								
								<input type="hidden" id="roomidfrimg" value="W3Schools">
								 <input required type="file" name="image" id="image">
								
						  </form>
                
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" id="uploadroomimagebutn" class="btn btn-primary"  >Save changes</button>
                      </div>

                    </div>
                  </div>
                </div>
		   <!--end images upload modal -->
  
  <!-- room video upload modal -->
                
                <div class="modal fade bs-example-modal-lg" id="roomsvideoupload"tabindex="-1" role="dialog" aria-hidden="true">
                  <div class="modal-dialog modal-lg">
                    <div class="modal-content col-xs-12">

                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                        </button>
                        <h4 class="modal-title" id="romimgslbl">Add Room Video</h4>
                      </div>
                      <div class="modal-body">
                      
                      <!-- Media Gallery Starts Here -->
                      
                       <div class="row">
            <div class="col-md-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Rooms Video </h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                  <div class="row" id="vidrow">

                    

                    
                    
                    

                  </div>

                </div>
              </div>
            </div>
          </div>
      
                      
                      
                      <!-- Media Gallery Ends Here -->
								 <form id="roomvidupldform" data-parsley-validate class="form-horizontal form-label-left"  method="post" action="" enctype="multipart/form-data">
								
								<input type="hidden" id="roomidfrvid" value="W3Schools">
								 <input required type="file" name="romvid" id="romvid">
								
						  </form>
                
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" id="uploadroomvidbutn" class="btn btn-primary"  >Save changes</button>
                      </div>

                    </div>
                  </div>
                </div>
		   <!--end video upload modal -->
  
  <!-- Large modal Rooms Updation Form -->
                
                <div class="modal fade bs-example-modal-lg" id="roomsUpdationForm"tabindex="-1" role="dialog" aria-hidden="true">
                  <div class="modal-dialog modal-lg">
                    <div class="modal-content col-xs-12">

                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                        </button>
                        <h4 class="modal-title" id="myModalLabel">Update Room</h4>
                      </div>
                      <div class="modal-body">
								 <form id="roomUpdationForm" data-parsley-validate class="form-horizontal form-label-left">
								<input type="hidden" id="roomId" value="W3Schools">
								<div class="form-group">
								  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="roomno1">Room No./Name <span class="required">*</span>
								  </label>
								  <div class="col-md-6 col-sm-6 col-xs-12">
									<input type="text" id="roomno1" name="roomno1" required class="form-control col-md-7 col-xs-12">
								  </div>
								</div>
								<div class="form-group">
								  <label class="control-label col-md-3 col-sm-3 col-xs-12">Hotel Floor</label>
								  <div class="col-md-6 col-sm-6 col-xs-12">
									<select class="form-control" id="hotelfloor1" >
									   
									</select>
								  </div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-3 col-sm-3 col-xs-12">Room Category</label>
									<div class="col-md-6 col-sm-6 col-xs-12">
										<select class="form-control" id="roomcategory1" >
										   
										</select>
								  </div>
								</div>
								
								 <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="adultbeds1">Number of Adult Beds <span class="required">*</span>
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input min="1" type="number" id="adultbeds1" name="adultbeds1" required class="form-control col-md-7 col-xs-12" placeholder="Number of Adult Beds">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="childbeds1">Number of Child Beds </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input min="0" type="number" id="childbeds1" name="childbeds1" class="form-control col-md-7 col-xs-12" placeholder="Number of Child Beds">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="infantbeds1">Number of Infant Beds </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input min="0" type="number" id="infantbeds1" name="infantbeds1" class="form-control col-md-7 col-xs-12" placeholder="Number of Infant Beds">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="portablebeds1">Number of Portable Beds </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input min="0" type="number" id="portablebeds1" name="portablebeds1" class="form-control col-md-7 col-xs-12" placeholder="Number of Portable Beds">
                      </div>
                    </div>
					
								
								<div class="form-group">
								  <label class="control-label col-md-3 col-sm-3 col-xs-12">Room Status</label>
								  <div class="col-md-6 col-sm-6 col-xs-12">
									<select class="form-control" id="roomStatus1" >
									   <option value="ready">Ready</option>
									   <option value="maintenance">Under Maintenance</option>
									</select>
								  </div>
								</div>
								<div class="form-group">
				                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Description</label>
				                  <div class="col-md-6 col-sm-6 col-xs-12">
				                    <textarea class="resizable_textarea form-control" style="width: 100%; overflow: hidden; word-wrap: break-word; resize: horizontal; height: 87px;" id="description1" required name="description1">
				                    </textarea>
				                  </div>
			                	</div>
								
								
								
						  </form>
                
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" onClick="updateRoomInfo()"class="btn btn-primary" data-dismiss="modal">Save changes</button>
                      </div>

                    </div>
                  </div>
                </div>
		   <!--end modal -->
  
  <!-- Large modal Room Cetegory Updation Form -->
                
                <div class="modal fade bs-example-modal-lg" id="catgUpdationForm"tabindex="-1" role="dialog" aria-hidden="true">
                  <div class="modal-dialog modal-lg">
                    <div class="modal-content col-xs-12">

                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                        </button>
                        <h4 class="modal-title" id="romctglbl">Update Room Category</h4>
                      </div>
                      <div class="modal-body">
								 <form id="hotelFloorUpdationForm" data-parsley-validate class="form-horizontal form-label-left">
								<input type="hidden" id="catgId" value="W3Schools">
								<div class="form-group">
								  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="catName1">Category Name<span class="required">*</span>
								  </label>
								  <div class="col-md-6 col-sm-6 col-xs-12">
									<input type="text" id="catName1" required class="form-control col-md-7 col-xs-12">
								  </div>
								</div>
								<div class="form-group">
								  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="catBeds1">No. of Beds<span class="required">*</span>
								  </label>
								  <div class="col-md-6 col-sm-6 col-xs-12">
									<input type="text" id="catBeds1" required class="form-control col-md-7 col-xs-12">
								  </div>
								</div>
								<div class="form-group">
								  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="catDesc1">Description <span class="required">*</span>
								  </label>
								  <div class="col-md-6 col-sm-6 col-xs-12">
									<input type="text" id="catDesc1" name="catDesc1" required class="form-control col-md-7 col-xs-12">
								  </div>
								</div>
								
						  </form>
                
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" onClick="updateCatgInfo()"class="btn btn-primary" data-dismiss="modal">Save changes</button>
                      </div>

                    </div>
                  </div>
                </div>
		   <!--end modal -->

  <div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
  </div>

  <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>

  <!-- bootstrap progress js -->
  <script src="<?php echo base_url();?>assets/js/progressbar/bootstrap-progressbar.min.js"></script>
  <script src="<?php echo base_url();?>assets/js/nicescroll/jquery.nicescroll.min.js"></script>
  <!-- icheck -->
  <script src="<?php echo base_url();?>assets/js/icheck/icheck.min.js"></script>
  <!-- tags -->
  <script src="<?php echo base_url();?>assets/js/tags/jquery.tagsinput.min.js"></script>
  <!-- switchery -->
  <script src="<?php echo base_url();?>assets/js/switchery/switchery.min.js"></script>
  <!-- daterangepicker -->
  <script type="text/javascript" src="<?php echo base_url();?>assets/js/moment/moment.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>assets/js/datepicker/daterangepicker.js"></script>
  <!-- richtext editor -->
  <script src="<?php echo base_url();?>assets/js/editor/bootstrap-wysiwyg.js"></script>
  <script src="<?php echo base_url();?>assets/js/editor/external/jquery.hotkeys.js"></script>
  <script src="<?php echo base_url();?>assets/js/editor/external/google-code-prettify/prettify.js"></script>
  <!-- select2 -->
  <script src="<?php echo base_url();?>assets/js/select/select2.full.js"></script>
  <!-- form validation -->
  <script type="text/javascript" src="<?php echo base_url();?>assets/js/parsley/parsley.min.js"></script>
  <!-- textarea resize -->
  <script src="<?php echo base_url();?>assets/js/textarea/autosize.min.js"></script>

<!-- dropzone -->
  <script src="<?php echo base_url();?>assets/js/dropzone/dropzone.js"></script>

  <script src="<?php echo base_url();?>assets/js/timepickadon/jquery-ui-timepicker-addon.js" type="text/javascript"></script>
  <!-- pace -->
  <script src="<?php echo base_url();?>assets/js/pace/pace.min.js"></script>
  
  <script src="<?php echo base_url();?>assets/js/custom.js"></script>
<!-- Datatables -->
  <script src="<?php echo base_url();?>assets/js/datatables/js/jquery.dataTables.js"></script>
  <script src="<?php echo base_url();?>assets/js/datatables/tools/js/dataTables.tableTools.js"></script>

  <!-- PNotify -->
  <script type="text/javascript" src="<?php echo base_url();?>assets/js/notify/pnotify.core.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>assets/js/notify/pnotify.buttons.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>assets/js/notify/pnotify.nonblock.js"></script>
<script src="<?php echo base_url();?>assets/js/hmsappscripts/customnotificationsscript.js" type="text/javascript"></script>

  
<script src="<?php echo base_url();?>assets/js/hmsappscripts/roomsscript.js" type="text/javascript"></script>
  
  
</body>

</html>
