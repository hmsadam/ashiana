<?php

class hotelbrancheshasrsvr extends CI_Model {

  function saveReservationsOfBranches($branchid, $rsvrid, $db) {

    $data['hotelbranches_hotelbranchesid'] = $branchid;
    $data['reservations_reservationsid'] = $rsvrid;
    $data['status'] = "active";
	
	
	
	 $this->$db->trans_start();
	 $this->$db->insert('hotelbrancheshasreservations',$data);
	 $insert_id = $this->$db->insert_id();
	 $this->$db->trans_complete();
	return $insert_id;

    
  } //end function

  function deletereservation($resvid, $db)
  {
		
	$this->$db->trans_start();
		$this->$db->where('reservations_reservationsid', $resvid);
		$this->$db->delete('hotelbrancheshasreservations');
	$this->$db->trans_complete();				  	
		return true;
	  
  } //end function
  
  function getRsrvByBranchId($branchid, $status, $db) {
    // start building a query
    $this->$db->trans_start();
	$this->$db->select('reservations_reservationsid');
	$this->$db->from('hotelbrancheshasreservations');
     $this->$db->where( array('hotelbranches_hotelbranchesid'=>$branchid, 'status'=>$status));
	$reservations = $this->$db->get()->result_array();
	$this->$db->trans_complete();
	return $reservations;
	
	
  } //end functioin
  
  function getCurrentRsvs($branchid, $status, $db) {
  	// start building a query
  	$this->$db->trans_start();
  	
  	
  	$this->$db->select('hotelbrancheshasreservationsid, hotelbranches_hotelbranchesid,
reservations_reservationsid, status, tblid, reservationid, reservation.startdate ,
reservation.enddate, duration, reservationdate, guestarrivaldate, gestdeparturedate, totalreservedadults,
totalreservedchilds, totalinfants, totalreservationcharges, firstname, lastname, cellNo, landLineNo, email, gender, age, street, city, county, postcode, country, isAFamilyMember, comments, guestsid, reservation.lastmodifiedby, reservation.noofroom, reservation.currentlynoofrooms');
  	
  	$this->$db->from('hotelbrancheshasreservations');
  	$this->$db->join('reservation', 'reservation.reservationid = hotelbrancheshasreservations.reservations_reservationsid');
  	$this->$db->join('guesthasreservations', 'guesthasreservations.reservation_reservationid = hotelbrancheshasreservations.reservations_reservationsid');
  	$this->$db->join('guests', 'guests.guestsid = guesthasreservations.guests_guestsid');
  	//$this->$db->join('guestshasstays', 'guestshasstays.guest_guestsid = guests.guestsid');
  	//$this->$db->join('guestsstays', 'guestsstays.guestsstaysid = guestshasstays.gueststays_gueststaysid');
  	$this->$db->where( array('hotelbranches_hotelbranchesid'=>$branchid, 'status'=>$status));
  	$reservations = $this->$db->get()->result_array();
  	$this->$db->trans_complete();
  	return $reservations;
  
  
  } //end functioin
  
  function getCurrentRsvsForStatusUpdate($branchid, $status, $db) {
  	
  	$this->$db->trans_start();
  	  	 
  	/*$this->$db->select('hotelbrancheshasreservationsid, hotelbranches_hotelbranchesid,
  	 reservations_reservationsid, status, tblid, reservationid, reservation.startdate ,
  	 reservation.enddate, duration, reservationdate, guestarrivaldate, gestdeparturedate, totalreservedadults,
  	 reservedroomsid, totalreservedchilds, hotelrooms_hotelroomsid, totalreservationcharges, reservation.noofroom,
  	 reservedrooms.startdate as rsrvdRoomStDt, reservedrooms.enddate as rsrvdRoomEnDt');*/
  	
  	$this->$db->select('hotelbrancheshasreservationsid, hotelbranches_hotelbranchesid,
  	 reservations_reservationsid, status, tblid, reservationid, reservation.startdate ,
  	 reservation.enddate, duration, reservationdate, guestarrivaldate, gestdeparturedate, totalreservedadults,
  	 totalreservedchilds, totalreservationcharges');
  	
  	 
  	$this->$db->from('hotelbrancheshasreservations');
  	$this->$db->join('reservation', 'reservation.reservationid = hotelbrancheshasreservations.reservations_reservationsid');
  	//$this->$db->join('reservedrooms', 'reservedrooms.reservation_reservationid = hotelbrancheshasreservations.reservations_reservationsid');
  	$this->$db->where( array('hotelbranches_hotelbranchesid'=>$branchid, 'status'=>$status));
  	$reservations = $this->$db->get()->result_array();
  	$this->$db->trans_complete();
  	return $reservations;
  
  
  } //end functioin
  
  function getAllCurrentRsvs($branchid, $db) {
  	
  	$this->$db->trans_start();
  
  	$this->$db->select('hotelbrancheshasreservationsid, hotelbranches_hotelbranchesid,
reservations_reservationsid, status, tblid, reservationid, reservation.startdate ,
reservation.enddate, duration, reservationdate, guestarrivaldate, gestdeparturedate, totalreservedadults,
totalreservedchilds, totalinfants, totalreservationcharges, firstname, lastname, cellNo, landLineNo, email, gender, age, street, city, county, postcode, country, isAFamilyMember, comments, guestsid, reservation.lastmodifiedby, reservation.noofroom, reservation.currentlynoofrooms');
  	 
  	$this->$db->from('hotelbrancheshasreservations');
  	$this->$db->join('reservation', 'reservation.reservationid = hotelbrancheshasreservations.reservations_reservationsid');
  	$this->$db->join('guesthasreservations', 'guesthasreservations.reservation_reservationid = hotelbrancheshasreservations.reservations_reservationsid');
  	$this->$db->join('guests', 'guests.guestsid = guesthasreservations.guests_guestsid');
  	//$this->$db->join('guestshasstays', 'guestshasstays.guest_guestsid = guests.guestsid');
  	//$this->$db->join('guestsstays', 'guestsstays.guestsstaysid = guestshasstays.gueststays_gueststaysid');
  	$this->$db->where( array('hotelbranches_hotelbranchesid'=>$branchid));
  	$reservations = $this->$db->get()->result_array();
  	$this->$db->trans_complete();
  	return $reservations;
  
  
  } //end functioin
  
  function getGuestsCheckedIn($branchid, $status, $db) {

  	$this->$db->trans_start();
  
  	$this->$db->from('hotelbrancheshasreservations');
  	
  	$this->$db->join('guestshasstays as ghs1', 'ghs1.reservation_reservationid = hotelbrancheshasreservations.reservations_reservationsid');
  	
  	$this->$db->where( array('hotelbranches_hotelbranchesid'=>$branchid, 'status'=>$status));
  	$reservations = $this->$db->get()->result_array();
  	$this->$db->trans_complete();
  	return $reservations;
  
  
  } //end functioin
  
  function updateReservationStatus($rsvid, $status, $db){
  	$this->$db->trans_start();
  	$data['status'] = $status;
  	$this->$db->where('reservations_reservationsid', $rsvid);
  	$this->$db->update('hotelbrancheshasreservations', $data);
  	$this->$db->trans_complete();
  	return true;
  	
  }
  
  function getreservationstatus($reservationid, $db) {
  
  	$this->$db->trans_start();
  	$this->$db->select('status');
  	$this->$db->from('hotelbrancheshasreservations');
  	  	  	 
  	$this->$db->where('reservations_reservationsid', $reservationid);
  	$reservations = $this->$db->get()->result_array();
  	$this->$db->trans_complete();
  	return $reservations;
  
  
  } //end functioin 
  
  function getactiversvids($branchid, $status, $db) {
  	 
  	$this->$db->trans_start();
  
  	//$this->$db->select('reservations_reservationsid');
    	
  	$this->$db->from('hotelbrancheshasreservations');
  
  	$this->$db->where( array('status'=>$status));
  	$this->$db->where( array('hotelbrancheshasreservations.hotelbranches_hotelbranchesid'=>$branchid));
  	$reservations = $this->$db->get()->result_array();
  	$this->$db->trans_complete();
  	return $reservations;
  
  
  } //end functioin

  
} //end class
