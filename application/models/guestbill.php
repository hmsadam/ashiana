<?php

class guestbill extends CI_Model {

  
	
  
  function saveBillingInfo($resvid, $roomcost, $tax, $otherchrgs, $subtotal, $rebateperc, $rebateamount, $grandtotal, $method, $paymentpaid, $balance, $rsvduration, $userid, $db)
  {
	$data['roomscost'] = $roomcost;
	$data['paymentmethod'] = $method;
	$data['rebateamount'] = $rebateamount;
	$data['rebatepercent'] = $rebateperc;
	$data['paymentpaid'] = $paymentpaid;
	$data['reservation_reservationid'] = $resvid;
	$data['subtotal'] = $subtotal;
	$data['grandtotal'] = $grandtotal;
	$data['othercharges'] = $otherchrgs;
	$data['taxamount'] = $tax;
	$data['balance'] = $balance;
	$data['rsvduration'] = $rsvduration;
	
	
	if($balance == 0)
	{
		$data['datebillpaid'] = date("Y-m-d H:i:s" ,time());
	}
	
    $data['createdon'] = date("Y-m-d H:i:s" ,time());
    
    $data['createdby'] = $userid; 
    
	
	$this->$db->trans_start();	
	$this->$db->insert('guestbill',$data);
	$insert_id = $this->$db->insert_id();
	$this->$db->trans_complete();
	
	if ($this->$db->trans_status() === FALSE) {
    # Something went wrong.
    $this->$db->trans_rollback();
    return false;
	
	} 
	else {
		# Everything is Perfect. 
		# Committing data to the database.
		$this->$db->trans_commit();
		return true;
	}
  } //end function
  
 
  
  function getGuestsBill($rsvid, $db)
  {
  	$this->$db->trans_start();
  	 
  	$this->$db->from('guestbill');
  	$this->$db->where( array('reservation_reservationid'=>$rsvid));
  	$bill = $this->$db->get()->result_array();
  	$this->$db->trans_complete();
  	return $bill;
  } //end function
  
  function paybalance($resvid, $grandtotal, $balance, $userid, $db)
  {
  	
  	$data['paymentpaid'] = $grandtotal;
  	$data['balance'] = $balance;
  
  	if($balance == 0)
  	{
  		$data['datebillpaid'] = date("Y-m-d H:i:s" ,time());
  	}
  	
  	$data['lastmodifiedon'] = date("Y-m-d H:i:s" ,time());
  
  	$data['lastmodifiedby'] = $userid;
  
  
  	$this->$db->trans_start();
  	$this->$db->where('reservation_reservationid', $resvid);
  	$this->$db->update('guestbill',$data);
  	$this->$db->trans_complete();
  	return true;
  } //end function
  
  function getallbills($db)
  {
  	$this->$db->trans_start();
  
  	$this->$db->from('guestbill');
  
  	$bill = $this->$db->get()->result_array();
  	$this->$db->trans_complete();
  	return $bill;
  } //end function
 
  
} //end model class

