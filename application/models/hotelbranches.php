<?php

class hotelbranches extends CI_Model {

  
	//this function is just for reference and not being used anywhere in the application

  function getHotelsByUser()
  {
	  //this function is not updated for all controllers for multiple DBs
	  $db = $this->session->userdata('db');
	  $this->$db->trans_start();
		$this->$db->from('hotelbranches');
      $hotels = $this->$db->get()->result_array();
		$this->$db->trans_complete();
		
		$this->load->model('usershashotelbranches');
		$hotelUsers = $this->usershashotelbranches->getHotelUsers();
		$allHotels = array();
		foreach ($hotelUsers as $hotelIds) {
						
						foreach ($hotels as $hotel) {
							if ($hotelIds['hotelbranches_hotelbranchesid'] == $hotel['hotelbranchesid']) { 
								$allHotels[$hotel['hotelbranchesid']] = $hotel['branchname'];
							} //end if
							
						} // end inner foreach
						
					} //end outer foreach
					return $allHotels;
					//return $hotelUsers;
	  
  }//end function
 
  
  function getHotelShortName($branchid, $db)
  {
		$this->$db->trans_start();
		$this->$db->select('shortname');
		$this->$db->from('hotelbranches');
		$this->$db->where('hotelbranchesid', $branchid);
      $shortname = $this->$db->get()->result_array();
	  $this->$db->trans_complete();
	  return $shortname;
	  
  } //end function
  
  function getHotelTitle($branchid, $db)
  {
		$this->$db->trans_start();
		$this->$db->select('branchtitle');
		$this->$db->from('hotelbranches');
		$this->$db->where('hotelbranchesid', $branchid);
      $title = $this->$db->get()->result_array();
	  $this->$db->trans_complete();
	  return $title;
	  
  } //end function
  
  function getBranchData($branchid, $db)
  {
  	$this->$db->trans_start();
  	
  	$this->$db->from('hotelbranches');
  	$this->$db->where('hotelbranchesid', $branchid);
  	$title = $this->$db->get()->result_array();
  	$this->$db->trans_complete();
  	return $title;
  } //end function
		
  
} //end model class
