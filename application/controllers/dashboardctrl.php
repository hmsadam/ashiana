<?php
class dashboardctrl extends CI_Controller{

	public function __construct()
	{
		parent::__construct();


		if( !$this->session->userdata('isLoggedIn') ) {
			redirect('login');
		}
	}
	

function getdashboarddata(){
	
	$hotelId = $this->input->post('hotelid');
	$status = "active";
	$reservedrooms = array();
	
	
	//$hotelId = 1;
	
	$db = $this->session->userdata('db');
	$this->load->model('hotelbrancheshasrsvr');
  	$rsvns = $this->hotelbrancheshasrsvr->getCurrentRsvs($hotelId, $status, $db);
  	$noofactvrsvns = sizeof($rsvns);
  	
  	
  	foreach($rsvns as $reservation )
  	{
  		$this->load->model('reservedrooms');
  		$reservedrooms[] = $this->reservedrooms->getRsvdRomByRsvId($reservation['reservations_reservationsid'], $db);
  	} //end foreach
  	
  	$status = "confirmed";
  	$this->load->model('hotelbrancheshasrsvr');
  	$cnfrmrsvns = $this->hotelbrancheshasrsvr->getCurrentRsvs($hotelId, $status, $db);
  	
  	$noofactvrsvns += sizeof($cnfrmrsvns); 
  	 
  	foreach($cnfrmrsvns as $reservation )
  	{
  		$this->load->model('reservedrooms');
  		$reservedrooms[] = $this->reservedrooms->getRsvdRomByRsvId($reservation['reservations_reservationsid'], $db);
  	} //end foreach
  	
  	$this->load->model('hotelrooms');
  	$allrooms = $this->hotelrooms->getAllRooms($hotelId, $db);
  	
  	
  	$rsvdrooms = 0;
  	$ocupdrooms = 0;
  	$vacantrooms = 0;
  	$totalrooms = sizeof($allrooms);
  	
  	foreach($reservedrooms as $rsvroms)
  	{
  		foreach($rsvroms as $rsvrom)
  		{
  			if($rsvrom['reservedroomstatus'] == "reserved"){
  				$rsvdrooms++;
  			} elseif ($rsvrom['reservedroomstatus'] == "occupied"){
  				$ocupdrooms++;
  			}
  		}
  	}
  	
  	$vacantrooms = $totalrooms - $ocupdrooms;
  	
  	$guestscheckedin = $this::getGuestsCheckedIn($hotelId);
  	$faultyrooms = $this::getUnderMaintenanceRooms($hotelId);
  	
  	
  	//echo json_encode(sizeof($rsvns));
  	
	echo json_encode(array(
			'ttlrsvns' => $noofactvrsvns,
			'ttlroms' => $totalrooms,
			'rsvdrooms' => $rsvdrooms, 
			'ocpdroms' => $ocupdrooms,
			'vacantroms' => $vacantrooms,
			'rsvnsdata' => $rsvns,
			'rsvdromsdata' => $reservedrooms,
			'allroomsdata' => $allrooms,
			'guestscheckedin' => $guestscheckedin,
			'faultyrooms' => $faultyrooms,
	));
} //end function

function getdashboarddataformaingraph(){
	$branchid = $this->input->post('hotelid');
	$startdate = $this->input->post('startdate');
	$enddate = $this->input->post('enddate');
	
	/* $branchid = 1;
	//$status = "active";
	$startdate = '2019-09-09 11:00:00';
	$enddate = '2019-09-14 10:59:59';*/
	
	
	$db = $this->session->userdata('db');
	$this->load->model('reservedrooms');
	$rsrvdRooms = $this->reservedrooms->getrsvdromsformaingrph($branchid, $startdate, $enddate, $db);
	
	//$startdate = "2019-09-11 11:00:00";
	//$enddate = "2019-09-14 10:59:59";
	
	$from = date( 'Y-m-d', strtotime($startdate));
	$sttime = date( 'H:i:s', strtotime($startdate));
	$to = date( 'Y-m-d', strtotime($enddate));
	$entime = date( 'H:i:s', strtotime($enddate));
	
	$datesrange = $this::createDateRangeArray($from, $to);
	
	$alldatesandtimes = array();
	
	for($i = 0; $i< (sizeof($datesrange) - 1); $i++)
	{
		$alldatesandtimes[] = $datesrange[$i]." 11:00:00";
		$alldatesandtimes[] = $datesrange[($i+1)]." 10:59:59";
	} //end function
	
	$datescount = array();
	for($i = 0; $i <sizeof($datesrange); $i++){
		$datescount[] = array($datesrange[$i] => array("reserved" => 0, "occupied" => 0, "vacant" => 0));
	} //end loop
	
	
	$datespairs = array();
	
	foreach(array_chunk($alldatesandtimes, 2) as $pairs) {
		$datespairs[] = array_combine(['datein', 'dateout'], $pairs);
	}
	
	for($i = 0; $i < sizeof($datespairs); $i++){
		for($j = 0; $j < sizeof($rsrvdRooms); $j++){
			if($datespairs[$i]['datein'] >= $rsrvdRooms[$j]['startdate'] AND $datespairs[$i]['dateout'] <= $rsrvdRooms[$j]['enddate'] ){
				
				$stdt = date( 'Y-m-d', strtotime($datespairs[$i]['datein'])); 
				
				foreach($datescount as $datecount){
					foreach($datecount as $key => $value){
							if($key == $stdt){
								if($rsrvdRooms[$j]['reservedroomstatus'] == "reserved"){
									$temp = (int)$value['reserved'];
									
									$datescount[$i][$key]['reserved'] = $temp+1;
								} else{
									$temp = (int)$value['occupied'];
									
									$datescount[$i][$key]['occupied'] = $temp+1;
								}
							}
					}
					
				}
				
			}
			
		} //end for loop
	} //end for loop
	
	$this->load->model('hotelrooms');
	$noofrooms = $this->hotelrooms->countNoOfRooms($branchid, $db);
	$totalrooms = (int)$noofrooms;
	
	for($i = 0; $i < sizeof($datescount); $i++){
	
		foreach($datescount[$i] as $key => $value){
			
					$rsrvrms = (int)$value['reserved'];
					
					$ocpdrms = (int)$value['occupied'];
						
					$datescount[$i][$key]['vacant'] = ($totalrooms - ($rsrvrms + $ocpdrms));
				
			}
	}
				
	
	
	
	echo json_encode($datescount);
	
} // end function

function getfinancialgraphdataold(){
	$branchid = $this->input->post('hotelid');
	$startdate = $this->input->post('startdate');
	$enddate = $this->input->post('enddate');
	
	/*$branchid = 1;
	$startdate = '2019-09-22 11:00:00';
	$enddate = '2019-09-25 10:59:59';*/
	
	
	$db = $this->session->userdata('db');
	$this->load->model('reservedrooms');
	$allrsrvdRooms = $this->reservedrooms->getrsvdromsformaingrph($branchid, $startdate, $enddate, $db);
	$allrsvids = array();
	
	for($i = 0; $i < sizeof($allrsrvdRooms); $i++){
		$allrsvids[] = $allrsrvdRooms[$i]['reservation_reservationid'];
	}
	
	$unqursvids = array_unique($allrsvids);
	$fnlrsvids = array();
	
	foreach($unqursvids as $id){
		$fnlrsvids[] = $id;
	}
	
	$rsvidbillswdar = array();
	foreach($fnlrsvids as $billid){
		
		$this->load->model('guestbill');
		$rsvidbillswdar[] = $this->guestbill->getGuestsBill($billid, $db);
		
	}
	
	$rsvidbills = array();
	foreach($rsvidbillswdar as $bills){
		foreach($bills as $bill){
			$rsvidbills[] = $bill;
		}
	}
	
	//echo json_encode($rsvidbills);
	
	$from = date( 'Y-m-d', strtotime($startdate));
	$sttime = date( 'H:i:s', strtotime($startdate));
	$to = date( 'Y-m-d', strtotime($enddate));
	$entime = date( 'H:i:s', strtotime($enddate));
	
	$datesrange = $this::createDateRangeArray($from, $to);
	
	$alldatesandtimes = array();
	
	for($i = 0; $i< (sizeof($datesrange) - 1); $i++)
	{
		$alldatesandtimes[] = $datesrange[$i]." 11:00:00";
		$alldatesandtimes[] = $datesrange[($i+1)]." 10:59:59";
	} //end function
	
	$datescount = array();
	for($i = 0; $i <sizeof($datesrange); $i++){
		$datescount[] = array($datesrange[$i] => array("totalbill" => 0, "rebate" => 0, "netbill" => 0, "advance" => 0, "balance" => 0));
	} //end loop
	
	
	
	$datespairs = array();
	
	foreach(array_chunk($alldatesandtimes, 2) as $pairs) {
		$datespairs[] = array_combine(['datein', 'dateout'], $pairs);
	}
	
	for($i = 0; $i < sizeof($allrsrvdRooms); $i++){
		if($i == 0){
			if($allrsrvdRooms[$i]['reservation_reservationid'] == $allrsrvdRooms[($i+1)]['reservation_reservationid']){
				
				unset($allrsrvdRooms[($i+1)]);
				$i+= 1;
			}
		} elseif(($i+1) < sizeof($allrsrvdRooms)){
			if($allrsrvdRooms[$i]['reservation_reservationid'] == $allrsrvdRooms[($i+1)]['reservation_reservationid']){
			
				unset($allrsrvdRooms[($i+1)]);
				$i+= 1;
			}
		}
	} //end loop
	$rsrvdRooms = array();
	foreach($allrsrvdRooms as $rsrm){
		$rsrvdRooms[] = $rsrm;
	}
	
	for($i = 0; $i < sizeof($datespairs); $i++){
		for($j = 0; $j < sizeof($rsrvdRooms); $j++){	
			if($datespairs[$i]['datein'] >= $rsrvdRooms[$j]['startdate'] AND $datespairs[$i]['dateout'] <= $rsrvdRooms[$j]['enddate'] ){
	
				$stdt = date( 'Y-m-d', strtotime($datespairs[$i]['datein']));
				foreach($rsvidbills as $bill){
						
				foreach($datescount as $datecount){
					foreach($datecount as $key => $value){
						
								if($key == $stdt){
									if($rsrvdRooms[$j]['reservation_reservationid'] == $bill['reservation_reservationid']){
										
										$temptotalbill = (int)$value['totalbill'];
										$temprebate = (int)$value['rebate'];
										$tempnetbill = (int)$value['netbill'];
										$tempadvance = (int)$value['advance'];
										$tempbalance = (int)$value['balance'];
										if(((int)$bill['rsvduration']) > 1){
											
											$dur = (int)$bill['rsvduration'];
											
											$subtt1 = (int)$bill['subtotal'];
											$rbtamnt1 = (int)$bill['rebateamount'];
											$grndttl1 = (int)$bill['grandtotal'];
											$pmntpd1 = (int)$bill['paymentpaid'];
											$blnc1 = (int)$bill['balance'];
											
											
											$subtt2 = ($subtt1/$dur);
											$rbtamnt2 = ($rbtamnt1/$dur);
											$grndttl2 = ($grndttl1/$dur);
											$pmntpd12 = ($pmntpd1/$dur);
											$blnc12 = ($blnc1/$dur);
											
											
											$subtt = $subtt2;
											$rbtamnt = $rbtamnt2;
											$grndttl = $grndttl2;
											$pmntpd = $pmntpd12;
											$blnc = $blnc12;
											
										} else {
											
											$subtt = (int)$bill['subtotal'];
											$rbtamnt = (int)$bill['rebateamount'];
											$grndttl = (int)$bill['grandtotal'];
											$pmntpd = (int)$bill['paymentpaid'];
											$blnc = (int)$bill['balance'];
											
										}
										
										$datescount[$i][$key]['totalbill'] = $temptotalbill+$subtt ;
										$datescount[$i][$key]['rebate'] = $temprebate+$rbtamnt ;
										$datescount[$i][$key]['netbill'] = $tempnetbill+$grndttl ;
										$datescount[$i][$key]['advance'] = $tempadvance+$pmntpd ;
										$datescount[$i][$key]['balance'] = $tempbalance+$blnc ;
										
									} else{
										
									}
							}
							
						
						
					} //end  rsvnbills
					//break;
				} //end datecount
				
			} //end datescount
			
		} //end if dates comparison
		
		
		} //end for loop
		//continue;
	} //end for loop9
	
	//print_r($datespairs);
	echo json_encode($datescount);
	
} //end function



function getfinancialgraphdataold2(){
	$branchid = $this->input->post('hotelid');
	$startdate = $this->input->post('startdate');
	$enddate = $this->input->post('enddate');

	$branchid = 1;
	$startdate = '2019-09-20 11:00:00';
	$enddate = '2019-09-25 10:59:59';


	$db = $this->session->userdata('db');
	$this->load->model('reservedrooms');
	$allrsrvdRooms = $this->reservedrooms->getrsvdromsformaingrph($branchid, $startdate, $enddate, $db);
	$allrsvids = array();
	for($i = 0; $i < sizeof($allrsrvdRooms); $i++){
		$allrsvids[] = $allrsrvdRooms[$i]['reservation_reservationid'];
	}

	$unqursvids = array_unique($allrsvids);
	$fnlrsvids = array();

	foreach($unqursvids as $id){
		$fnlrsvids[] = $id;
	}

	$rsvidbillswdar = array();
	foreach($fnlrsvids as $billid){

		$this->load->model('guestbill');
		$rsvidbillswdar[] = $this->guestbill->getGuestsBill($billid, $db);

	}

	$rsvidbills = array();
	foreach($rsvidbillswdar as $bills){
		foreach($bills as $bill){
			$rsvidbills[] = $bill;
		}
	}

	$from = date( 'Y-m-d', strtotime($startdate));
	$sttime = date( 'H:i:s', strtotime($startdate));
	$to = date( 'Y-m-d', strtotime($enddate));
	$entime = date( 'H:i:s', strtotime($enddate));

	$datesrange = $this::createDateRangeArray($from, $to);

	$alldatesandtimes = array();

	for($i = 0; $i< (sizeof($datesrange) - 1); $i++)
	{
		$alldatesandtimes[] = $datesrange[$i]." 11:00:00";
		$alldatesandtimes[] = $datesrange[($i+1)]." 10:59:59";
	} //end function

	$datescount = array();
	for($i = 0; $i <sizeof($datesrange); $i++){
		$datescount[] = array($datesrange[$i] => array("totalbill" => 0, "rebate" => 0, "netbill" => 0, "advance" => 0, "balance" => 0));
	} //end loop



	$datespairs = array();

	foreach(array_chunk($alldatesandtimes, 2) as $pairs) {
		$datespairs[] = array_combine(['datein', 'dateout'], $pairs);
	}

	for($i = 0; $i < sizeof($allrsrvdRooms); $i++){
		if($i == 0){
			if($allrsrvdRooms[$i]['reservation_reservationid'] == $allrsrvdRooms[($i+1)]['reservation_reservationid']){

				unset($allrsrvdRooms[($i+1)]);
				$i+= 1;
			}
		} elseif(($i+1) < sizeof($allrsrvdRooms)){
			if($allrsrvdRooms[$i]['reservation_reservationid'] == $allrsrvdRooms[($i+1)]['reservation_reservationid']){
					
				unset($allrsrvdRooms[($i+1)]);
				$i+= 1;
			}
		}
	} //end loop
	
	
	$rsrvdRooms = array();
	foreach($allrsrvdRooms as $rsrm){
		$rsrvdRooms[] = $rsrm;
	}
 $rsltdtsa = array();
	for($i = 0; $i < sizeof($datespairs); $i++){
		for($j = 0; $j < sizeof($rsrvdRooms); $j++){
			if($datespairs[$i]['datein'] >= $rsrvdRooms[$j]['startdate'] AND $datespairs[$i]['dateout'] <= $rsrvdRooms[$j]['enddate'] ){

				$stdt = date( 'Y-m-d', strtotime($datespairs[$i]['datein']));
				
					//echo $stdt."\n";
					
					$rsltdtsa [] = $stdt; 
			} //end if dates comparison


		} //end for loop
		//continue;
	} //end for loop9*/
	
	
	foreach($rsltdtsa as  $stdt){
	foreach($rsvidbills as $bill){
	
		foreach($datescount as $key1 => $datecount){
			foreach($datecount as $key => $value){
				$billdate = date( 'Y-m-d', strtotime($bill['createdon']));
				
				if($billdate == $key ) {
				if($key == $stdt){
						
						$temptotalbill = (int)$value['totalbill'];
						$temprebate = (int)$value['rebate'];
						$tempnetbill = (int)$value['netbill'];
						$tempadvance = (int)$value['advance'];
						$tempbalance = (int)$value['balance'];
						
								
							$subtt = (int)$bill['subtotal'];
							$rbtamnt = (int)$bill['rebateamount'];
							$grndttl = (int)$bill['grandtotal'];
							$pmntpd = (int)$bill['paymentpaid'];
							$blnc = (int)$bill['balance'];
								
						
	
						$datescount[$key1][$key]['totalbill'] = $temptotalbill+$subtt ;
						$datescount[$key1][$key]['rebate'] = $temprebate+$rbtamnt ;
						$datescount[$key1][$key]['netbill'] = $tempnetbill+$grndttl ;
						$datescount[$key1][$key]['advance'] = $tempadvance+$pmntpd ;
						$datescount[$key1][$key]['balance'] = $tempbalance+$blnc ;
	
					
				}
	
				}
	
			} //end  rsvnbills
			//break;
		} //end datecount
	
	} //end datescount
	}
	print_r($rsltdtsa);
	//echo json_encode($rsrvdRooms);

} //end function

function getfinancialgraphdata(){
	$branchid = $this->input->post('hotelid');
	$startdate = $this->input->post('startdate');
	$enddate = $this->input->post('enddate');

	/*$branchid = 1;
	$startdate = '2019-09-20 11:00:00';
	$enddate = '2019-09-25 10:59:59';*/


	$db = $this->session->userdata('db');
	
		$this->load->model('guestbill');
		$rsvidbillswdar = $this->guestbill->getallbills($db);

		
	$from = date( 'Y-m-d', strtotime($startdate));
	$sttime = date( 'H:i:s', strtotime($startdate));
	$to = date( 'Y-m-d', strtotime($enddate));
	$entime = date( 'H:i:s', strtotime($enddate));

	$datesrange = $this::createDateRangeArray($from, $to);

	$alldatesandtimes = array();

	for($i = 0; $i< (sizeof($datesrange) - 1); $i++)
	{
		$alldatesandtimes[] = $datesrange[$i]." 11:00:00";
		$alldatesandtimes[] = $datesrange[($i+1)]." 10:59:59";
	} //end function

	$datescount = array();
	for($i = 0; $i <sizeof($datesrange); $i++){
		$datescount[] = array($datesrange[$i] => array("totalbill" => 0, "rebate" => 0, "netbill" => 0, "advance" => 0, "balance" => 0));
	} //end loop


		foreach($rsvidbillswdar as $bill){

			foreach($datescount as $key1 => $datecount){
				foreach($datecount as $key => $value){
					$billdate = date( 'Y-m-d', strtotime($bill['createdon']));

					if($billdate == $key ) {
							$temptotalbill = (int)$value['totalbill'];
							$temprebate = (int)$value['rebate'];
							$tempnetbill = (int)$value['netbill'];
							$tempadvance = (int)$value['advance'];
							$tempbalance = (int)$value['balance'];


							$subtt = (int)$bill['subtotal'];
							$rbtamnt = (int)$bill['rebateamount'];
							$grndttl = (int)$bill['grandtotal'];
							$pmntpd = (int)$bill['paymentpaid'];
							$blnc = (int)$bill['balance'];



							$datescount[$key1][$key]['totalbill'] = $temptotalbill+$subtt ;
							$datescount[$key1][$key]['rebate'] = $temprebate+$rbtamnt ;
							$datescount[$key1][$key]['netbill'] = $tempnetbill+$grndttl ;
							$datescount[$key1][$key]['advance'] = $tempadvance+$pmntpd ;
							$datescount[$key1][$key]['balance'] = $tempbalance+$blnc ;

					}

				} //end  rsvnbills
				//break;
			} //end datecount

		} //end datescount
	
	//print_r($rsltdtsa);*/
	echo json_encode($datescount);

} //end function

function makedatesarrays(){
	$startdate = "2019-09-11 11:00:00";
	$enddate = "2019-09-14 10:59:59";
	
	$from = date( 'Y-m-d', strtotime($startdate));
	$sttime = date( 'H:i:s', strtotime($startdate));
	$to = date( 'Y-m-d', strtotime($enddate));
	$entime = date( 'H:i:s', strtotime($enddate));
	
	$datesrange = $this::createDateRangeArray($from, $to);
	 
	$alldatesandtimes = array();
	
	
	for($i = 0; $i< sizeof($datesrange); $i++)
	{
			
		$alldatesandtimes[] = date('Y-m-d H:i:s', strtotime("$datesrange[$i] $sttime"));
		$alldatesandtimes[] = date('Y-m-d H:i:s', strtotime("$datesrange[$i] $entime"));
			
	} //end for loop
	
	echo json_encode($alldatesandtimes);
	
} //end function 


function createDateRangeArray($strDateFrom,$strDateTo)
{
	//$strDateFrom,$strDateTo
	
	$aryRange=array();

	$iDateFrom=mktime(1,0,0,substr($strDateFrom,5,2),     substr($strDateFrom,8,2),substr($strDateFrom,0,4));
	$iDateTo=mktime(1,0,0,substr($strDateTo,5,2),     substr($strDateTo,8,2),substr($strDateTo,0,4));

	if ($iDateTo>=$iDateFrom)
	{
		array_push($aryRange,date('Y-m-d',$iDateFrom)); // first entry
		while ($iDateFrom<$iDateTo)
		{
			$iDateFrom+=86400; // add 24 hours
			array_push($aryRange,date('Y-m-d',$iDateFrom));
		}
	}
	return $aryRange;
	//echo json_encode($aryRange);
}

function getUnderMaintenanceRooms($hotelId){

	//$hotelId = 1;
	$db = $this->session->userdata('db');
	$this->load->model('hotelrooms');
	$maintainancerooms = $this->hotelrooms->getFaultyRooms($hotelId, $db);
	return $maintainancerooms;
	//print_r($maintainancerooms);

} //end function



private function getGuestsCheckedIn($hotelId){

	$status = "active";
	$ifguestcheckedin = array();
	$guestsstays = array();

	//$hotelId = 1;

	$db = $this->session->userdata('db');
	$this->load->model('hotelbrancheshasrsvr');
	$actvrsvs = $this->hotelbrancheshasrsvr->getGuestsCheckedIn($hotelId, $status, $db);
	
	
	$status = "confirmed";
	$this->load->model('hotelbrancheshasrsvr');
	$confrmrsvs = $this->hotelbrancheshasrsvr->getGuestsCheckedIn($hotelId, $status, $db);

	foreach($actvrsvs as $key => $value)
	{
		$this->load->model('guestsstays');
		$ifguestcheckedin[] = $this->guestsstays->getGuestsCheckedIn($value['gueststays_gueststaysid'], $db);
	}
	
	foreach($confrmrsvs as $key => $value)
	{
		$this->load->model('guestsstays');
		$ifguestcheckedin[] = $this->guestsstays->getGuestsCheckedIn($value['gueststays_gueststaysid'], $db);
	}

	foreach($ifguestcheckedin as $checkins){
		if(sizeof($checkins) != 0 ){
			$guestsstays[] = $checkins;
		}
	}
	return $guestsstays;


} //end function


function dashboardtests() {
	$this->load->model('hotelbranches');
	$branches = $this->hotelbranches->getHotelsByUser();
	//print_r($branches);
	$data['branches'] = $branches;
	$data['username'] = $this->session->userdata('name');

	$user_id = $this->session->userdata('id');

	$data['userid'] = $user_id;


	$this->load->helper('form');
	$this->load->view('dashboardtests',$data);
}


} //end class