<!DOCTYPE html>
<?php ?>
<!--[if gt IE 8]><!-->
<html class="no-js"><!--<![endif]--><head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Hotel Management System</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">		
        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
        <link href="<?php echo base_url();?>assets/css/css.css" rel="stylesheet" type="text/css">
        <!-- Include Fonts from the theme fonts directory -->
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/font-awesome.css">
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/weather-icons.css">      
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.css">        <!-- Optional theme -->
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-theme.css">
        <!-- Theme's own CSS file -->
        <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/main.css">
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/admin.css">
        
        
        
  
  <link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery-ui.css">

</head>
    <body class="ng-scope body-special" data-ng-app="app" id="app" data-custom-background="" data-off-canvas-nav="" data-ng-controller="AdminAppCtrl">
            <div class="view-container">

                
                <!-- ngView:  --><section data-ng-view="" id="content" class="animate-fade-up ng-scope"><div class="page-signin ng-scope">
	

    <div class="main-body">
        <div class="container">
            <div class="form-container col-md-7 col-sm-7 col-xs-9 centerd loginp">
				<div class="col-md-12 col-sm-12 col-xs-12">
    				<a href="#"><img src="<?php echo base_url();?>assets/images/hms/msr_logo_rgb.png" alt="" class="logo" /></a>
    			</div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <h1 class="logohead">
                        Booking Manager
                    </h1>
                    <h3 class="logintitle">Forgot Password?</h3>
       			</div>
                <form id="frgtpswrdfrm"  data-parsley-validate> 
				
                    
                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                <label >
                                    Email Address
                                </label>
                                <input required class="form-control" placeholder="Email" type="email" name="email" id="email">
                        </div>
                        
                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
							<a name="submit" id="sndnwpswrd" class="btn btn-success btn-block">Send New Password</a>                           
                        </div>
                                    </form>
                                    
                                    

                <section>
                    <p class="text-center" id="rspnspswdchng"></p>
                </section>
               
            </div>
        </div>
    </div>

</div></section>
            </div>
        <!--Uncomment for deployment using Grunt-->
    </body>
    
    <script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/jquery-ui.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <!-- form validation -->
  <script type="text/javascript" src="<?php echo base_url();?>assets/js/parsley/parsley.min.js"></script>
  <!-- textarea resize -->
  <script src="<?php echo base_url();?>assets/js/textarea/autosize.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/hmsappscripts/frgtpswrdscript.js" type="text/javascript"></script>
    
    </html>
    