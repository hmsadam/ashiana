<?php

class guestsstays extends CI_Model {

  
	
  
  function saveGuestRoomNo($roomid, $userid, $db)
  {
	$data['hotelrooms_hotelroomsid'] = $roomid;
    $data['createdon'] = date("Y-m-d H:i:s" ,time());
    
    $data['createdby'] = $userid; 
    
	
	$this->$db->trans_start();	
	$this->$db->insert('guestsstays',$data);
	$insert_id = $this->$db->insert_id();
	$this->$db->trans_complete();
	
	if ($this->$db->trans_status() === FALSE) {
    # Something went wrong.
    $this->$db->trans_rollback();
    return false;
	
	} 
	else {
		# Everything is Perfect. 
		# Committing data to the database.
		$this->$db->trans_commit();
		return $insert_id;
	}
  } //end function
  
  function updateGuestRoomNo($guestsstaysid, $checkin, $checkout, $checkedin, $checkedout, $roomid, $userid, $db)
  {
  	$data['checkindatetime'] = $checkin;
  	$data['checkedin'] = $checkedin;
  	$data['checkedout'] = $checkedout;
  	$data['checkoutdatetime'] = $checkout;
  	$data['hotelrooms_hotelroomsid'] = $roomid;
  	$data['lastmodifiedon'] = date("Y-m-d H:i:s" ,time());
  
  	$data['lastmodifiedby'] = $userid;
  
  
  	$this->$db->trans_start();
  	$this->$db->where('guestsstaysid', $guestsstaysid);
	$this->$db->update('guestsstays', $data);
  	$this->$db->trans_complete();
  
  	if ($this->$db->trans_status() === FALSE) {
  		# Something went wrong.
  		$this->$db->trans_rollback();
  		return false;
  
  	}
  	else {
  		# Everything is Perfect.
  		# Committing data to the database.
  		$this->$db->trans_commit();
  		return true;
  	}
  } //end function
  
  function deleteGuest($id, $db)
  {
  	$this->$db->trans_start();
  	$this->$db->where('guestsstaysid', $id);
  	$this->$db->delete('guestsstays');
  	$this->$db->trans_complete();
  
  
  	return true;
  } //end function
  
  function getGuestsCheckedIn($gueststaysid, $db) {
  
  	$this->$db->trans_start();
  
  	$this->$db->from('guestsstays');
  	$this->$db->where('guestsstaysid', $gueststaysid);
  	$this->$db->where('checkedin', '1');
  	
  	$stays = $this->$db->get()->result_array();
  	$this->$db->trans_complete();
  	return $stays;  
  
  } //end functioin  
 
  function getgeuststays($guestid, $db)
  {
  	$this->$db->trans_start();
  	//$this->$db->select('guestsid, firstname, lastname, cellNo, landLineNo, email, gender, age, street, city, county, postcode, country, comments, isAFamilyMember, guestsstaysid, checkindatetime, checkoutdatetime, hotelroomsid, roomno, guesthasreservations.reservation_reservationid');
  	$this->$db->from('guestshasstays');
  
  	
  	$this->$db->join('guestsstays', 'guestsstays.guestsstaysid = guestshasstays.gueststays_gueststaysid', 'left');
  	
  
  	$this->$db->where('guestshasstays.guest_guestsid', $guestid);
  
  	$result = $this->$db->get()->result_array();
  	$this->$db->trans_complete();
  	return $result;
  
  
  } //end function
  
} //end model class
