<?php

class useraddresses extends CI_Model {

  

  function saveUsersAddress($addresstype, $stName, $hNo, $townName, $cityName, $stateName, $countName, $zipCode, $hPhone, $mPhone, $oPhone, $wEmail, $pEmail, $website, $userId, $db) {

	$data['addresstype'] = $addresstype;
    $data['streetname'] = $stName;
    $data['houseno'] = $hNo;
    $data['townname'] = $townName;
    $data['cityname'] = $cityName;
    $data['statename'] = $stateName;
    $data['countryname'] = $countName;
    $data['zipcode'] = $zipCode;
    $data['homephone'] = $hPhone;
    $data['mobilephone'] = $mPhone;
    $data['officephone'] = $oPhone;
    $data['workemail'] = $wEmail;
    $data['personalemail'] = $pEmail;
    $data['website'] = $website;
    $data['usersoflabs_userId'] = $userId;
    
    
		$this->$db->trans_start();
	 $this->$db->insert('address',$data);
	 $insert_id = $this->$db->insert_id();
	 $this->$db->trans_complete();
			if($insert_id)
			{
				return true;
			} else 
			{
				return false;
			} //end else
  } //end function 
  
  function updateUsersAddress($addressid, $stName, $hNo, $townName, $cityName, $stateName, $countName, $zipCode, $hPhone, $mPhone, $oPhone, $wEmail, $pEmail, $website, $db) {

	
    $data['streetname'] = $stName;
    $data['houseno'] = $hNo;
    $data['townname'] = $townName;
    $data['cityname'] = $cityName;
    $data['statename'] = $stateName;
    $data['countryname'] = $countName;
    $data['zipcode'] = $zipCode;
    $data['homephone'] = $hPhone;
    $data['mobilephone'] = $mPhone;
    $data['officephone'] = $oPhone;
    $data['workemail'] = $wEmail;
    $data['personalemail'] = $pEmail;
    $data['website'] = $website;
    $this->$db->trans_start();
		$this->$db->where('addressid', $addressid);
		//$this->db->where('addresstype', $addresstype);
	$this->$db->update('address', $data);
	$this->$db->trans_complete();
				return true;
	 
  } //end function
  
  function getUsersAddress($userId, $db){
	$this->$db->trans_start();
	$this->$db->from('address');
    $this->$db->where('usersoflabs_userId',$userId );
		
	$addresses = $this->$db->get()->result_array();
	$this->$db->trans_complete();
	if(!empty($addresses)){
		return $addresses; 
	} else
	{
		return false;
		
	}
    
  } 
  //end function
   
  
  
  
} //end class
