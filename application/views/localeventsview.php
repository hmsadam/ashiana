<?php ?>

<?php include("header.php")?>
<?php include("menu.php")?>

<script src="<?php echo base_url();?>assets/js/jquery-ui.js"></script>
  
  <link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery-ui.css">
<script src="<?php echo base_url();?>assets/js/hmsappscripts/mainscript.js" type="text/javascript"></script> 
 <style>
 /***************************Events Calendar**********************************/
 .ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default{    font-family: 'Helvetica' !important;
    font-size: 16px;
 
   line-height:30px;
   
    text-align: center;
   
    height: 41px;}
	.ui-slider-handle.ui-state-default.ui-corner-all{height: 20px;
    background: #999;
    line-height: inherit;}
.ui-state-default,
.ui-widget-content .ui-state-default, 
.ui-widget-header .ui-state-default{font-family:"Helvetica Neue",Helvetica,Arial,sans-serif;font-size:16px;}


.ui-state-highlight,  .ui-widget-content .ui-state-highlight,  .ui-widget-header .ui-state-highlight
{background:: #4AB2CB;color:#fff;}
 .ui-state-default,  .ui-widget-content .ui-state-default,  .ui-widget-header .ui-state-default{ border:1px solid #E6E9ED !important;color:#000 !important;}
 /*.ui-ui-datepicker-calendar tr td a.ui-state-default{background:transparent !important;}*/
 .ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default{background:#fff;}
 .ui-state-highlight, .ui-widget-content .ui-state-highlight, .ui-widget-header .ui-state-highlight{background: #eeeeee;color:#000 !important;}
 .ui-state-default.ui-state-hover{background:#eeeeee;}
 .ui-state-default.ui-state-active{color:#fff !important;}
 </style>

<body class="nav-md">

  

      
      <!-- page content -->
      <div class="right_col"  role="main">
        <div class="">

          <div class="page-title">
            <div class="title_left">
              <h2>Local Events</h2>
            </div>
            
          </div>
        
          <!-- start of data table -->
          <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel localevents">
                <div class="x_title">
                  <h3>Manage Local Events</h3>
             
                </div>
                <div class="x_content">
                  <table id="enqtble" class="table table-striped responsive-utilities jambo_table">
                    <thead>
                      <tr class="headings">
                        
                        <th>Position</th>
                        <th>Title</th>
                        <th>Date</th>
                        <th>Image</th>
                        <th>Actions</th>
                      </tr>
                    </thead>
				
						
						

					
                    					
					

                  </table>
                </div>
              </div>
            </div>


          </div>
		  
		  <!-- end of data table -->
		  
		  
		  	  <!-- Large modal -->
                
                <div class="modal fade bs-example-modal-lg" id="evntupdationmdl"tabindex="-1" role="dialog" aria-hidden="true">
                  <div class="modal-dialog modal-lg">
                    <div class="modal-content  col-xs-12">

                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                        </button>
                        <h4 class="modal-title" id="myModalLabel">Update New Event</h4>
                      </div>
                      <div class="modal-body"  >
								 <form autocomplete="off" id="localevntuptform" data-parsley-validate class="form-horizontal form-label-left">
								<input type="hidden" id="lclevntuptid" value="W3Schools">
								
								<div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="headline1">Headline / Title <span class="required">*</span>
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" id="headline1" name="headline1" required class="form-control col-md-7 col-xs-12">
                      </div>
                    </div>
                    
                    <div class="form-group" id="addEventDate">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="evntdate1">Date <span class="required">*</span>
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" id="evntdate1" name="evntdate1" required class="form-control col-md-7 col-xs-12">
                      </div>
                    </div>
                    
					<div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="evnttext1">Event Text <span class="required">*</span>
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <textarea id="evnttext1" name="evnttext1" required class="form-control col-md-7 col-xs-12"></textarea>
                      </div>
                    </div>
					<div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="evntposition1">Event Position <span class="required">*</span>
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="number" id="evntposition1" name="evntposition1" required class="form-control col-md-7 col-xs-12">
                      </div>
                    </div>
					
								
						  </form>
                
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" id="evntupdtfrmbtn"class="btn btn-primary" >Save changes</button>
                      </div>

                    </div>
                  </div>
                </div>
		   <!--end modal -->
		  
		   <div class="clearfix"></div>
		  <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h3>Create New Local Event </h3>
                
                </div>

                <div class="x_content">
              
                  <form id="localeventscrtsfrm" data-parsley-validate class="form-horizontal form-label-left">

                    <div class="form-group  col-md-6 col-sm-6 col-xs-12 padding-left-0">
                      <label class="control-label" for="headline">Headline / Title <span class="required">*</span>
                      </label>
                            <input type="text" id="headline" name="headline" required class="form-control col-md-7 col-xs-12">
                    </div>
                    <div class="clearfix"></div>
                    <div class="form-group  col-md-6 col-sm-6 col-xs-12 padding-left-0">
                      <label class="control-label" for="evntdate">Date <span class="required">*</span>
                      </label>
                     
                        <input type="text" id="evntdate" name="evntdate" required class="form-control col-md-7 col-xs-12">
                     
                    </div>
                    <div class="clearfix"></div>
					<div class="form-group  col-md-6 col-sm-6 col-xs-12 padding-left-0">
                      <label class="control-label " for="evnttext">Event Text <span class="required">*</span>
                      </label>
                      
                        <textarea id="evnttext" name="evnttext" required class="form-control col-md-7 col-xs-12"></textarea>
                    
                    </div>
					<div class="clearfix"></div>
					<div class="form-group  col-md-6 col-sm-6 col-xs-12 padding-left-0">
                      <label class="control-label " for="evntposition">Event Position <span class="required">*</span>
                      </label>
                      
                        <input type="number" id="evntposition" name="evntposition" required class="form-control col-md-7 col-xs-12">
                     
                    </div>
                    <div class="clearfix"></div>
                    <div class="ln_solid"></div>
                    <div class="form-group">
                      <div class="col-md-12 col-sm-12 col-xs-12 padding-left-0 padding-right-0">
                        
                        <input type="reset" value="Cancel" class="btn btn-default"> 
                        <a href="javascript:void(0);" id="save" class="btn btn-success" style="float:right;"> Submit</a>
                      </div>
                    </div>

                  </form>
                </div>
              </div>
            </div>
          </div>
		  
		  
		  
		  
		  
		  <!-- room video upload modal -->
                
                <div class="modal fade bs-example-modal-lg" id="evntimguploadmdl"tabindex="-1" role="dialog" aria-hidden="true">
                  <div class="modal-dialog modal-lg">
                    <div class="modal-content col-xs-12">

                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                        </button>
                        <h4 class="modal-title" id="romimgslbl">Add Event Image</h4>
                      </div>
                      <div class="modal-body">
                      
                      <!-- Media Gallery Starts Here -->
                      
                       <div class="row">
            <div class="col-md-12">
              <div class="x_panel">
                <div class="x_title">
                  <h3>Local Event Image </h3>
                </div>
                <div class="x_content">

                  <div class="row" id="imgrow">

                    

                    
                    
                    

                  </div>

                </div>
              </div>
            </div>
          </div>
      
                      
                      
                      <!-- Media Gallery Ends Here -->
								 <form id="lclevntimgupldform" data-parsley-validate class="form-horizontal form-label-left"  method="post" action="" enctype="multipart/form-data">
								
								<input type="hidden" id="lclevntid" value="W3Schools">
								 <input required type="file" name="evntimg" id="evntimg">
								
						  </form>
                
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" id="entimgupldbutn" class="btn btn-primary"  data-dismiss="modal">Save changes</button>
                      </div>

                    </div>
                  </div>
                </div>
		   <!--end video upload modal -->


          <script type="text/javascript">
            $(document).ready(function() {
              
            });
          </script>


    </div>
  </div>

  <div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
  </div>

  <script src="<?php echo base_url();?>/assets/js/bootstrap.min.js"></script>

  <!-- bootstrap progress js -->
  <script src="<?php echo base_url();?>assets/js/progressbar/bootstrap-progressbar.min.js"></script>
  <script src="<?php echo base_url();?>assets/js/nicescroll/jquery.nicescroll.min.js"></script>
  <!-- icheck -->
  <script src="<?php echo base_url();?>assets/js/icheck/icheck.min.js"></script>
  <!-- tags -->
  <script src="<?php echo base_url();?>assets/js/tags/jquery.tagsinput.min.js"></script>
  <!-- switchery -->
  <script src="<?php echo base_url();?>assets/js/switchery/switchery.min.js"></script>
  <!-- daterangepicker -->
  <script type="text/javascript" src="<?php echo base_url();?>assets/js/moment/moment.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>assets/js/datepicker/daterangepicker.js"></script>
  <!-- richtext editor -->
  <script src="<?php echo base_url();?>assets/js/editor/bootstrap-wysiwyg.js"></script>
  <script src="<?php echo base_url();?>assets/js/editor/external/jquery.hotkeys.js"></script>
  <script src="<?php echo base_url();?>assets/js/editor/external/google-code-prettify/prettify.js"></script>
  <!-- select2 -->
  <script src="<?php echo base_url();?>assets/js/select/select2.full.js"></script>
  <!-- form validation -->
  <script type="text/javascript" src="<?php echo base_url();?>assets/js/parsley/parsley.min.js"></script>
  <!-- textarea resize -->
  <script src="<?php echo base_url();?>assets/js/textarea/autosize.min.js"></script>
  <script>
    autosize($('.resizable_textarea'));
  </script>
  <!-- Datatables -->
  <script src="<?php echo base_url();?>assets/js/datatables/js/jquery.dataTables.js"></script>
  <script src="<?php echo base_url();?>assets/js/datatables/tools/js/dataTables.tableTools.js"></script>

  <script src="<?php echo base_url();?>assets/js/timepickadon/jquery-ui-timepicker-addon.js" type="text/javascript"></script> 
<link rel="stylesheet" href="<?php echo base_url();?>assets/js/timepickadon/jquery-ui-timepicker-addon.css">
  <!-- pace -->
  <script src="<?php echo base_url();?>assets/js/pace/pace.min.js"></script>
  
  <script src="<?php echo base_url();?>assets/js/custom.js"></script>
  
  <!-- PNotify -->
  <script type="text/javascript" src="<?php echo base_url();?>assets/js/notify/pnotify.core.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>assets/js/notify/pnotify.buttons.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>assets/js/notify/pnotify.nonblock.js"></script>
  
  <script src="<?php echo base_url();?>assets/js/hmsappscripts/customnotificationsscript.js" type="text/javascript"></script>
  
  <script type="text/javascript" src="<?php echo base_url();?>assets/js/hmsappscripts/mainscript.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>assets/js/hmsappscripts/localeventsscript.js"></script>


 <style>
/* .ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default{    font-family: 'Helvetica' !important;
    font-size: 16px;
    padding: 5px !important;
    line-height: 30px;
    width: 100%;
    text-align: center;
    padding-top: 0px !important;
    height: 41px;background:#fff !important;border:1px solid #eeeeee ;}
	.ui-state-default{border:1px solid #eeeeee !important;}
	
	.ui-state-highlight, .ui-widget-content .ui-state-highlight, .ui-widget-header .ui-state-highlight{background:#eee !important;border:1px solid #eee !important;color:#000 !important;}
	.ui-state-default.ui-state-active{background:#4AB2CB !important;}
	.ui-datepicker td {
    border: 0;
    padding: 1px !important;}
	.ui-widget-content{padding:20px !important;border:1px solid #eee !important;border-radius:2px !important;}
	.ui-state-default.ui-state-hover{border :1px solid #4ab2cb !important}
	.ui-widget-header .ui-datepicker-prev{background:url(../images/hms/rightarrow.png) no-repeat center center !important;background-color:transparent !important;}
	
	.ui-widget-header .ui-datepicker-next{background:url(../images/hms/leftarrow.png) no-repeat center center !important;background-color:transparent !important;}
	.ui-widget-header .ui-datepicker-prev:hover, .ui-datepicker-prev-hover{background:url(../images/hms/rightarrow_active.png) no-repeat center center !important;border:0px !important;}
	.ui-widget-header .ui-datepicker-next:hover, .ui-datepicker-next-hover{background:url(../images/hms/leftarrow_active.png) no-repeat center center !important;border:0px !important}
	/*, {background-color:transparent !important;border:0px !important}*/
	.ui-datepicker .ui-datepicker-prev, .ui-datepicker .ui-datepicker-next{top:1px !important;}
	.ui-datepicker .ui-datepicker-prev {
    left: 1px !important;
}
	.ui-datepicker .ui-datepicker-next{right:1px !important;}
	ui-datepicker .ui-datepicker-pre{left:1px !important;}
	.ui-datepicker .ui-datepicker-title{margin-top:-4px !important;}
	.ui-state-default.ui-state-hover{background:#eeeeee !important;border: 1px solid #eee !important;}*/
 </style>
  
 
</body>

</html>
