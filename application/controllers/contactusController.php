<?php

class contactusController extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
	

    if( !$this->session->userdata('isLoggedIn') ) {
        redirect('/login/show_login');
    }
  }

	function getenquiries()
	{
		
	$this->load->model('contactusenquiry');
	$enquiries = $this->contactusenquiry->getenquiries();
	echo json_encode($enquiries);
	}
  
  function enquiryadmnview() {
    
	$this->load->model('hotelbranches');
	$branches = $this->hotelbranches->getHotelsByUser();
		
	$user_id = $this->session->userdata('id');
	$db = $this->session->userdata('db');
	
	$data['branches'] = $branches;
    $data['username'] = $this->session->userdata('name');
    
    
  
	$this->load->helper('form');
    $this->load->view('contactenquiryadminview',$data);
  }

    
  
  
  function deleteenquiries()
  {
  	$id = $this->input->post('id');
  	
  	$this->load->model('contactusenquiry');
  	$result = $this->contactusenquiry->deleteenquiry($id);
  	echo json_encode($result);
  }
      
  
  
  
} //end class
