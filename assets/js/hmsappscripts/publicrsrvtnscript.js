var mltplrmrsrv = false;

$(document).ready(function(){
	
	//more less code starts here
	
  
    
	
	//more less code ends here
		
	startWizardReservation();
	$("#searchrslts").hide();
	$('#chckinoterr').hide();
		
	//form form validation code starts here
	$.listen('parsley:field:validate', function() {
		vldtrmavblt();
      });
      $('#viwavbtbtn').on('click', function() {
        $('#chckavbtyfrmpblc').parsley().validate();
        var result = vldtrmavblt();
		if(result)
		{
			checkavaialability();
			
		}
		
      });
      var vldtrmavblt = function() {
        if (true === $('#chckavbtyfrmpblc').parsley().isValid()) {
          $('.bs-callout-info').removeClass('hidden');
          $('.bs-callout-warning').addClass('hidden');
		  return true;
        } else {
          $('.bs-callout-info').addClass('hidden');
          $('.bs-callout-warning').removeClass('hidden');
		  return false;
        }
      };
	
	//customer form validation code ends here
	
	
	//customer form validation code starts here
	$.listen('parsley:field:validate', function() {
        validateFront();
      });
      $('#saveCustomer').on('click', function() {
        $('#customerDataForm').parsley().validate();
        var result = validateFront();
		if(result)
		{
			addCustomerData();
			
		}
		
      });
      var validateFront = function() {
        if (true === $('#customerDataForm').parsley().isValid()) {
          $('.bs-callout-info').removeClass('hidden');
          $('.bs-callout-warning').addClass('hidden');
		  return true;
        } else {
          $('.bs-callout-info').addClass('hidden');
          $('.bs-callout-warning').removeClass('hidden');
		  return false;
        }
      };
	   
        //start and end data timepicker api call
     // var dateToday = moment().subtract(1, 'days').format('YYYY-MM-DD');
	 var dateToday = new Date();
      var datetodayforend = new Date(); 
        $('#startdate').datetimepicker({
        	 showButtonPanel: false,
             minDate: dateToday,
        	timeFormat: '',
        	dateFormat: 'yy-mm-dd',
        		onSelect: function(){
        			var datetime = $('#startdate').val();
        			$('#startdate').val(converttodmy(datetime));
        			
        			var dt = datetime + "11:00:00";
        			setStartDate(dt);
        			$('#ui-datepicker-div').hide();
        		} //end inner function
        		
        		
        	} //end body
        	);
        	
        	$('#enddate').datetimepicker({
        	showButtonPanel: false,
            minDate: datetodayforend,
        	timeFormat: '',
        	dateFormat: 'yy-mm-dd',
        		onSelect: function(){
        			//alert('Time Selected');
        			var datetime = $('#enddate').val();
        			$('#enddate').val(converttodmy(datetime));
        			var dt = datetime + "10:59:59";
        			setEndDate(dt);
        			$('#ui-datepicker-div').hide();
        		} //end inner function 
        	
        	} //end body
        	
        	);
        
        	localtimestring();
        	
        	//calling of checkin checkout from range picker
        	/*var cb = function(start, end, label) {
                $('#checkin span').html(start.format('MMMM D') + ' - ' + end.format('MMMM D'));
              }
              var optionSet1 = {
                startDate: moment().subtract(1, 'days'),
                endDate: moment(),
                //minDate: '01/01/2012',
                //maxDate: '12/31/2015',
                
                isUTC: false,
                minDate: moment().subtract(1, 'days'),
                maxDate: '2050-12-31',
                dateLimit: {
                  days: 60
                },
                showDropdowns: true,
                showWeekNumbers: true,
                timePicker: false,
                timePickerIncrement: 1,
                timePicker12Hour: false,
                
                opens: 'right',
                buttonClasses: ['btn btn-default'],
                applyClass: 'btn-small btn-primary',
                cancelClass: 'btn-small',
                format: 'YYYY-MM-DD',
                separator: ' to ',
                locale: {
                	format: 'YYYY-mm-dd H:i:s',
                  applyLabel: 'Submit',
                  cancelLabel: 'Clear',
                  fromLabel: '',
                  toLabel: '',
                  customRangeLabel: 'Custom',
                  daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                  monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                  firstDay: 1
                }
              };
              $('#checkin span').html(moment().subtract(1, 'days').format('ddd, DD MMM ') + ' - ' + moment().format('ddd, DD MMM '));
              $('#checkin').daterangepicker(optionSet1, cb);
              $('#checkin').on('show.daterangepicker', function() {
                
              });
              $('#checkin').on('hide.daterangepicker', function() {
                
              });
              $('#checkin').on('apply.daterangepicker', function(ev, picker) {
                
            	  
                var stdatebeforetrim = picker.startDate.toString();
                var enddatebeforetrim = picker.endDate.toString();

                var stdate = convert(stdatebeforetrim);
                var endate = convert(enddatebeforetrim);
                
                
                
                var stdatedmy = converttodmy(stdatebeforetrim);
                var endatedmy = converttodmy(enddatebeforetrim);
                var slctddtee = stdatedmy + ' to ' + endatedmy;
                
                $('#checkin').val(slctddtee);
                
                
                
                var startdatefrgrph = stdate + ' 11:00:00'; 
                var enddatefrgrph = endate + ' 10:59:59';
               
               
               var isvalidrnge = validatedatefrrngpkr(startdatefrgrph, enddatefrgrph);
                if(isvalidrnge){
                	$('#chckinoterr').hide();
                setStartDate(startdatefrgrph);
                setEndDate(enddatefrgrph);
              } else {
            	  $('#chckinoterr').show();
              }
                  
              });
              $('#checkin').on('cancel.daterangepicker', function(ev, picker) {
                
              });
              $('#options1').click(function() {
                $('#checkin').data('daterangepicker').setOptions(optionSet1, cb);
              });
              $('#options2').click(function() {
                //$('#checkin').data('daterangepicker').setOptions(optionSet2, cb);
              });
              $('#destroy').click(function() {
                //$('#checkin').data('daterangepicker').remove();
              });
        	
              */
        	
	}); //end readyfunction

function AddReadMore() {
	
	
    //This limit you can set after how much characters you want to show Read More.
    var carLmt = 250;
    // Text to show when text is collapsed
    var readMoreTxt = "... <label>More</label>";
    // Text to show when text is expanded
    var readLessTxt = "... <label>Less</label>";


    //Traverse all selectors with this class and manupulate HTML part to show Read More
    $(".addReadMore").each(function() {
        if ($(this).find(".firstSec").length)
            return;

        var allstr = $(this).text();
        if (allstr.length > carLmt) {
            var firstSet = allstr.substring(0, carLmt);
            var secdHalf = allstr.substring(carLmt, allstr.length);
            var strtoadd = firstSet + "<span class='SecSec'>" + secdHalf + "</span><span class='readMore'  title='Click to Show More'>" + readMoreTxt + "</span><span class='readLess' title='Click to Show Less'>" + readLessTxt + "</span>";
            $(this).html(strtoadd);
        }

    });
    //Read More and Read Less Click Event binding
    $(document).on("click", ".readMore", function(e) {
    	
    	
    	
    	$(this).closest(".addReadMore").removeClass("showlesscontent");
    	$(this).closest(".addReadMore").addClass("showmorecontent");
    });
    
    $(document).on("click", ".readLess", function(e) {
    	

        //$(this).closest(".addReadMore").toggleClass("showlesscontent showmorecontent");
    	$(this).closest(".addReadMore").removeClass("showmorecontent");
    	$(this).closest(".addReadMore").addClass("showlesscontent");
    	
    });
}; 

function convert(str) {
	  var date = new Date(str),
	    mnth = ("0" + (date.getMonth() + 1)).slice(-2),
	    day = ("0" + date.getDate()).slice(-2);
	  return [date.getFullYear(), mnth, day].join("-");
} //end function

function converttodmy(str) {
	  //var date = new Date(str),
	    //mnth = ("0" + (date.getMonth() + 1)).slice(-2),
	  //  day = ("0" + date.getDate()).slice(-2);
	 // return [day, mnth, date.getFullYear()].join("/");
	 var year = str.substring(0,4);
	 //alert(year);
var month = str.substring(5,7);
       var day = str.substring(8,10);
	 var datenew = day+"/"+month+"/"+year;
	 return datenew;
} //end function




function localtimestring(){
	var timnow = new Date();
	return timnow.toLocaleTimeString();
}
	
	function datetimefrmt(ndate){
	 if( /iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {	
		var arr = ndate.split(/[- :]/);
		var datoo = new Date(arr[0], arr[1]-1, arr[2], arr[3], arr[4], arr[5]);
		//var datooonew = datoo.substring(0,4);
		//alert(datoo);
		 return datoo;
	 }
	else{	
		
//		
	var today = new Date(ndate);
		
		var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
		var d = new Date(ndate);
		var dayName = days[d.getDay()];
		var daysNames = dayName.substring(0,3)


		var monthNames = [
			"January", "February", "March",
			"April", "May", "June", "July",
			"August", "September", "October",
			"November", "December"
		  ];
		
		  var day = today.getDate();
		  var monthIndex = today.getMonth();
		//  var year = today.getFullYear();

			//alert( day + ' ' + monthNames[monthIndex] + ' ' + year);
		  var months = monthNames[monthIndex].substring(0,3);
			
		//alert(months);
		
			
				///	var options = { weekday: 'short', day: 'numeric', month: 'short'  };
			//	var result = today.toLocaleDateString("en-US", options);
				//	var week = result.substring(0,4);
			//var month = result.substring(4,8);
			 //      var date = result.substring(9,12);
       
	
       var finaldateString = daysNames+ ",  " +day + ' ' + months;		
	   return finaldateString;
	}
		
	}
	
	//start and end date code start



//startdate enddate code starts here
	
	var mystartdate;
	var myenddate;
	function setStartDate(date)
	{
		
		
		mystartdate = date;
		if($('#enddate').val())
		{
			
			validateDate();
		} 		
	} //end function
	
	function setEndDate(date)
	{
		
		myenddate = date;
		if($('#startdate').val())
		{
			
			validateDate();
		} 	
	} //end function
	
	var intadults;
	var intchild;
	var intinfant; 
	
	
	function checkavaialability(){
		
			if(validateDate()) {
				
				var adult = $('#adults').val();
				var child = $('#childrens').val();
				var infant = $('#infants').val();
				
				 intadults = parseInt(adult);
				 intchild = parseInt(child);
				 intinfant = parseInt(infant);
				
				$('#stDate').val("");
				$('#enDate').val("");
				$('#duration').val("");
				
				if(selectedroomsforrsv.length > 0)
					{
					selectedroomsforrsv.pop();
					}
								
			   //$("#selectedRooms").empty(); //if dates changed after selecting rooms on other dates
				//getReservationDuration(mystartdate, myenddate);
				//setTimeout(function(){ findAvailableRooms(); }, 3000);
				findAvailableRooms();
			   
			} else {
				
			}
		
		
		
		
	} //end function
	
	
	function validateDate()
	{
		
		if(Date.parse(mystartdate) > Date.parse(myenddate)){
		   					
					$('#chckinoterr').show();

		} else{
			$('#chckinoterr').hide();
			return true;
		}
	}
	
	function validatedatefrrngpkr(start, end)
	{
		
		if(Date.parse(start) > Date.parse(end)){
		   		
					return false;
		} else{
			return true;
		}
	}
	//start and end date code end
	
	function showWiz()
	{
		
		clearAllFormsFields();
		
		$('#strtrsv').hide();
		$('#rsvwzrd').show();
	} //end function
	
	function showStrtRsv(data)
	{
		var result = insertresvIdInSess(data['reservationid']);
		
		if(result){
		showHideWizardR();
		window.open (''+pageURL+'/pblcreservationController/showInvoice', '_blank');
		}
		
	} //end function
	
	function insertresvIdInSess(resvId)
	{
		$.post(''+pageURL+'/pblcreservationController/insertRsvIdInSess', {resvId: resvId}, function(data){
			
		}, 'json'	);
		return true;
	}
	
	function cancelRsv()
	{
		//after canceling starting new reservation is visible
		showHideWizardR();
	} //end function
	
	function showHideWizardR(){
		$('#rsvwzrd').hide();
		$('#strtrsv').show();
		window.location.reload();
	}
	
	
	
	function CreateNewResrvTempFunction()
	{
		
		$('#resvId').val("KHAN2016Q230");
					getRoomsCatgs();
					getHotelFloors();
					//getReservedRoomsForGuestAssignment();
					showWiz();
	} //end function
	var mywizard;
	function startWizardReservation()
	{
		
		mywizard = $('#wizard').smartWizard();
		
		//showWiz();
		
		
	} //
	
	
	
	var currentlyAvailableRooms;
	var fetchedrooms;
	var roomrates;
	var roomimages;
	function findAvailableRooms()
	{
		
		$("#roomsuidiv").empty();
		fetchedrooms = null;
		var branchid = $('#hotelId').val();
		$.post(""+pageURL+"/pblcreservationController/getRoomsBYDateTime", {branchid:branchid, mystartdate:mystartdate, myenddate:myenddate, adults : intadults, child: intchild, infant: intinfant}, function(data){
			
			if(data['rooms']['data'].length > 0 ){
				
				fetchedrooms = data['rooms'];
				roomrates = data['rates'];
				roomimages = data['images'];
				mergeromsrtsimgs();
				
			} else {
				noroomfound();
			}
						
		}, 'json');
				
	} //end function
	
	function noroomfound(){
		$("#searchrslts").hide();
		$("#roomsuidiv").empty();
		$("#roomsuidiv").append('<div id="romfndnot">'+
                      	'<div class="head">No Results On Those Dates</div>'+
                      	'<div class="text">We are sorry, but we are fully reserved for the dates you have chosen.</div>'+
                      	'<div class="text">Please enter a different start and end date and search again.</div>'+
                      '</div>');
		
	} //end function
	
	
	
	function mergeromsrtsimgs(){
		
		var roomratesrng = [];
		
		for(var i = 0; i < fetchedrooms['data'].length; i++){
			roomratesrng[i] = {
					'roomid' : fetchedrooms['data'][i]['hotelroomsid'],
					'rates' : [],					 
			}
		}
		
		for(var i = 0; i < fetchedrooms['data'].length; i++){
			for(var j = 0; j < roomrates.length; j++){
				if(fetchedrooms['data'][i]['hotelroomsid'] == roomrates[j]['hotelrooms_hotelroomsid']){
					//roomratesrng[i] = fetchedrooms['data'][i]['hotelroomsid'];
				roomratesrng[i]['rates'].push(parseInt(roomrates[j]['roomrate']));
				
				} //end if
			} // end inner loop
		} //end for loop
		
		for(var i = 0; i < fetchedrooms['data'].length; i++){
			for(var j = 0; j < roomratesrng.length; j++){
				if(fetchedrooms['data'][i]['hotelroomsid'] == roomratesrng[j]['roomid']){
					if(roomratesrng[j]['rates'].length > 1){
						fetchedrooms['data'][i]['roomrate'] = ''+roomratesrng[j]['rates'][0]+' - ' + roomratesrng[j]['rates'][(roomratesrng[j]['rates'].length - 1)];
					} else {
						fetchedrooms['data'][i]['roomrate'] = ''+roomratesrng[j]['rates'][0];
					}
				}
			}
		} //end for
		
		
		for(var k = 0; k < fetchedrooms['data'].length; k++){
			for(var l = 0; l < roomimages.length; l++){
				for(var m = 0; m < roomimages[l].length; m++){
					if(fetchedrooms['data'][k]['hotelroomsid'] == roomimages[l][m]['hotelrooms_hotelroomsid']){
						fetchedrooms['data'][k]['images'] = roomimages[l];
					} //end if
				} //end innermost loop
			} // end inner loop
		} //end for loop
		
		
		currentlyAvailableRooms = fetchedrooms;
		showavlbromsui();
		
	} //end function
		
	
	function showavlbromsui(){
		$("#roomsuidiv").empty();
		$("#searchrslts").show();
		
		for (var key in currentlyAvailableRooms) {
		  if (currentlyAvailableRooms.hasOwnProperty(key)) {
				var val = currentlyAvailableRooms[key];
								
			for(var key1 in val){
				val1 = val[key1];
					
							$("#roomsuidiv").append("<div class='searchrow'>" +
									
									"<div id='imgdiv"+val1.hotelroomsid+"' class='col-md-6 col-sm-6 col-xs-12 p-0-img pull-right imgscont'></div>" +
									"<div class='searchresult col-md-6 col-sm-6 col-xs-12 padding-left-0'><div class='roominfo'><h3 class='roomno'>"+val1.roomno+"</h3><div class='noofbeds'>"+val1.categoryname+" &#xb7; " +val1.adultbeds+" Beds &#xb7; " +val1.floorname+"</div></div>" +
									"<div class='timings'><div class='col-md-6 col-sm-6 col-xs-6 padding-left-0 checkin'>Check-in: <h3>"+datetimefrmt(mystartdate)+"</h3><span class='datetime'> From 3:00 PM</span></div><div class='col-md-6 col-sm-6 col-xs-6 padding-right-0 checkout'>Checkout: <h3>"+datetimefrmt(myenddate)+"</h3><span class='datetime'>  At 11:00 AM</span></div></div>" +

									"<div class='description'><p class='addReadMore showlesscontent' id='dsc"+val1.hotelroomsid+"'>"+val1.description+"</p></div>" +
									"<div class='roomrates' id=ratid"+val1.hotelroomsid+"><p>Total for Your Stay:</p><h3>&pound;"+val1.cost+"</h3><span> (&pound;"+val1.roomrate+"/night)</span></div>" +

									

									
									
									"<input type='button' value='Reserve' onClick='selectRooms(\""+val1.hotelroomsid+"\")' class='btn btn-success col-md-14 col-sm-12 col-xs-12 padding-left-0 btn btn-success' /></div>" +
									"</div><div class='clearfix'></div>");
			} //end inner loop
		  }
	} //end out for
		
		
		
		for(var i = 0; i < currentlyAvailableRooms['data'].length; i++){
			
			$("#imgdiv"+currentlyAvailableRooms['data'][i].hotelroomsid+"").append('<div class="container">'+
					    
					  '<div id="myCarousel'+currentlyAvailableRooms['data'][i].hotelroomsid+'" class="carousel slide" data-ride="carousel">'+
					    '<!-- Indicators -->'+
					    '<ol class="carousel-indicators" id="lnkimgs'+currentlyAvailableRooms['data'][i].hotelroomsid+'">'+
					      
					    '</ol>'+

					    '<!-- Wrapper for slides -->'+
					    '<div class="carousel-inner" id="sliddv'+currentlyAvailableRooms['data'][i].hotelroomsid+'">'+
					      
					    '</div>'+

					    '<!-- Left and right controls -->'+
					    '<a class="left carousel-control" href="#myCarousel'+currentlyAvailableRooms['data'][i].hotelroomsid+'" data-slide="prev">'+
					      '<span class="glyphicon glyphicon-chevron-left"></span>'+
					      '<span class="sr-only">Previous</span>'+
					    '</a>'+
					    '<a class="right carousel-control" href="#myCarousel'+currentlyAvailableRooms['data'][i].hotelroomsid+'" data-slide="next">'+
					      '<span class="glyphicon glyphicon-chevron-right"></span>'+
					      '<span class="sr-only">Next</span>'+
					    '</a>'+
					  '</div>'+
					'</div>'+
					'');
		} //end for
		for(var i = 0; i < currentlyAvailableRooms['data'].length; i++){
			var imglengths;
			if(!currentlyAvailableRooms['data'][i]['roomvideo']){
				if(currentlyAvailableRooms['data'][i]['images'] ) {
					
					imglengths = currentlyAvailableRooms['data'][i]['images'].length;
				} else {
					imglengths = 0;
				}
				
			} else { 
			
				if(currentlyAvailableRooms['data'][i]['images'] ) {
				imglengths = currentlyAvailableRooms['data'][i]['images'].length+1;
				} else {
					imglengths = 1;
				}
			}
			
			for(var j = 0; j < imglengths; j++){
				if(j == 0){
				$("#lnkimgs"+currentlyAvailableRooms['data'][i].hotelroomsid+"").append('<li data-target="#myCarousel'+currentlyAvailableRooms['data'][i].hotelroomsid+'" data-slide-to="'+j+'" class="active"></li>');
				} else {
					$("#lnkimgs"+currentlyAvailableRooms['data'][i].hotelroomsid+"").append('<li data-target="#myCarousel'+currentlyAvailableRooms['data'][i].hotelroomsid+'" data-slide-to="'+j+'" class=""></li>');
				}
				
			} // end for
		}
		//'<img src="'+pageURL+"/assets/images/"+currentlyAvailableRooms['data'][i]['images'][j]['imagefilename']+'" alt="Los Angeles" style="width:100%;">'+
		
		for(var i = 0; i < currentlyAvailableRooms['data'].length; i++){
			if(currentlyAvailableRooms['data'][i]['images']){
			for(var j = 0; j< currentlyAvailableRooms['data'][i]['images'].length; j++ ){
				if( j == 0) {
				$("#sliddv"+currentlyAvailableRooms['data'][i].hotelroomsid+"").append('<div class="item active">'+
						'<img src="'+pageURL+"/assets/images/"+currentlyAvailableRooms['data'][i]['images'][j]['imagefilename']+'" alt="Los Angeles" style="width:100%;">'+
			      '</div>');
				} else {
					$("#sliddv"+currentlyAvailableRooms['data'][i].hotelroomsid+"").append('<div class="item" id="imfrdv'+currentlyAvailableRooms['data'][i].hotelroomsid+'">'+
							'<img src="'+pageURL+"/assets/images/"+currentlyAvailableRooms['data'][i]['images'][j]['imagefilename']+'" alt="Los Angeles" style="width:100%;">'+
				      '</div>');
					
				}
				
			} //end inner for
			
		}	
			if(!currentlyAvailableRooms['data'][i]['roomvideo']){}
			else{$("#sliddv"+currentlyAvailableRooms['data'][i].hotelroomsid+"").append('<div class="item" id="imfrdv'+currentlyAvailableRooms['data'][i].hotelroomsid+'">'+
					'<video class="videos" controls="" style="width:100%;"  >'+
					'<source src="'+pageURL+"/assets/images/"+currentlyAvailableRooms['data'][i]['roomvideo']+'" alt="Los Angeles" style="width:100%;" type="video/mp4">'+
					'</video>'+
		      '</div>');}
		} //end outer for
		

		AddReadMore();
		
	} //end function
	
		
	
	
	var selectedroomsforrsv = new Array();
	
	function selectRooms(id)
	{
		for (var key1 in currentlyAvailableRooms) {
			if (currentlyAvailableRooms.hasOwnProperty(key1)) {
				var room = currentlyAvailableRooms[key1]; 
				
					for(var i = 0; i<room.length; i++)
					{
						if(room[i].hotelroomsid == id){	
						
						   insertSelectedRooms(currentlyAvailableRooms.data[i]);
						   
						} //end inner if  
					}
				}//end if
        } //end for loop
		
		if(!mltplrmrsrv){
			
			$('#roomsuidiv').empty();
		}
		printslctdroomonstptwo();
		$('#wizard').smartWizard('goToStep', 2);
		
	} // end function
		
	
	function printslctdroomonstptwo(){
		$("#slctdromdiv").empty();
		for(var i = 0; i < selectedroomsforrsv.length; i++){
		$("#slctdromdiv").append("" +
				"<h1 class='col-md-12 col-sm-12 col-xs-12'>Reserving... </h1>" +
				"<div class=' col-md-12 col-sm-12 col-xs-12 '><div class='reservationinfo searchresult'>"+
				
				"<div class='roominfo'><h3 class='roomno'>"+selectedroomsforrsv[i].roomno+"</h3><div class='noofbeds'>"+selectedroomsforrsv[i].categoryname+"  &#xb7; " +selectedroomsforrsv[i].adultbeds+ " Beds &#xb7; " +selectedroomsforrsv[i].floorname+"</div></div>" +
									"<div class='timings'><div class='col-md-6 col-sm-6 col-xs-6 padding-left-0 checkin'>Check-in: <h3>"+datetimefrmt(selectedroomsforrsv[i].startdate)+"</h3><span class='datetime'> From 3:00 PM </span></div><div class='col-md-6 col-sm-6 col-xs-6 padding-right-0 checkout'>Checkout: <h3>"+datetimefrmt(selectedroomsforrsv[i].enddate)+"</h3><span class='datetime'>  At 11:00 AM</span></div></div>" +
									"<div class='roomrates'><p>Total for Your Stay:</p><h3>&pound;"+selectedroomsforrsv[i].cost+"</h3><span>(&pound;"+selectedroomsforrsv[i].roomrate+"/night)</span></div>" +
									
									
						
				"</div>"+
				"<div class='updatereservation col-md-12 col-sm-12 col-xs-12 padding-right-0 padding-left-0'><input type='button' value='Change Reservation' class='btn btn-success col-md-12 col-sm-12 col-xs-12' onclick='gotostepone()'></div>");
		} //end inner loop
		
		$("#slctdromdiv2").empty();
		for(var i = 0; i < selectedroomsforrsv.length; i++){
		$("#slctdromdiv2").append("<div>" +
				"<h1>Your Reservation Details</h1>" +
				"<div class='reservationinfo searchresult col-md-12 col-sm-12 col-xs-12'>"+
				
				"<div class='roominfo'><h3 class='roomno'>"+selectedroomsforrsv[i].roomno+"</h3><div class='noofbeds'>"+selectedroomsforrsv[i].categoryname+"  &#xb7; " +selectedroomsforrsv[i].adultbeds+ " Beds &#xb7; " +selectedroomsforrsv[i].floorname+"</div></div>" +
									"<div class='timings'><div class='col-md-6 col-sm-6 col-xs-6 padding-left-0 checkin'>Check-in: <h3>"+datetimefrmt(selectedroomsforrsv[i].startdate)+"</h3><span class='datetime'> From 3:00 PM </span></div><div class='col-md-6 col-sm-6 col-xs-6 padding-right-0 checkout'>Checkout: <h3>"+datetimefrmt(selectedroomsforrsv[i].enddate)+"</h3><span class='datetime'>  At 11:00 AM</span></div></div>" +
									"<div class='roomrates'><p>Total for your stay:</p><h3 class='seleroomcost'>&pound;"+selectedroomsforrsv[i].cost+"</h3><span class='seleroomcoststay'>(&pound;"+selectedroomsforrsv[i].roomrate+"/night)</span></div>" +
									
									
						
				"</div>"+
				"<div class='updatereservation col-md-12 col-sm-12 col-xs-12 padding-right-0 padding-left-0'><input type='button' value='Change Reservation' class='btn btn-success col-md-12 col-sm-12 col-xs-12 link' onclick='gotostepone()'></div>");
		} //end inner loop
		
		$("#slctdromdiv3").empty();
		for(var i = 0; i < selectedroomsforrsv.length; i++){
		$("#slctdromdiv3").append("<div>" +
				"<h1>Your Reservation Details</h1>" +
				"<div class='reservationinfo searchresult col-md-12 col-sm-12 col-xs-12'>"+
				
				"<div class='roominfo'><h3 class='roomno'>"+selectedroomsforrsv[i].roomno+"</h3><div class='noofbeds'>"+selectedroomsforrsv[i].categoryname+"  &#xb7; " +selectedroomsforrsv[i].adultbeds+ " Beds &#xb7; " +selectedroomsforrsv[i].floorname+"</div></div>" +
									"<div class='timings'><div class='col-md-6 col-sm-6 col-xs-6 padding-left-0 checkin'>Check-in: <h3>"+datetimefrmt(selectedroomsforrsv[i].startdate)+"</h3><span class='datetime'> From 3:00 PM </span></div><div class='col-md-6 col-sm-6 col-xs-6 padding-right-0 checkout'>Checkout: <h3>"+datetimefrmt(selectedroomsforrsv[i].enddate)+"</h3><span class='datetime'>  At 11:00 AM</span></div></div>" +
									"<div class='roomrates'><p>Total for your stay:</p><h3 class='seleroomcost'>&pound;"+selectedroomsforrsv[i].cost+"</h3><span class='seleroomcoststay'>(&pound;"+selectedroomsforrsv[i].roomrate+"/night)</span></div>" +
									
									
						
				"</div>");
		} //end inner loop
				
	} //end function
	
	function gotostepone(){
		selectedroomsforrsv.pop();
		resetavailablity();
		$('#wizard').smartWizard('goToStep', 1);
	} //end function
	
	function resetavailablity(){
				
		
				
				$('#startdate').val("");
				$('#enddate').val("");
				
				$('#cstmRsvdRoms').empty();
				
				
	} //end function 
	
	function insertSelectedRooms(data)
	{
		$("#searchrslts").hide();
		
		var startdate = mystartdate;
		var enddate = myenddate;
		
		data.startdate = startdate;
		data.enddate = enddate;
				
		selectedroomsforrsv.push(data);
				
        getReservedRoomsForGuestAssignment();
	
	} //end function

	
	
	
	function addCustomerData()
	{
		
		saveGuestData();
	
	} //end fucntion
	
	var guestsclctn = [];
	var guestcounter = 0;
	var confirmationemail;
	function saveGuestData(){
		
		
		var fname = $('#fname').val();
		var lname = $('#lname').val();
		var cellno = $('#cellno').val();
		var landline = $('#landline').val();
		var email = $('#email').val();
		var gender = $('#gender').val();
		var age = $('#age').val();
		var street = $('#street').val();
		var city = $('#city').val();
		var county = $('#county').val();
		var postcode = $('#postcode').val();
		var country = $('#country').val();
		var cstmRsvdRoms = $('#cstmRsvdRoms').val();
		var comments = $('#comments').val();
		
		//confirmation email
		confirmationemail = email;
		
		var guest = {'id': guestcounter, 'fname': fname, 'lname': lname, 'cellno': cellno, 'landline': landline, 'email': email, 'gender': gender, 'age': age, 'street': street, 'city': city, 'county': county, 'postcode': postcode, 'country': country, 'cstmRsvdRoms': cstmRsvdRoms, 'comments': comments, 'checkin' : '0000-00-00 00:00:00'}
		
		guestsclctn[0] = (guest);
		//guestsclctn.push(guest); for multiple guest
		
		guestcounter++;
		printgustrscdtls();
		gotostepthree();
	} //end function 
	
	function printgustrscdtls(){
		$("#gstdatadiv").empty();
		for(var i = 0; i < guestsclctn.length; i++){
		$("#gstdatadiv").append("<div>" +
				"<h1>Your Details</h1>" +
				"<div class='infocont'><h3 class=''>"+guestsclctn[i].fname+" "+guestsclctn[i].lname+"</h3>"+
				"<div class='info'>Mobile Number<br><label>"+guestsclctn[i].cellno+"</label></div>" +
				"<div class='info'>Email<br><label>"+guestsclctn[i].email+"</label></div>" +
				"<div class='info'>Gender<br><label>"+guestsclctn[i].gender+"</label></div>" +
				"<div class='info'>Age<br><label>"+guestsclctn[i].age+"</label></div>" +
				"<div class='info'>Street<br><label>"+guestsclctn[i].street+"</label></div>" +
				"<div class='info'>Town/City<br><label>"+guestsclctn[i].city+"</label></div>" +
				"<div class='info'>County<br><label>"+guestsclctn[i].county+"</label></div>" +
				"<div class='info'>Post Code<br><label>"+guestsclctn[i].postcode+"</label></div>" +
				"<div class='info'>Country<br><label>"+guestsclctn[i].country+"</label></div>" +
				"<div class='info'>Comments<br><label>"+guestsclctn[i].comments+"</label></div></div>" +
				"<div class='updatereservation'><input type='button' value='Amend Your Details' class='btn btn-primary' onclick='gotosteptwo()'></div>" +
				"<div class='total' id='rsvcost'></div>" +
				"<div class='col-md-12 col-sm-12 col-xs-12 padding-left-0 padding-right-0 reservedfour'><input type='button' value='Continue to Confirmation' class='btn btn-primary col-md-12 col-sm-12 col-xs-12' style='margin-bottom:90px;' onclick='gotostepfour()'></div>" +
				"</div>");
		} //end inner loop
		
		
		
		for(var i = 0; i < selectedroomsforrsv.length; i++){
			$("#rsvcost").append('<p >Total for your stay</p><h3 class="seleroomcost">&pound;'+selectedroomsforrsv[i].cost+'</h3>'+ '<span class="seleroomcoststay">(&pound;'+selectedroomsforrsv[i].roomrate+'/night)</span>');
			} //end inner loop
		
		$('#confirmemail').empty();
		$('#confirmemail').append("<label>"+confirmationemail+"</label>");
		
	} //end function
	
function gotosteptwo(){
	guestsclctn.pop();
		$('#wizard').smartWizard('goToStep', 2);
	} //end function
	
	function gotostepthree(){
		
		$('#wizard').smartWizard('goToStep', 3);
	} //end function
	
function gotostepfour(){
		saveallreservationdata();
		$('#wizard').smartWizard('goToStep', 4);
	} //end function
	
	function deleteGuest(guestconter){
		
		guestsclctn.pop();
		
	} //end function
	
	var guestAsgndRoms;
	
	
	function getReservedRoomsForGuestAssignment()
	{
		
		guestAsgndRoms = selectedroomsforrsv;
			
		
		populateRoomsForGuestAssignment();
		
	} // end function 
	
	
	function populateRoomsForGuestAssignment()
	{
		
		$('#cstmRsvdRoms').empty();
		$('#cstmRsvdRoms1').empty();
		for(var key in guestAsgndRoms){
			val1 = guestAsgndRoms[key];
			
			$("#cstmRsvdRoms").val(val1.hotelroomsid);

			
		} //end inner loop
		
	} //end function
	
	function clearAllFormsFields()
	{
	
		$('#startdate').val("");
		$('#enddate').val("");
		
		$('#cstmRsvdRoms').empty();
		
		$('#stDate').val("");
		$('#enDate').val("");
				
		currentlyAvailableRooms = null;
		
		
	} //end function
	var rsvobj = {};
	function saveallreservationdata(){
		rsvobj.startdate = mystartdate;
		rsvobj.enddate = myenddate; 
		rsvobj.totaladults = intadults;
		rsvobj.totalchilds = intchild;
		rsvobj.totalinfants = intinfant;
				
		var branchid = $('#hotelId').val();
		$.ajax({
		    type: "post",
		    url: ""+pageURL+"/pblcreservationController/saveallreservationdata",
		    data:{branchid, selectedroomsforrsv, guestsclctn, rsvobj},
		    error: function(returnval) {
		        
				if(returnval.status == 0)
				 {
					 var title = "Oops!";
					var text = "Internet Connection is Off. Please check your Internet Connection!";
					var type = "error";
					customNotification(title, text, type);
				 } else if (returnval.status == 500) {
					 var title = "Oops!";
					var text = "Server is Off. Please Wait!";
					var type = "error";
					customNotification(title, text, type);
				 } else if (returnval.status == 404) {
					 var title = "Oops!";
					var text = "Server Component is not Found!";
					var type = "error";
					customNotification(title, text, type);
				 } else {
					 var title = "Oops!";
					var text = "Sorry. Unable to Save Billing Data!";
					var type = "error";
					customNotification(title, text, type);
				 }
		    },
		    success: function (data) {
		    	
		    	var result = $.parseJSON(data);
		    	
					if(result)
					{
						var title = "Congrats!";
						var text = "Room has been reserved successfully!";
						var type = "success";
						//customNotification(title, text, type);
						//showStrtRsv(result);
						
						
					} else {
						var title = "Oops!";
						var text = "Sorry. Unable to Save Billing Data!";
						var type = "error";
						//customNotification(title, text, type);
					}
		    }
		});
		
		
	
} //end function 

	
	
	