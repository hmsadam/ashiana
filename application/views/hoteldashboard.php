<?php include("header.php")?>
<?php include("menu.php")?>



<body class="nav-md">


      <!-- page content -->
      <div class="right_col" role="main">

        <!-- top tiles -->
        <div class="row tile_count">
          <div class="animated flipInY col-md-2 col-sm-4 col-xs-4 tile_stats_count">
            <div class="left"></div>
            <div class="right">
              <span class="count_top">Total Rooms</span>
              <div class="count blue" id="ttlrmsstt"></div>
              
					
            </div>
          </div>
          <div class="animated flipInY col-md-2 col-sm-4 col-xs-4 tile_stats_count">
            <div class="left"></div>
            <div class="right">
              <span class="count_top">Vacant Rooms</span>
              <div class="count red" id="vcntrmsstt"></div>
              
            </div>
          </div>
          <div class="animated flipInY col-md-2 col-sm-4 col-xs-4 tile_stats_count">
            <div class="left"></div>
            <div class="right">
              <span class="count_top">Under Maintenance</span>
              <div class="count red" id="undrminrmsstt"></div>
              
            </div>
          </div>
          <div class="animated flipInY col-md-2 col-sm-4 col-xs-4 tile_stats_count">
            <div class="left"></div>
            <div class="right">
              <span class="count_top">Rooms Reserved</span>
              <div class="count green" id="rsvrmsrmsstt"></div>
              
            </div>
          </div>
          <div class="animated flipInY col-md-2 col-sm-4 col-xs-4 tile_stats_count">
            <div class="left"></div>
            <div class="right">
              <span class="count_top">Active/Confirmed Reservations</span>
              
              <div class="count green" id="ttlrsvnsrmsstt"></div>
              
            </div>
          </div>
          <div class="animated flipInY col-md-2 col-sm-4 col-xs-4 tile_stats_count">
            <div class="left"></div>
            <div class="right">
              <span class="count_top">Guests Checked In</span>
              <div class="count green" id="gstchinsrmsstt"></div>
              
            </div>
          </div>

        </div>
        <!-- /top tiles -->

        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="dashboard_graph">

              <div class="row x_title">
                <div class="col-md-6">
                  <h3>Hotel Rooms <small>Room Reservation Trends</small></h3>
				  <?php 
				  $testvalues = array('10', '12', '21', '54', '260', '830', '710');
				  //echo "Jamshaid";
				  //print_r($testvalues);
				  //$dbname = $this->session->userdata('groupid');
				  //echo $dbname;
				  ?>
                </div>
                <div class="col-md-6">
                  <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc">
                    <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                    <span>December 30, 2014 - January 28, 2015</span> <b class="caret"></b>
                  </div>
                </div>
              </div>

              <div class="col-md-12 col-sm-12 col-xs-12">
                <div id="placeholder33" style="height: 260px; display: none" class="demo-placeholder"></div>
                <div style="width: 100%;">
                  <div id="canvas_dahs" class="demo-placeholder" style="width: 100%; height:270px;"></div>
                </div>
              </div>
			  
              <!-- start pie area-->
			  
			  
              <!-- end pie area -->

              <div class="clearfix"></div>
            </div>
          </div>

        </div>
        
        <!-- Financial Graph starts -->
        
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="dashboard_graph">

              <div class="row x_title">
                <div class="col-md-6">
                  <h3>Hotel Income <small>Daily Financial Trends</small></h3>
				  <?php 
				  $testvalues2 = array('1000', '1020', '2010', '540', '2600', '830', '710');
				  //echo "Jamshaid";
				  //print_r($testvalues);
				  //$dbname = $this->session->userdata('groupid');
				  //echo $dbname;
				  ?>
                </div>
                <div class="col-md-6">
                  <div id="fnreportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc">
                    <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                    <span>December 30, 2014 - January 28, 2015</span> <b class="caret"></b>
                  </div>
                </div>
              </div>

              <div class="col-md-12 col-sm-12 col-xs-12">
                <div id="placeholder33" style="height: 260px; display: none" class="demo-placeholder"></div>
                <div style="width: 100%;">
                  <div id="canvas_dahs2" class="demo-placeholder" style="width: 100%; height:270px;"></div>
                </div>
              </div>
			  
              <!-- start pie area-->
			  
			  
              <!-- end pie area -->

              <div class="clearfix"></div>
            </div>
          </div>

        </div>
        
        <!-- Financial Graph ends -->
        <br />

        <!-- footer content -->

        <footer>
          <div class="copyright-info">
            <p class="pull-right">Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>  
            </p>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
      <!-- /page content -->

    </div>

  </div>

  <div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
  </div>

  <script src="<?php echo base_url();?>/assets/js/bootstrap.min.js"></script>

  
  <!-- bootstrap progress js -->
  <script src="<?php echo base_url();?>/assets/js/progressbar/bootstrap-progressbar.min.js"></script>
  <script src="<?php echo base_url();?>/assets/js/nicescroll/jquery.nicescroll.min.js"></script>
  <!-- icheck -->
  <script src="<?php echo base_url();?>/assets/js/icheck/icheck.min.js"></script>
  <!-- daterangepicker -->
  <script type="text/javascript" src="<?php echo base_url();?>/assets/js/moment/moment.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>/assets/js/datepicker/daterangepicker.js"></script>
  <!-- chart js -->
  <script src="<?php echo base_url();?>/assets/js/chartjs/chart.min.js"></script>

  <script src="<?php echo base_url();?>/assets/js/custom.js"></script>

  <!-- flot js -->
  <!--[if lte IE 8]><script type="text/javascript" src="js/excanvas.min.js"></script><![endif]-->
  <script type="text/javascript" src="<?php echo base_url();?>/assets/js/flot/jquery.flot.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>/assets/js/flot/jquery.flot.pie.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>/assets/js/flot/jquery.flot.orderBars.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>/assets/js/flot/jquery.flot.time.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>/assets/js/flot/date.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>/assets/js/flot/jquery.flot.spline.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>/assets/js/flot/jquery.flot.stack.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>/assets/js/flot/curvedLines.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>/assets/js/flot/jquery.flot.resize.js"></script>
  
  
<!-- PNotify -->
  <script type="text/javascript" src="<?php echo base_url();?>assets/js/notify/pnotify.core.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>assets/js/notify/pnotify.buttons.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>assets/js/notify/pnotify.nonblock.js"></script>
  
  <script src="<?php echo base_url();?>assets/js/hmsappscripts/mainscript.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/js/hmsappscripts/hoteldashboardcript.js" type="text/javascript"></script>
  
  
  
  
  <script>
    /*$(document).ready(function() {
      // [17, 74, 6, 39, 20, 85, 7]
      //[82, 23, 66, 9, 99, 6, 2]
      var data1 = [
        [gd(2012, 1, 1), 17],
        [gd(2012, 1, 2), 74],
        [gd(2012, 1, 3), 6],
        [gd(2012, 1, 4), 39],
        [gd(2012, 1, 5), 20],
        [gd(2012, 1, 6), 85],
        [gd(2012, 1, 7), 7]
      ];

      var data2 = [
        [gd(2012, 1, 1), 82],
        [gd(2012, 1, 2), 23],
        [gd(2012, 1, 3), 66],
        [gd(2012, 1, 4), 9],
        [gd(2012, 1, 5), 119],
        [gd(2012, 1, 6), 6],
        [gd(2012, 1, 7), 9]
      ];
      $("#canvas_dahs").length && $.plot($("#canvas_dahs"), [
        data1, data2
      ], {
        series: {
          lines: {
            show: false,
            fill: true
          },
          splines: {
            show: true,
            tension: 0.4,
            lineWidth: 1,
            fill: 0.4
          },
          points: {
            radius: 0,
            show: true
          },
          shadowSize: 2
        },
        grid: {
          verticalLines: true,
          hoverable: true,
          clickable: true,
          tickColor: "#d5d5d5",
          borderWidth: 1,
          color: '#fff'
        },
        colors: ["rgba(38, 185, 154, 0.38)", "rgba(3, 88, 106, 0.38)"],
        xaxis: {
          tickColor: "rgba(51, 51, 51, 0.06)",
          mode: "time",
          tickSize: [1, "day"],
          //tickLength: 10,
          axisLabel: "Date",
          axisLabelUseCanvas: true,
          axisLabelFontSizePixels: 12,
          axisLabelFontFamily: 'Verdana, Arial',
          axisLabelPadding: 10
            //mode: "time", timeformat: "%m/%d/%y", minTickSize: [1, "day"]
        },
        yaxis: {
          ticks: 8,
          tickColor: "rgba(51, 51, 51, 0.06)",
        },
        tooltip: false
      });

      function gd(year, month, day) {
        return new Date(year, month - 1, day).getTime();
      }
    });*/
  </script>
  <!-- multiple charts -->
  
  <!-- echart -->
  <script src="<?php echo base_url();?>/assets/js/echart/echarts-all.js"></script>
  <script src="<?php echo base_url();?>/assets/js/echart/green.js"></script>
  
  
  <script> 
  $(document).ready(function(){
	
	}); //end readyfunction
  
	<?php foreach($testvalues as $key => $val){ ?>
    testvalues.push('<?php echo $val; ?>');
    	
<?php } ?>

  
  
 
  </script>
  
  
	<!-- end multiple charts -->
  <!-- worldmap -->
  <script type="text/javascript" src="<?php echo base_url();?>/assets/js/maps/jquery-jvectormap-2.0.3.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>/assets/js/maps/gdp-data.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>/assets/js/maps/jquery-jvectormap-world-mill-en.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>/assets/js/maps/jquery-jvectormap-us-aea-en.js"></script>
  <!-- pace -->
  <script src="<?php echo base_url();?>/assets/js/pace/pace.min.js"></script>
  <script src="<?php echo base_url();?>assets/js/hmsappscripts/customnotificationsscript.js" type="text/javascript"></script> 
  
  
  <script>
    NProgress.done();
  </script>
  <!-- /datepicker -->
  <!-- /footer content -->
</body>

</html>
