<?php

class floorsController extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
	

    if( !$this->session->userdata('isLoggedIn') ) {
        redirect('/login/show_login');
    }
  }

	function getHotelFloors()
	{
		$branchid = $this->input->post('branchid');
		//$branchid = 3;
		$db = $this->session->userdata('db');
		
		$this->load->model('hotelfloors');
	$floors = $this->hotelfloors->getFloorByBranch($branchid, $db);
	echo json_encode(array('data' => $floors));
	}
  
  function floorsView() {
    
	$this->load->model('hotelbranches');
	$branches = $this->hotelbranches->getHotelsByUser();
		
	$user_id = $this->session->userdata('id');
	$db = $this->session->userdata('db');
	
	$data['branches'] = $branches;
    $data['username'] = $this->session->userdata('name');
   ///log_message('info', 'The purpose of some variable is to provide some value.');
	$this->load->helper('form');
    $this->load->view('floorsView',$data);
  }

    
  function createNewHotelFloor()
  {
	  $floorname = $this->input->post('floorname');
	  $desc = $this->input->post('desc');
	  $branchid = $this->input->post('branchid');
	  
	  $db = $this->session->userdata('db');
	  
	  $userid = $this->session->userdata('id');
	 
	 
	
	  $this->load->model('hotelfloors');
	  $result = $this->hotelfloors->createNewHotelFloor($floorname, $desc, $branchid, $db, $userid);
	  
	 if($result == true)
	  {
		  //echo json_encode($result);
		  echo json_encode(true);
	  } else {
		  echo json_encode(false);
		  
	  }
	  
  }   //end function
      
  function updateHotelFloor()
  {
	  $floorid = $this->input->post('floorid');
	  $floorname = $this->input->post('floorname');
	  $desc = $this->input->post('desc');
	  
	  /*$floorid = "1";
	  $floorname = "Ground Floor";
	  $desc = "10 Rooms on the floor";*/
	  
	  $userid = $this->session->userdata('id');
	  $db = $this->session->userdata('db');
	  
	  
	 
	  $this->load->model('hotelfloors');
	  $result = $this->hotelfloors->updateHotelFloor($floorid, $floorname, $desc, $userid, $db);
	  //log_message('info', "Floor Updated");
	  if($result == true)
	  {
		  //echo json_encode($result);
		  echo json_encode(true);
	  } else {
		  echo json_encode(false);
		  
	  }
	  
	  
  }   //end function
  
  
} //end class
