<?php ?>

<?php include("header.php")?>



  
  <link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery-ui.css">

 

<body class="nav-md" style="background:#fff;">
<div class="maincont">
  <div class="cont">
  <div class="header row public_head">
    <div class="col-md-3 col-sm-3 col-xs-12 ">
    		<a href="http://ashiana.net/"><img src="<?php echo base_url();?>assets/images/hms/msr_logo_rgb.png" alt="" class="logo"  /></a>
    </div>
    <div class="col-md-9 col-sm-9 col-xs-12  txt-right links">
    	<a class="linkback" href="http://ashiana.net/">BACK TO MAIN SITE</a>
    </div><!--
    <div class="nav_client_menu">
          <nav class="" role="navigation">
            <div class="nav toggle">
              <a id="menu_toggle"><i class="fa fa-bars"></i></a>
            </div>

            <ul class="nav navbar-nav navbar-right">
				  
				  <li class="">
						<a href="#" class="linkback" >BACK TO MAIN SITE</a>	
				  </li>

            </ul>
          </nav>
        </div>
-->

<nav class="navbar navbar-expand-lg navbar-light bg-light row">
  <button class="linkbutton" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler navbar-toggler-icon"><i class="fa fa-bars"></i></span>
  </button>

  <div class="collapse navbar-collapse row" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
       <div class="txt-right links_nav">
    	<a href="https://ashiana.net/" class="linkback" >BACK TO MAIN SITE</a>
        </div>
      </li>
    </ul>
  </div>
</nav>
</div>

      
      <!-- page content -->
      
      <div class="right_col" role="main">
        

          
         <div class="clearfix"></div>
          <!-- start of data table -->
          <div class="row">

                <div class="col-md-8 col-sm-8 col-xs-11 m-auto form_wizard wizard_horizontal">
              <div class="">
              	<div class="page-title">
            <div class="title_left">
              <h3>Contact Market Sell Repeat</h3>
            </div>
           
          </div>
                

                <div class="x_content">
                  <br />
                  <form id="enquiryForm" data-parsley-validate class="form-horizontal form-label-left">

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="enqname">Your Name: <span class="required">*</span>
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12 padding-left-0">
                        <input type="text" id="enqname" name="enqname" required class="form-control col-md-7 col-xs-12">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="enqemail">Email Address: <span class="required">*</span>
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12 padding-left-0">
                        <input type="email" id="enqemail" name="enqemail" required class="form-control col-md-7 col-xs-12">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="teleno">Telephone Number: <span class="required">*</span>
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12 padding-left-0">
                        <input type="text" id="teleno" name="teleno" required class="form-control col-md-7 col-xs-12">
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label for="message" class="control-label col-md-3 col-sm-3 col-xs-12">Your Message:</label>
                      <div class="col-md-6 col-sm-6 col-xs-12 padding-left-0">
                        <textarea class="resizable_textarea form-control" style="width: 100%; overflow: hidden; word-wrap: break-word; resize: horizontal; height: 87px;" required id="message" name="message" placeholder="Any comments to your booking?" ></textarea> 
                      </div>
                    </div>
					
					
					
                    <div class="ln_solid"></div>
                    <div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 padding-left-0">
                        
                        <input type="reset" value="Cancel" class="btn btn-primary"> 
                        <a href="javascript:void(0);" id="save" class="btn btn-success"> Submit Message</a>
                      </div>
                    </div>

                  </form>
                </div>
              </div>
            </div>
        
      
          </div>
		  
		  <!-- end of data table -->
		 
		
		  
		
         

    	<div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
  </div>
  	  </div>
	<div class="clearfix"></div>
		<!-- end page content -->
<div class="header col-md-12 footer">
<div class="col-md-6 col-sm-6 col-xs-12 no-padding txt-right fweight" style="font-size:16px;float:right">
    	<a href="https://marketsellrepeat.co.uk/" >BACK TO MAIN SITE</a>
    </div>
<div class="col-md-6 col-sm-6 col-xs-12 no-padding cright " style="float:left;">
    		<label>&copy; 2019 <a href="https://marketsellrepeat.co.uk/">Market Sell Repeat.</a> All Rights Reserved.</label>
    </div>


    

</div>		
  </div>
  </div>

  <script src="<?php echo base_url();?>/assets/js/bootstrap.min.js"></script>

  <!-- bootstrap progress js -->
  <script src="<?php echo base_url();?>assets/js/progressbar/bootstrap-progressbar.min.js"></script>
  <script src="<?php echo base_url();?>assets/js/nicescroll/jquery.nicescroll.min.js"></script>
  <!-- icheck -->
  <script src="<?php echo base_url();?>assets/js/icheck/icheck.min.js"></script>
  <!-- tags -->
  <script src="<?php echo base_url();?>assets/js/tags/jquery.tagsinput.min.js"></script>
  <!-- switchery -->
  <script src="<?php echo base_url();?>assets/js/switchery/switchery.min.js"></script>
  
  <!-- richtext editor -->
  <script src="<?php echo base_url();?>assets/js/editor/bootstrap-wysiwyg.js"></script>
  <script src="<?php echo base_url();?>assets/js/editor/external/jquery.hotkeys.js"></script>
  <script src="<?php echo base_url();?>assets/js/editor/external/google-code-prettify/prettify.js"></script>
  <!-- select2 -->
  <script src="<?php echo base_url();?>assets/js/select/select2.full.js"></script>
  <!-- form validation -->
  <script type="text/javascript" src="<?php echo base_url();?>assets/js/parsley/parsley.min.js"></script>
  <!-- textarea resize -->
  <script src="<?php echo base_url();?>assets/js/textarea/autosize.min.js"></script>
  <script>
    autosize($('.resizable_textarea'));
  </script>
 
<script src="<?php echo base_url();?>assets/js/jquery-ui.js"></script>
  <script src="<?php echo base_url();?>assets/js/hmsappscripts/customnotificationsscript.js" type="text/javascript"></script>
  <!-- pace -->
  <script src="<?php echo base_url();?>assets/js/pace/pace.min.js"></script>
  
  <script src="<?php echo base_url();?>assets/js/custom.js"></script>
  
  <!-- form wizard -->
  <script type="text/javascript" src="<?php echo base_url();?>assets/js/wizard/jquery.smartWizard.js"></script>
  
  <script src="<?php echo base_url();?>assets/js/hmsappscripts/inputtags.js" type="text/javascript"></script> 


<script src="<?php echo base_url();?>assets/js/hmsappscripts/enquirypblcscript.js" type="text/javascript"></script>

 
  
 
</body>

</html>
