<?php

class usershashotelbranches extends CI_Model {

  

  function saveHotelUser($userId, $branchId, $db) {

    $data['usersofhotel_usersofhotelid'] = $userId;
    $data['hotelbranches_hotelbranchesid'] = $branchId;

	$data['createdon'] = date("Y-m-d H:i:s" ,time());
    
    $data['createdby'] = $userId; 
	
	$this->$db->trans_start();	
	$this->$db->insert('userhashotelbranches',$data);
	$insert_id = $this->$db->insert_id();
	$this->$db->trans_complete();
	if($insert_id)
	{
		return true;
	} else {
		return false;
	}
	
  } //end function 
  
  function getHotelUsers() {
	  //this function is updated for multiple DBs
	  $db = $this->session->userdata('db');
	$this->$db->trans_start();
    $this->$db->from('userhashotelbranches');
    $this->$db->where( array('usersofhotel_usersofhotelid'=>$this->session->userdata('id')) );

    $usersLabs = $this->$db->get()->result_array();
	$this->$db->trans_complete();
	return $usersLabs;
	
	
  } //end function
  //this function is not used/depriciated 
  function getUsersByLabId($labId) {
	$this->db->trans_start();
	$this->db->from('usershaslabbranches');
	$this->db->join('usersoflabs', 'usersoflabs.userId = usershaslabbranches.usersoflabs_userId','left');
	$this->db->where('usershaslabbranches.labbranches_labbranchid', $labId); 
	$result = $this->db->get()->result_array();
	$this->db->trans_complete();
	return $result;
	
  } //end function
  
  
  
  function getLabsById($userId) {
	$this->db->trans_start();
    $this->db->from('usershaslabbranches');
    $this->db->where( array('user_id'=>$userId) );

    $usersBranches = $this->db->get()->result_array();
	$this->db->trans_complete();
	return $usersBranches;
	
	
  } //end function
  
  function assignSchoolUser( $userId, $schoolIds) {

    for($i = 0; $i<sizeof($schoolIds);$i++){
		//assigns multiple schools to a user
		$data['user_id'] = $userId;
		$data['school_school_id'] = $schoolIds[$i]; 
		
		$this->db->insert('users_of_schools',$data);
		
	} //end for loop
	
	 return true;
	  
	 

    
  } //end function assignSchoolUser
  
  function reAssignSchoolUser( $userId, $schoolIds) {
	  //delete previous entries of schools
	  $this->db->where('user_id', $userId);
		$this->db->delete('users_of_schools');
		
		//assign new schools
		for($i = 0; $i<sizeof($schoolIds);$i++){
		
			$data['user_id'] = $userId;
			$data['school_school_id'] = $schoolIds[$i]; 
			
			$this->db->insert('users_of_schools',$data);
		
		} //end for loop
	return true;
	
  } //end function reAssignSchoolUser
  
  
  
  
  
	function getSchoolUsersBySchoolId($schoolId) {

    $this->db->from('users_of_schools');
    $this->db->where( array('school_school_id'=>$schoolId) );

    $schoolUsers = $this->db->get()->result_array();
	return $schoolUsers;
	
	
  } //end function

  
	//utilitiy function
	function provideCompareableArray()
	{
		$resultArray = array();
		$this->db->from('users_of_schools');
    	$schoolsOfUsers = $this->db->get()->result_array();
		
		return $schoolsOfUsers;
		
		
	} //end function
  
  
  
} //end class
