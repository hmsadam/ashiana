<?php
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

  <title>Hotels Management System! | </title>

  <!-- Bootstrap core CSS -->

  <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">

  <link href="<?php echo base_url();?>assets/fonts/css/font-awesome.min.css" rel="stylesheet">
  <link href="<?php echo base_url();?>assets/css/animate.min.css" rel="stylesheet">

  <!-- Custom styling plus plugins -->
  <link href="<?php echo base_url();?>assets/css/custom.css" rel="stylesheet">
  <link href="<?php echo base_url();?>assets/css/icheck/flat/green.css" rel="stylesheet">


  <script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>

  <!--[if lt IE 9]>
        <script src="../assets/js/ie8-responsive-file-warning.js"></script>
        <![endif]-->

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

</head>


<body class="nav-md">

  <div class="container body">


    <div class="main_container">

      

     
      <!-- page content -->
      <div class="right_col"  role="main">

        <div class="">
          
          <div class="clearfix"></div>

          <div class="row">
            <div class="col-md-12">
              <div class="x_panel">
                
                <div class="x_content">

                  <section class="content invoice">
                    <!-- title row -->
                    <div class="row">
                      <div class="col-xs-12 invoice-header">
                        <h1>
                                        <i class="fa fa-globe"></i> Reservation Card!
                                        <small class="pull-right">Date: <?php $date = date('m/d/Y h:i:s a', time()); echo $date;?></small>
                                    </h1>
                      </div>
                      <!-- /.col -->
                    </div>
                    <!-- info row -->
                    <div class="row invoice-info">
                      <div class="col-sm-4 invoice-col">
                        From
                        <address>
                                        <strong><?php echo $branch['0']['branchtitle']?></strong>
                                        <br><?php echo $branch['0']['address']?>
                                        
                                        <br>Phone: <?php echo $branch['0']['landlineno']?>
                                        <br>Email: <?php echo $branch['0']['email']?>
                                    </address>
                      </div>
                      <!-- /.col -->
                      <div class="col-sm-4 invoice-col">
                        To
                        <address>
                                        <?php 
                                        for($i = 0; $i<sizeof($gstdata); $i++)
                                        {
                                        	if(!$gstdata[$i]['isAFamilyMember'])
                                        	{
                                        		?>
                                        		<strong><?php echo $gstdata[$i]['firstname']." ".$gstdata[$i]['lastname'];?></strong>
                                        		<br><?php echo $gstdata[$i]['street'].", ".$gstdata[$i]['city'];?>
		                                        <br><?php echo $gstdata[$i]['county'].", ".$gstdata[$i]['country'];?>
		                                        <br>Phone: <?php echo $gstdata[$i]['landLineNo'].", ".$gstdata[$i]['cellNo'];?>
		                                        <br>Email: <?php echo $gstdata[$i]['email'];?>
                                        		<?php
                                        	}//end if
                                        } //end for loop
                                        
                                        ?>
                                        
                                       
                                    </address>
                      </div>
                      <!-- /.col -->
                      <div class="col-sm-4 invoice-col">
                        <b>Reservation # <?php echo $rsvid;?></b>
                        <br>
                        
                      </div>
                      <!-- /.col -->
                    </div>
                    <!-- /.row -->

                    <!-- Table row -->
                    <div class="row">
                      <div class="col-xs-12 table">
                      <p class="lead">Rooms Info</p>
                        <table class="table table-striped">
                          <thead>
                            <tr>
                              <th>Room No.</th>
                              <th>Floor</th>
                              <th>Room Category</th>
                              <th>Reservation Start Date</th>
                              <th>Reservation End Date</th>
                              <th>Room Rate</th>
                              
                            </tr>
                          </thead>
                          <tbody>
                          <?php 
                                        for($i = 0; $i<sizeof($roomsdata); $i++)
                                        { ?>
                            <tr>
                              <td><?php echo $roomsdata[$i]['roomno'];?></td>
                              
                              <td><?php echo $roomsdata[$i]['floorname'];?></td>
                              <td><?php echo $roomsdata[$i]['categoryname'];?></td>
                              <td><?php echo $roomsdata[$i]['startdate'];?></td>
                              <td><?php echo $roomsdata[$i]['enddate'];?></td>
                              <td><?php echo $roomsdata[$i]['roomrate'];?></td>
                            </tr>
                            <?php } //end for loop?>
                          </tbody>
                        </table>
                      </div>
                      <!-- /.col -->
                    </div>
                    <!-- /.row -->

                    <div class="row">
                      <!-- accepted payments column -->
                      <div class="col-xs-6">
                      <p class="lead">Reservation Info</p>
                      <div class="table-responsive">
                          <table class="table">
                            <tbody>
                              <tr>
                                <th style="width:50%">Reservation Duration:</th>
                                <td><?php echo $rsvdata[0]['duration'];?></td>
                              </tr>
                              <tr>
                                <th>No. of Rooms Reserved:</th>
                                <td><?php echo $rsvdata[0]['noofroom'];?></td>
                              </tr>
                              <tr>
                                <th>No. of Adults:</th>
                                <td><?php echo $rsvdata[0]['totalreservedadults'];?></td>
                              </tr>
                              <tr>
                                <th>No. of Childs:</th>
                                <td><?php echo $rsvdata[0]['totalreservedchilds'];?></td>
                              </tr>
                              <tr>
                                <th>No. of Infants:</th>
                                <td><?php echo $rsvdata[0]['totalinfants'];?></td>
                              </tr>
                              <tr>
                                <th>Guest Arival Date & Time:</th>
                                <td><?php echo $rsvdata[0]['guestarrivaldate'];?></td>
                              </tr>
                              <tr>
                                <th>Guest Departure Date & Time:</th>
                                <td><?php echo $rsvdata[0]['gestdeparturedate'];?></td>
                              </tr>
                              <tr>
                                <th>Reserved On:</th>
                                <td><?php echo $rsvdata[0]['reservationdate'];?></td>
                              </tr>
                              
                            </tbody>
                          </table>
                        </div>
                        
                        <img src="<?php echo base_url();?>assets/images/visa.png" alt="Visa">
                        <img src="<?php echo base_url();?>assets/images/mastercard.png" alt="Mastercard">
                        <img src="<?php echo base_url();?>assets/images/american-express.png" alt="American Express">
                        <img src="<?php echo base_url();?>assets/images/paypal2.png" alt="Paypal">
                        <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                          Please keep Reservation Card during the stay at Hotel!
                        </p>
                      </div>
                      <!-- /.col -->
                      <div class="col-xs-6">
                        <p class="lead">Billing Info</p>
                        <div class="table-responsive">
                          <table class="table">
                            <tbody>
                              <tr>
                                <th style="width:50%">Room(S) Cost:</th>
                                <td><?php echo $billdata[0]['roomscost'];?></td>
                              </tr>
                              <tr>
                                <th>Tax: </th>
                                <td><?php echo $billdata[0]['taxamount'];?></td>
                              </tr>
                              <tr>
                                <th>Other Charges:</th>
                                <td><?php echo $billdata[0]['othercharges'];?></td>
                              </tr>
                              <tr>
                                <th>Sub Total: </th>
                                <td><?php echo $billdata[0]['subtotal'];?></td>
                              </tr>
                              <tr>
                                <th>Rebate%: </th>
                                <td><?php echo $billdata[0]['rebatepercent'];?>%</td>
                              </tr>
                              <tr>
                                <th>Rebate Amount: </th>
                                <td><?php echo $billdata[0]['rebateamount'];?></td>
                              </tr>
                              <tr>
                                <th>Grand Total: </th>
                                <td><?php echo $billdata[0]['grandtotal'];?></td>
                              </tr>
                              <tr>
                                <th>Bill Paid: </th>
                                <td><?php echo $billdata[0]['paymentpaid'];?></td>
                              </tr>
                              <tr>
                                <th>Balance: </th>
                                <td><?php echo $billdata[0]['balance'];?></td>
                              </tr>
                              <tr>
                                <th>Payment Method: </th>
                                <td><?php echo $billdata[0]['paymentmethod'];?></td>
                              </tr>
                            </tbody>
                          </table>
                          <button class="btn btn-success pull-right" onclick="window.print();"><i class="fa fa-print"></i> Print</button>
                        </div>
                      </div>
                      <!-- /.col -->
                    </div>
                    <!-- /.row -->

                    <!-- this row will not appear when printing -->
                    
                  </section>
                </div>
              </div>
            </div>
          </div>
        </div>

        

      </div>
      <!-- /page content -->
    </div>

  </div>

  <div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
  </div>

  <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>

  <!-- bootstrap progress js -->
  <script src="<?php echo base_url();?>assets/js/progressbar/bootstrap-progressbar.min.js"></script>
  <script src="<?php echo base_url();?>assets/js/nicescroll/jquery.nicescroll.min.js"></script>
  <!-- icheck -->
  <script src="<?php echo base_url();?>assets/js/icheck/icheck.min.js"></script>

  <script src="<?php echo base_url();?>assets/js/custom.js"></script>

  <!-- pace -->
  <script src="<?php echo base_url();?>assets/js/pace/pace.min.js"></script>
</body>

</html>
