<?php


class user extends CI_Model {

    var $details;

    function validate_user( $email, $password ) {
        // Build a query to retrieve the user's details
        // based on the received username and password
		$this->db1->trans_start();
        $this->db1->from('usersofhotels');
        $this->db1->where('username',$email );
        $this->db1->where( 'password', $password );
        $login = $this->db1->get()->result();
		$this->db1->trans_complete();

        // The results of the query are stored in $login.
        // If a value exists, then the user account exists and is validated
        if ( is_array($login) && count($login) == 1 ) {
            // Set the users details into the $details property of this class
            $this->details = $login[0];
            // Call set_session to set the user's session vars via CodeIgniter
            $this->set_session();
            return true;
        }

        return false;
    }
    
    function checkemailexists($email){
    	
    	$this->db1->trans_start();
    	$this->db1->from('usersofhotels');
    	$this->db1->where('username',$email );
    	
    	$login = $this->db1->get()->result();
    	$this->db1->trans_complete();
    	
    	if ( is_array($login) && count($login) == 1 ) {
    		
    		return true;
    	} else {
    	
    	return false;
    	
    	}
    	
    	
    } //end function

    function set_session() {
        // session->set_userdata is a CodeIgniter function that
        // stores data in CodeIgniter's session storage.  Some of the values are built in
        // to CodeIgniter, others are added.  See CodeIgniter's documentation for details.
        $this->session->set_userdata( array(
                'id'=>$this->details->usersofhotelsid,
                'name'=> $this->details->firstname . ' ' . $this->details->lastname,
                'email'=>$this->details->username,
                'role'=>$this->details->role,
				'db'=>$this->details->dbname,
				'groupid'=>$this->details->clientgroups_clientgroupsid,
                'isLoggedIn'=>true
            )
        );
    }

    function createNewUserInParentDB($email, $pw, $firstname, $lastname, $role, $dbname, $groupid, $isclientuser)
  {
	 $data['username'] = $email;
    $data['password'] = $pw;
    $data['firstname'] = $firstname; 
	$data['lastname'] = $lastname;
	$data['role'] = $role;
	$data['dbname'] = $dbname;
	$data['clientgroups_clientgroupsid'] = $groupid;
	$data['isclientuser'] = $isclientuser;
	
	$createdBy = $this->session->userdata('id');
	
	$data['createdby'] = $createdBy;
	
	
	$this->db1->trans_start();
	 $this->db1->insert('usersofhotels',$data);
	 $insert_id = $this->db1->insert_id();
	 $this->db1->trans_complete();
	return $insert_id;
	  
  } //end function
  
  function createNewUserInClientDB($userid, $email, $pw, $firstname, $lastname, $role, $dbname, $groupid)
  {
	 $data['usersofhotelid'] = $userid;
	 $data['username'] = $email;
    $data['password'] = $pw;
    $data['firstname'] = $firstname; 
	$data['lastname'] = $lastname;
	$data['role'] = $role;
	$data['dbname'] = $dbname;
	$data['clientgroups_clientgroupsid'] = $groupid;
	
	$createdBy = $this->session->userdata('id');
	
	$data['createdby'] = $createdBy;
	$data['clientgroups_clientgroupsid'] = $groupid;
	
	
	$this->$dbname->trans_start();
	 $this->$dbname->insert('usersofhotel',$data);
	 $insert_id = $this->$dbname->insert_id();
	 $this->$dbname->trans_complete();
	return true;
	  
  } //end function

    
  function getAllGroupUsers(){
	
	$this->db1->trans_start();
	$this->db1->select('usersofhotelsid, username, password, firstname, lastname, userrolesid, roleName, clientgroupsid, groupname, usersofhotels.dbname, isclientuser');
	$this->db1->from('usersofhotels');
	$this->db1->join('clientgroups', 'clientgroups.clientgroupsid = usersofhotels.clientgroups_clientgroupsid','left');
	$this->db1->join('userroles', 'userroles.userrolesid = usersofhotels.role','left');
		
	
	$result = $this->db1->get()->result_array();
	$this->db1->trans_complete();				  	
		return $result;
  }   //end function
  
  function getClientUsers($db){
	
	$this->$db->trans_start();
	$this->$db->select('usersofhotelsid, username, password, firstname, lastname, userrolesid, roleName, usersofhotels.dbname');
	$this->$db->from('usersofhotels');
	
	$this->$db->join('userroles', 'userroles.userrolesid = usersofhotels.role','left');
	
	
	$result = $this->$db->get()->result_array();
	$this->$db->trans_complete();				  	
		return $result;
  }   //end function
  
  function updateUser($userid, $firstname, $lastname, $email, $pw, $role, $db, $isClientUser)
  {
	 //this function shall insert the key of user lab info data (Table) 
	 //which shall be joined to get user all data
	 
	  $data['username'] = $email;
    $data['password'] = $pw;
    $data['firstname'] = $firstname; 
	$data['lastname'] = $lastname;
	$data['role'] = $role;
	$data['lastmodifiedby'] = $this->session->userdata('id');
	$data['lastmodifiedon'] = date("Y-m-d H:i:s" ,time());
	
		
	$this->db1->trans_start();
	$this->db1->where('usersofhotelsid', $userid);
	$this->db1->update('usersofhotels', $data);
	$this->db1->trans_complete();
	if($isClientUser == 1)
	{
		$this->$db->trans_start();
		$this->$db->where('usersofhotelid', $userid);
		$this->$db->update('usersofhotel', $data);
		$this->$db->trans_complete();
		return true;
	} else {
		$result = "super";
	  return $result;
	}
	
  } //end function
  
  function setnewpasswordfrprnt($email, $password){
  	
  	$data['password'] = $password;
  	$this->db1->where('username', $email);
  	$result  = $this->db1->update('usersofhotels', $data);
  	  	
  	return $result;
  	
  } //end function
  
  function setnewpasswordfrchld($email, $password, $db){
  	 
  	$data['password'] = $password;
  	 
  	$this->db1->where('username', $email);
  	$result  = $this->db1->update('usersofhotels', $data);
  	
  	$this->$db->where('username', $email);
  	$result  = $this->$db->update('usersofhotel', $data);
  	
  	 
  	return $result;
  	 
  } //end function
  
  function updateUserLabInfo($userlabinfoId, $userId, $db)
  {
	 //this function shall insert the key of user lab info data (Table) 
	 //which shall be joined to get user all data
	$this->$db->trans_start();
	$data['userslabsinfo_userslabsinfoid'] = $userlabinfoId;
	$this->$db->where('userId', $userId);
	$this->$db->update('usersoflabs', $data);
	$this->$db->trans_complete();
	return true;
	  
  } //end function
  
  function updateUserDemoDet($demodetid, $userId, $db)
  {
	 //this function shall insert the key of user demographic details (Table) 
	 //which shall be joined to get user all data
	 
	$data['demographicdetails_demographicdetailsid'] = $demodetid;
	$this->$db->trans_start();
	$this->$db->where('userId', $userId);
	$this->$db->update('usersoflabs', $data);
	$this->$db->trans_complete();
	return true;
	  
  } //end function
  
  function updateUserEmergCont($emergId, $userId, $db)
  {
	 //this function shall insert the key of user emergency contact details (Table) 
	 //which shall be joined to get user all data
	 
	$data['emergencycontact_emergencycontactid'] = $emergId;
	$this->$db->trans_start();
	$this->$db->where('userId', $userId);
	$this->$db->update('usersoflabs', $data);
	$this->$db->trans_complete();
	return true;
	  
  } //end function

  
  function getUsersByRole($roleId){
	$this->db->trans_start();
	$this->db->from('user');
    $this->db->where('role',$roleId );
		
	$users = $this->db->get()->result_array();
	$this->db->trans_complete();
    return $users; 
  } 
  //end function
   
   function getUsersByUserIdRole($schoolUsers, $userRole)
   {
	  
	   $users = array();
	   $teachers = array();
	   //$this->$db->trans_start();
	   $this->db->from('user');
	   $users = $this->db->get()->result_array();
	   //$this->$db->trans_complete();
	   
	    foreach($schoolUsers as $schoolUser)
	   {
		   foreach($userRole as $role)
		   {
			   foreach($users as $user){
			if($user['id'] == $schoolUser['user_id'] && $user['role'] == $role['id'])
			{
				$teachers[] = $user;
			}
		   } //end loop
			
		   } //end inner loop
			
		   
	   } //end loop
	   
	   
	   
	   return $teachers;    
	   
   } //end function

    function getUserById( $userId ) {
    // start building a query
	$this->$db->trans_start();
    $this->db->from('usersoflabs');
     $this->db->where( array('userId'=>$userId));
	$user = $this->db->get()->result_array();
	$this->$db->trans_complete();
	return $user;
	
	
  } //end functioin
    
	/*function getUsersLabInfo($userId){
	$this->db->from('usersoflabs');
    $this->db->where('userId',$userId );
		
	$users = $this->db->get()->result_array();
	
	foreach($users as $user)
	{
		if($user['userslabsinfo_userslabsinfoid'])
		{
			
			 $this->load->model('userslabinfo');
			$userInfo = $this->userslabinfo->getUsersLabInfo($user['userslabsinfo_userslabsinfoid']);
			return $userInfo;
		} else 
		{
			return false;
		}
		
	} //end for each
  }   //end function*/
	
	function getUsersLabInfo($userId, $db){
	
	$this->$db->trans_start();
	$this->$db->from('usersoflabs');
	$this->$db->join('userslabinfo', 'userslabinfo.userslabinfoid = usersoflabs.userslabsinfo_userslabsinfoid','left');
	$this->$db->join('demographicdetails', 'demographicdetails.demographicdetailsid = usersoflabs.demographicdetails_demographicdetailsid','left');
	$this->$db->join('emergencycontact', 'emergencycontact.emergencycontactid = usersoflabs.emergencycontact_emergencycontactid','left');
	
	$this->$db->where('usersoflabs.userId', $userId); 
	$result = $this->$db->get()->result_array();
	$this->$db->trans_complete();				  	
		return $result;
  }   //end function
  
  function getUsersFromAnyDB($userId, $db){
	$this->$db->from('usersoflabs');
    $this->$db->where('userId',$userId );
		
	$users = $this->$db->get()->result_array();
    return $users; 
  } 
	
} //end class
