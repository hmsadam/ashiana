

<?php include("header.php")?>
<?php include("menu.php")?>

<script src="<?php echo base_url();?>assets/js/jquery-ui.js"></script>
  

<script src="<?php echo base_url();?>assets/js/hmsappscripts/createusersbyclientscript.js" type="text/javascript"></script> 
<script src="<?php echo base_url();?>assets/js/hmsappscripts/mainscript.js" type="text/javascript"></script>
<body class="nav-md">

  

      
      <!-- page content -->
      <div class="right_col" role="main">
        <div class="">
			<div class="page-title">
            <div class="title_left">
              <h2>Users</h2>
            </div>
            
          </div>
          
         <div class="clearfix"></div>
          <!-- start of data table -->
          <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h3>Manage Users</h3>
                  
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <table id="example" class="table table-striped responsive-utilities jambo_table">
                    <thead>
                      <tr class="headings">
                        
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>User Name</th>
                        <th>User Role</th>
                        
                        
                      </tr>
                    </thead>
				
						
						

					
                    					
					

                  </table>
                </div>
              </div>
            </div>

            <br />
            <br />
            <br />

          </div>
		  
		  <!-- end of data table -->
		  
		  <div class="clearfix"></div>
		  <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h3>Create New User</h3>
                </div>

                <div class="x_content">
                  <br />
                  <form id="userCreationForm" data-parsley-validate class="form-horizontal form-label-left">

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="fName">First Name <span class="required">*</span>
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" id="fName" required class="form-control col-md-7 col-xs-12">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="lName">Last Name <span class="required">*</span>
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" id="lName" name="lName" required class="form-control col-md-7 col-xs-12">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Email / User Name <span class="required">*</span>
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="email" id="email" name="email" required class="form-control col-md-7 col-xs-12">
                      </div>
                    </div>
					<div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="pWord">Password <span class="required">*</span>
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="password" id="pWord" name="pWord" required class="form-control col-md-7 col-xs-12">
                      </div>
                    </div>
					
					
					<input type="hidden" id="role" value="2">
					<input type="hidden" id="clientGroup" value="<?php echo $groupid; ?>">
						<input type="hidden" id="clientDb" value="<?php echo $db; ?>">					
						
								
					
										
					
					
                    <div class="ln_solid"></div>
                    <div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        
                        <input type="reset" value="Cancel" class="btn btn-primary"> 
                        <a href="javascript:void(0);" id="save" class="btn btn-success"> Submit</a>
                      </div>
                    </div>

                  </form>
                </div>
              </div>
            </div>
          </div>
		  
		  <!-- Large modal -->
                
                <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
                  <div class="modal-dialog modal-lg">
                    <div class="modal-content col-xs-12">

                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                        </button>
                        <h4 class="modal-title" id="myModalLabel">Update User</h4>
                      </div>
                      <div class="modal-body">
                         <form id="userCreationForm1" data-parsley-validate class="form-horizontal form-label-left">
						<input type="hidden" id="userId" value="W3Schools">
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="fName1">First Name <span class="required">*</span>
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" id="fName1" required class="form-control col-md-7 col-xs-12">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="lName1">Last Name <span class="required">*</span>
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" id="lName1" name="lName1" required class="form-control col-md-7 col-xs-12">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email1">Email / User Name <span class="required">*</span>
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="email" id="email1" name="email1" required class="form-control col-md-7 col-xs-12">
                      </div>
                    </div>
					<div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="pWord1">Password <span class="required">*</span>
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" id="pWord1" name="pWord1" required class="form-control col-md-7 col-xs-12">
                      </div>
                    </div>
					
					<div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12">User Role </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <select class="form-control" id="role1" >
                          <option>- Select Role - </option>
                          <option>Hotel Administrator</option>
								
                        </select>
                      </div>
                    </div>
					
					<input type="hidden" id="clientGroup1" value="<?php echo $groupid; ?>">
						<input type="hidden" id="clientDb1" value="<?php echo $db; ?>">					
				
                  </form>
                
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" onClick="updateUserInfo()"class="btn btn-primary" data-dismiss="modal">Save changes</button>
                      </div>

                    </div>
                  </div>
                </div>
		   <!--end modal -->
		  
		  

          <script type="text/javascript">
            $(document).ready(function() {
              
            });
          </script>


    </div>
  </div>

  <div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
  </div>

  <script src="<?php echo base_url();?>/assets/js/bootstrap.min.js"></script>

  <!-- bootstrap progress js -->
  <script src="<?php echo base_url();?>assets/js/progressbar/bootstrap-progressbar.min.js"></script>
  <script src="<?php echo base_url();?>assets/js/nicescroll/jquery.nicescroll.min.js"></script>
  <!-- icheck -->
  <script src="<?php echo base_url();?>assets/js/icheck/icheck.min.js"></script>
  <!-- tags -->
  <script src="<?php echo base_url();?>assets/js/tags/jquery.tagsinput.min.js"></script>
  <!-- switchery -->
  <script src="<?php echo base_url();?>assets/js/switchery/switchery.min.js"></script>
  <!-- daterangepicker -->
  <script type="text/javascript" src="<?php echo base_url();?>assets/js/moment/moment.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>assets/js/datepicker/daterangepicker.js"></script>
  <!-- richtext editor -->
  <script src="<?php echo base_url();?>assets/js/editor/bootstrap-wysiwyg.js"></script>
  <script src="<?php echo base_url();?>assets/js/editor/external/jquery.hotkeys.js"></script>
  <script src="<?php echo base_url();?>assets/js/editor/external/google-code-prettify/prettify.js"></script>
  <!-- select2 -->
  <script src="<?php echo base_url();?>assets/js/select/select2.full.js"></script>
  <!-- form validation -->
  <script type="text/javascript" src="<?php echo base_url();?>assets/js/parsley/parsley.min.js"></script>
  <!-- textarea resize -->
  <script src="<?php echo base_url();?>assets/js/textarea/autosize.min.js"></script>
  <script>
    autosize($('.resizable_textarea'));
  </script>
  <!-- Datatables -->
  <script src="<?php echo base_url();?>assets/js/datatables/js/jquery.dataTables.js"></script>
  <script src="<?php echo base_url();?>assets/js/datatables/tools/js/dataTables.tableTools.js"></script>

  
  <!-- pace -->
  <script src="<?php echo base_url();?>assets/js/pace/pace.min.js"></script>
  
  <script src="<?php echo base_url();?>assets/js/custom.js"></script>
  
  

 
  
 
</body>

</html>
