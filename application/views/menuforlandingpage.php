<?php ?>
  <div class="container body">


    <div class="main_container">

      <div class="col-md-3 left_col">
        <div class="left_col scroll-view">

          <div class="navbar nav_title" style="border: 0;">
            <a href="<?php echo page_url;?>home" class="site_title"><i class="fa fa-paw"></i> <span>HMS!</span></a>
          </div>
          <div class="clearfix"></div>

          <!-- menu prile quick info -->
          <div class="profile">
            <div class="profile_pic">
              <img src="<?php echo base_url();?>/assets/images/img.jpg" alt="..." class="img-circle profile_img">
            </div>
            <div class="profile_info">
              <span>Welcome,</span>
              <h2><?php //echo $username; ?></h2>
            </div>
          </div>
          <!-- /menu prile quick info -->

          <br />

          <!-- sidebar menu -->
          <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

            <div class="menu_section">
              <h3>Dalesman Inn</h3>
              <ul class="nav side-menu">
                
                <li><a><i class="fa fa-edit"></i> Reservations <span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu" style="display: none">
                    <li><a href="<?php echo page_url;?>roomavailability">Check Room Availibility</a>
                    </li>
                     <li><a href="<?php echo page_url;?>enquiry">Enquiry</a>
                    </li>
                    <li><a href="<?php echo page_url;?>dashboard">Login</a>
                    </li>
                    
                  </ul>
                </li>
                
               
              </ul>
            </div>
            
          </div>
          <!-- /sidebar menu -->

          <!-- /menu footer buttons -->
          
          <!-- /menu footer buttons -->
        </div>
      </div>

      <!-- top navigation -->
      <div class="top_nav">

        <div class="nav_menu">
          <nav class="" role="navigation">
            <div class="nav toggle">
              <a id="menu_toggle"><i class="fa fa-bars"></i></a>
            </div>

            <ul class="nav navbar-nav navbar-right">
              

            </ul>
          </nav>
        </div>

      </div>
      <!-- /top navigation -->
	  
	  

