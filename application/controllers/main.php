<?php

class main extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
	

    if( !$this->session->userdata('isLoggedIn') ) {
        redirect('/login/show_login');
    }
  } //end function

  
   
   function updateHotelId()
  {
	  $hotelId = $this->input->post('hotelId');
	  
	  //$this->session->set_userdata(array('labId'=>$labId, 'yId'=>$yearId));
	  $this->session->set_userdata(array('hotelId'=>$hotelId));
	  
  } //end function
  
  
  function show_main() {
    $this->load->model('hotelbranches');
	$branches = $this->hotelbranches->getHotelsByUser();
	$data['branches'] = $branches;
	
	$username = $this->session->userdata('name');
	   $data['username'] = $username;
	$db = $this->session->userdata('db');
	
	
    $data['email'] = $this->session->userdata('email');
    $data['name'] = $this->session->userdata('name');
    
	$this->load->helper('form');
    $this->load->view('hoteldashboard',$data);
  }

    
  
} //end class
