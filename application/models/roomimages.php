<?php

class roomimages extends CI_Model {

  
	//this function is just for reference and not being used anywhere in the application
  
  function createimage($roomid, $imagename, $db, $userid)
  {
	
    $data['imagefilename'] = $imagename;
    $data['hotelrooms_hotelroomsid'] = $roomid;
    
   $data['createdon'] = date("Y-m-d H:i:s" ,time());
    
    $data['createdby'] = $userid; 
    
	
	$this->$db->trans_start();	
	$this->$db->insert('roomimages',$data);
	$insert_id = $this->$db->insert_id();
	$this->$db->trans_complete();
	
	if ($this->$db->trans_status() === FALSE) {
    # Something went wrong.
    $this->$db->trans_rollback();
    return false;
	
	} 
	else {
		# Everything is Perfect. 
		# Committing data to the database.
		$this->$db->trans_commit();
		return true;
	}
  } //end function
  
  
  function getallimagenames($db)
  {
  	$this->$db->trans_start();
  	
  	$this->$db->select('imagefilename');
  	$this->$db->from('roomimages');
  	
  	$roomimgs = $this->$db->get()->result_array();
  	$this->$db->trans_complete();
  	return $roomimgs;
  	 
  } //end function
  
  
  function getimagesbyroomid($roomid, $db)
  {
	$this->$db->trans_start();
	$this->$db->select('roomimagesid, imagefilename, hotelrooms_hotelroomsid');
	$this->$db->from('roomimages');
	$this->$db->where('hotelrooms_hotelroomsid', $roomid);
    $roomimgs = $this->$db->get()->result_array();
	$this->$db->trans_complete();
	  return $roomimgs;
	  
  } //end function
  
  function deleteroomimage($id, $db)
  {
  	$this->$db->trans_start();
  	$this->$db->where('roomimagesid', $id);
  	$this->$db->delete('roomimages');
  	$this->$db->trans_complete();
  	return true;
  		
  } //end function
  
  
  
 
  
} //end model class
