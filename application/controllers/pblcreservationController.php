<?php

class pblcreservationController extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
	

    /*if( !$this->session->userdata('isLoggedIn') ) {
        redirect('/login/show_login');
    }*/
  }

  function show_roomavailabilityfrontg( ) {
  
  	$data['user'] = null;
  	$this->load->helper('form');
  	$this->load->view('checkavailability',$data);
  } //end function

  
  
  
  
  function getRoomsbyCatgId()
  {
	$catgid = $this->input->post('catgid');
	//$catgid = 2;
	  
	  $db = $this->session->userdata('db');
	
	$this->load->model('hotelrooms');
    $result = $this->hotelrooms->getRoomsByCatg($catgid, $db);
	echo json_encode($result);  
	  
  } //end function
  
  function getReservedDates()
  {
	  $db = $this->session->userdata('db');
	
	$this->load->model('hotelreservation');
    $result = $this->hotelreservation->getReservationBTWDates($startDate, $endDate, $db);
	echo json_encode($result);  
  } //end function
  
  function timeExpriments()
  {
	  $this->load->helper('date');
	  //$time = time();
	  //$time2 = mktime(23, 40, 00, 5, 14, 2016);
	  $reservationendtime = strtotime("2019-06-21 00:20:00");
	  //$time2 = strtotime("Yesterday");
	 // $dif= $time - $time2;
	  //$dif= timespan($time, $time);
	  //$mytime = $time." - ". $time2." =".$dif;
		//$result = strftime("Dif %m-%d-%y %H:%M:%S", $dif);
		//$result = strtotime($dif);
	  //$result =  human_to_unix($dif);
	  //$result =  unix_to_human($dif);
	  
	  $time2 = date('Y-m-d H:i:s');
	  $currenttime = strtotime($time2);
	  if($currenttime < $reservationendtime){
	  	echo "Active Reservation";
	  } else {
	  	echo "Completed Reservation";
	  }
	  
	  echo json_encode($currenttime);
	  
	  
	  
  } //end function

  //this function only selects rooms with date without time and so replaced with getRoomsBYDateTime
   function getAvailableRooms()
   {
	   $branchid = $this->input->post('branchid');
	   $startdate = $this->input->post('mystartdate');
	   $enddate = $this->input->post('myenddate');
		$branchid = 1;
		$status = "active";
		$startdate = '2016-05-16';
		$enddate = '2016-05-27';
		$db = $this->session->userdata('db');
				
		$this->load->model('hotelreservation');
		$braReservations = $this->hotelreservation->joinReserv($branchid, $startdate, $enddate,  $db);
		
		
		$this->load->model('hotelrooms');
		$allRooms = $this->hotelrooms->getRoomsByBranchForReservation($branchid, $db);
		
		
		$this->load->model('reservedrooms');
		$rsrvdRooms = $this->reservedrooms->getActiveReservedRooms($branchid, $db);
		$activeRsrvs;
		$output;
		
		foreach($rsrvdRooms as $key => $rroom)
		{
			foreach($braReservations as $key1 => $brarsv)
			{
				if($brarsv['reservationid'] == $rroom['reservation_reservationid'] AND $brarsv['status'] == 'active')
				{
					$activeRsrvs[] = $rroom;
									
				}
				
			} //end inner loop
			
		}//end outer loop activeRsrvs actvRsrv allRooms room 
		
		if(isset($activeRsrvs))
		{
			
			$tmp = array();
			foreach($allRooms as $room) {
				$tmp[$room['hotelroomsid']] = $room;
			} //end foreach
			foreach($activeRsrvs as $actvRsrv)
			{
				foreach($allRooms as $room)
				{
					if($actvRsrv['hotelrooms_hotelroomsid'] === $room['hotelroomsid'])
					{
						if(array_key_exists($room['hotelroomsid'], $tmp)) {
							 unset($tmp[$room['hotelroomsid']]); //exclude those rooms which are reserved on required date
						}
					} //end if
				} //end inner loop
			}//end outer loop
			$output = array_values($tmp);
		} else {
			
			$output = $allRooms;
		}
		
	   echo json_encode($output);
	   
   } //end function

   function getRoomsBYDateTime()
   {
		$branchid = $this->input->post('branchid');
	   $startdate = $this->input->post('mystartdate');
	   $enddate = $this->input->post('myenddate');
	   $adults = $this->input->post('adults');
	   $child = $this->input->post('child');
	   $infant = $this->input->post('infant');
	   $branchid = 1;
	   /*$adults = 1;
	   $branchid = 1;
		$status = "active";
		$startdate = '2019-09-24 11:00:00';
		$enddate = '2019-09-25 10:59:59';*/
		//$db = $this->session->userdata('db');
	   $db = "hmsfrazdb";
		//get all rooms of a branch
		$this->load->model('hotelrooms');
		$allRooms = $this->hotelrooms->getRoomsByBranchForReservation($branchid, $db);
		
		//echo json_encode($allRooms);
		//get all rooms which are reserved either active or not
		$this->load->model('reservedrooms');
		$rsrvdRooms = $this->reservedrooms->getRsvdRoom($branchid, $startdate, $enddate, $db);
		$activeRsrvs;
		$tempoutput;
		$output = array();
		//Exclude those rooms which are not actively reserved but included due to matching startdate and enddate time
		foreach($rsrvdRooms as $key => $value)
			{
				if($value['status'] == 'active' || $value['status'] == 'confirmed')
				{
					//print_r($value);
					$activeRsrvs[] = $value;
									
				}
				
			} //end inner loop
		
		//select and return only those rooms which are either empty or currently reserved but can be reserved for other dates
		
		if(isset($activeRsrvs))
		{
			
			$tmp = array();
			foreach($allRooms as $room) {
				$tmp[$room['hotelroomsid']] = $room;
			} //end foreach
			foreach($activeRsrvs as $actvRsrv)
			{
				foreach($allRooms as $room)
				{
					if($actvRsrv['hotelrooms_hotelroomsid'] === $room['hotelroomsid'])
					{
						if(array_key_exists($room['hotelroomsid'], $tmp)) {
							 unset($tmp[$room['hotelroomsid']]); //exclude those rooms which are reserved on required date
						}
					} //end if
				} //end inner loop
			}//end outer loop
			$tempoutput = array_values($tmp);
		} else {
			
			$tempoutput = $allRooms;
		}
		
		if($adults > 1){
			foreach ($tempoutput as $room){
				if($room['categoryname'] == "Single Rooms"){
					//do nothing
				} else {
					$output[] = $room;
				}
			}
		} else {
			foreach ($tempoutput as $room){
				if($room['categoryname'] == "Single Rooms"){
					$output[] = $room;
				} else {
					//do nothing
					
				}
			}
			
		} //end if
		
		
		$allrates;
		
		$this->load->model('roomrates');
		$allrates = $this->roomrates->getrngbsdromrate($startdate, $enddate, $db);
		
		$finalrates = array();
		$allimages = array();
		
		for($i = 0; $i < sizeof($output); $i++){
			for($j = 0; $j < sizeof($allrates); $j++){
				if($output[$i]['hotelroomsid'] == $allrates[$j]['hotelrooms_hotelroomsid']){
					$finalrates[] = $allrates[$j];
				} //end if
			} // end inner 	
		}
		
		for($m = 0; $m < sizeof($output); $m++){
			$output[$m]['cost'] = $this::calcstaycost($output[$m]['hotelroomsid'], $finalrates, $startdate, $enddate);
		}
		
		foreach($output as $room){
			$this->load->model('roomimages');
  			$allimages[] = $allroomimages = $this->roomimages->getimagesbyroomid($room['hotelroomsid'], $db);
		} //end foreach
		
		$finalrooms = array('data' => $output);
		
		echo json_encode(array('rooms' => $finalrooms, 'rates' => $finalrates, 'images' => $allimages));
		/*if(sizeof($finalrooms['data']) > 1) {
			
		}else {
			echo json_encode('rooms not available');
		}*/
		
		
	//$rmcst = $this::calcstaycost($finalrates);
		//echo json_encode($finalrooms);
		
   } //end function
   
   function calcstaycost($roomid, $finalrates, $strDateFrom, $strDateTo){
   	
   	$total = 0;
   	$allrsvddates = $this::createDateRangeArray($strDateFrom, $strDateTo);
   	
   	for($i = 0 ; $i < sizeof($allrsvddates); $i++ ){
   		foreach($finalrates as $rate){
   			
   			if($allrsvddates[$i] >= $rate['ratefrom'] && $allrsvddates[$i] <= $rate['rateto']){
   				if($rate['hotelrooms_hotelroomsid'] == $roomid )
   				$total += intval($rate['roomrate']); 
   			}
   			
   		} //end foreach
   	} //end for 
   	$result = $total/2;
   	return $result;
   	//echo json_encode($result);
   } //end function
   
   

   function dateTimeDifference($stDate, $enDate)
   {
   	//$stDate = $this->input->post('startdate');
   	//$enDate = $this->input->post('enddate');
   	//$stDate = "1962-03-07 00:02:35";
   	//$stDate = "2019-07-15 11:00:00";
   	//$enDate = "2019-08-14 10:59:59";
   	//$enDate = "2019-08-03 10:59:59"; //2017-03-22 00:00:00
   
   	$add_time = strtotime($enDate)+1;
   	$add_date = date('y-m-d h:i:s',$add_time);
   
   	$first_date = new DateTime($stDate);
   	$second_date = new DateTime($add_date);
   
   	$difference = $first_date->diff($second_date);
   	 
   	$result = $this::format_interval($difference);
   	//echo json_encode($result);
   	return $result;
   
   } //end function
    
   function format_interval(DateInterval $interval) {
   	$result = "";
   	if ($interval->y) { $result .= $interval->format("%y years "); }
   	if ($interval->m) { $result .= $interval->format("%m months "); }
   	if ($interval->d) { $result .= $interval->format("%d Night(s)"); }
   	if ($interval->h) { $result .= $interval->format("%h hours "); }
   	if ($interval->i) { $result .= $interval->format("%i minutes "); }
   	if ($interval->s) { $result .= $interval->format("%s seconds "); }
   	 
   	return $result;
   } //end result
   
   function getduration($stDate, $enDate)
   {
   	//$stDate = $this->input->post('startdate');
   	//$enDate = $this->input->post('enddate');
   	//$stDate = "1962-03-07 00:02:35";
   	//$stDate = "2019-07-15 11:00:00";
   	//$enDate = "2019-08-14 10:59:59";
   	//$enDate = "2019-08-03 10:59:59"; //2017-03-22 00:00:00
   	 
   	$add_time = strtotime($enDate)+1;
   	$add_date = date('y-m-d h:i:s',$add_time);
   	 
   	$first_date = new DateTime($stDate);
   	$second_date = new DateTime($add_date);
   	 
   	$difference = $first_date->diff($second_date);
   	 
   	$result = $this::format_durinterval($difference);
   	//echo json_encode($result);
   	return $result;
   	 
   } //end function
   
   function format_durinterval(DateInterval $interval) {
   	$result = "";
   	if ($interval->y) { $result .= $interval->format("%y years "); }
   	if ($interval->m) { $result .= $interval->format("%m months "); }
   	if ($interval->d) { $result .= $interval->format("%d"); }
   	if ($interval->h) { $result .= $interval->format("%h hours "); }
   	if ($interval->i) { $result .= $interval->format("%i minutes "); }
   	if ($interval->s) { $result .= $interval->format("%s seconds "); }
   	 
   	return $result;
   } //end result
    
   function createDateRangeArray($strDateFrom, $strDateTo)
   {
   	/*$strDateFrom = "2019-07-30 11:00:00";
   	$strDateTo = "2019-07-31 10:59:59";*/
         
   	$aryRange=array();
   	 
   	$iDateFrom=mktime(1,0,0,substr($strDateFrom,5,2),     substr($strDateFrom,8,2),substr($strDateFrom,0,4));
   	$iDateTo=mktime(1,0,0,substr($strDateTo,5,2),     substr($strDateTo,8,2),substr($strDateTo,0,4));
   	 
   	if ($iDateTo>=$iDateFrom)
   	{
   		array_push($aryRange, date('Y-m-d',$iDateFrom)); // first entry
   		while ($iDateFrom<$iDateTo)
   		{
   			$iDateFrom+=86400; // add 24 hours
   			array_push($aryRange,date('Y-m-d',$iDateFrom));
   			array_push($aryRange,date('Y-m-d',$iDateFrom));
   		}
   	}
   
   
   	array_pop($aryRange);
   	$resultdates = array();
   	for($i = 0; $i < sizeof($aryRange); $i++)
   	{
   		if($i == 0) {
   			$resultdates[$i] = $aryRange[$i]." 11:00:00";
   
   		} elseif ($i%2==0){
   			$resultdates[$i] = $aryRange[$i]." 11:00:00";
   		} else {
   			$resultdates[$i] = $aryRange[$i]." 10:59:59";
   		}
   	} //end for*/
   
   	return $resultdates;
   	//echo json_encode($resultdates);
   } //end function
     
     
   
   function getrangebasedrate(){
   	
   	
   	
   	//$roomid = 2;
   	$datefrom = "2019-07-23 00:00:00";
   	$dateto = "2019-07-23 23:59:59";
   	$db = $this->session->userdata('db');
   	$this->load->model('roomrates');
   	$roomrates = $this->roomrates->getrngbsdromrate($datefrom, $dateto, $db);
   	$ratesize = sizeof($roomrates);
   	$idbasedromrates = array();
   	
   	for($i = 0; $i < $ratesize; $i++)
   	{
   		if($roomid == $roomrates[$i]['hotelrooms_hotelroomsid']){
   			$idbasedromrates[]  = $roomrates[$i];
   		} //end if
   		
   	}  //end foreach
   	
   	echo json_encode($roomrates);
   } //end function
   
   function createDateRangeArrayHMSOWNOLD()
	{
		// takes two dates formatted as YYYY-MM-DD and creates an
		// inclusive array of the dates between the from and to dates.

		// could test validity of dates here but I'm already doing
		// that in the main script
		
		$strDateFrom ="2016-05-01 00:00:00";
		$strDateTo = "2016-05-01 00:01:00";

		$aryRange=array();

		$iDateFrom=mktime(1,0,0,substr($strDateFrom,5,2),     substr($strDateFrom,8,2),substr($strDateFrom,0,4));
		$iDateTo=mktime(1,0,0,substr($strDateTo,5,2),     substr($strDateTo,8,2),substr($strDateTo,0,4));

		if ($iDateTo>=$iDateFrom)
		{
			array_push($aryRange,date('Y-m-d ',$iDateFrom)); // first entry
			while ($iDateFrom<$iDateTo)
			{
				$iDateFrom+=86400; // add 24 hours
				array_push($aryRange,date('Y-m-d ',$iDateFrom));
			}
		}
		//return $aryRange;
		print_r($aryRange);
		//echo json_encode($aryRange);
	} //end function
   
   function getRsvdRomsForDataTable()
   {
	   $reservationId = $this->input->post('rsvid');
	   
	   //$reservationId = "KHAN2016Q3-34";
	   $db = $this->session->userdata('db');
	   
	   $this->load->model('reservedrooms');
	   $allRooms = $this->reservedrooms->getRsvdRomByRsvId($reservationId, $db);
		
		echo json_encode(array('data' => $allRooms));
		
   } //end function
   
   function deleteReservedRoom()
   {
   	//this function was used before first release
   	//now it is not used but may be used in future
   	$id = $this->input->post('id');
   	
   	 
   	$db = $this->session->userdata('db');
   
   	$this->load->model('reservedrooms');
   	$result = $this->reservedrooms->deleteReservedRoomFromReservation($id, $db);
   	
   	$status = 'vacant';
   	
   	$this->load->model('hotelrooms');
   	$rsvdroomupdt = $this->hotelrooms->updateRoomOccupationalStatus($roomid, $status, $userid, $db);
   	
   	echo json_encode($result);
   	 
   } //end function
   
   
   
   
   
   
   function getGuestData()
   {
	   $reservationId = $this->input->post('rsvid');
	   
	  //$reservationId = "KHAN2016Q3-34";
	   $db = $this->session->userdata('db');
	   
	   $this->load->model('guestshasstays');
	   $guestData = $this->guestshasstays->getGuestData($reservationId, $db);
		
		//print_r($guestData);
		
		echo json_encode(array('data' => $guestData));
		
   } //end function
   
   function deleteGuest()
   {
   	$guestid = $this->input->post('guestid');
   	$guestsstayid = $this->input->post('guestsstayid');
   	$db = $this->session->userdata('db');
   
   	$this->load->model('guests');
   	$checkParent = $this->guests->checkIsParent($guestid, $db);   	
   	
		   	if(sizeof($checkParent) > 0)
		   	{
		   		echo json_encode(false);
		   	} else {
		   	
		   		$this->load->model('guests');
		   		$gstdl = $this->guests->deleteGuest($guestid, $db);
		   		
		   		$this->load->model('guesthasreservations');
		   		$gsthsrsvdl = $this->guesthasreservations->deleteGuest($guestid, $db);
		   		
		   		$this->load->model('guestshasstays');
		   		$gsthsstydl = $this->guestshasstays->deleteGuest($guestid, $db);
		   		
		   		$this->load->model('guestsstays');
		   		$gststydl = $this->guestsstays->deleteGuest($guestsstayid, $db);
		   		
		   		echo json_encode(true);
		   	}
   } //end function
   
   function updateGuestData()
   {
   			$rsrvid = $this->input->post('rsvid');
		   	$guestId = $this->input->post('guestId');
		   	$guestsstaysid = $this->input->post('guestsstaysid');
		   	$checkin = $this->input->post('checkin');
		   	$checkout = $this->input->post('checkout');
		   	$fname = $this->input->post('fname');
		   	$lname = $this->input->post('lname');
		   	$cnic = $this->input->post('cnic');
		   	$passport = $this->input->post('passport');
		   	$cellno = $this->input->post('cellno');
		   	$landline = $this->input->post('landline');
		   	$email = $this->input->post('email');
		   	$gender = $this->input->post('gender');
		   	$age = $this->input->post('age');
		   	$ethnicity = $this->input->post('ethnicity');
		   	$city = $this->input->post('city');
		   	$district = $this->input->post('district');
		   	$province = $this->input->post('province');
		   	$country = $this->input->post('country');
		   	$roomno = $this->input->post('roomno');
		   	$ttlnorms = $this->input->post('ttlnorms');
		   	$crntnorms = $this->input->post('cstmRsvdRoms');
		   	
		   /*	$rsrvid = "FRZMNBR2019Q3-33";
		   	$guestId = 60;
		   	$guestsstaysid = 60;
		   	$checkin = "0000-00-00 00:00:00";
		   	$checkout = "0000-00-00 00:00:00";
		   	$fname = "Jamshaid";
		   	$lname = "Sabir";
		   	$cnic = "Sabir";;
		   	$passport = "Sabir";
		   	$cellno = "Sabir";;
		   	$landline = "Sabir";;
		   	$email = "Sabir";;
		   	$gender = "Sabir";;
		   	$age = "Sabir";;
		   	$ethnicity = "Sabir";;
		   	$city = "Sabir";;
		   	$district = "Sabir";;
		   	$province = "Sabir";;
		   	$country = "Sabir";
		   	$roomno = 1;
		   	$ttlnorms = "3";
		   	$crntnorms = "3";*/
		   	
		   	//echo json_encode($checkin);
		   //	$rsrvid = "FRZMNBR2019Q2-27";
		   	$db = $this->session->userdata('db');
		   	$userid = $this->session->userdata('id');
		   	
		   	$result1;
		   	$result;
		   	
		   	
		   	$this->load->model('guests');
		   	$result1 = $this->guests->updateGuestData($guestId, $fname, $lname, $cnic, $passport, $cellno, $landline, $email, $gender, $age, $ethnicity, $city, $district, $province, $country, $userid, $db);
		   	
		   	$checkedin = 0;
		   	$checkedout = 0;
		   	$status = "";
		   	if($checkin != "0000-00-00 00:00:00")
		   	{
		   		$checkedin = 1;
		   		$status = "occupied";
		   	} else {
		   		$checkedin = 0;
		   		$status = "reserved";
		   	}
		   	
		   	if($checkout != "0000-00-00 00:00:00")
		   	{
		   		$checkedout = 1;
		   		$status = "vacant";
		   	} else {
		   		$checkedout = 0;
		   		
		   		if($checkin == "0000-00-00 00:00:00"){
		   			
		   			$status = "reserved";
		   			
		   		} else {
		   			
		   			$status = "occupied";
		   		}
		   		
		   	}
		   	
		   	//these queries needs to be executed under condition
		   	$this->load->model('guestsstays');
		   	$result = $this->guestsstays->updateGuestRoomNo($guestsstaysid, $checkin, $checkout, $checkedin, $checkedout, $roomno, $userid, $db);
		   	
			   	$this->load->model('reservedrooms');
			   	$rsvdroomupdt = $this->reservedrooms->updatrsvdromocptionstatus($rsrvid, $roomno, $status, $db);
		 	   	
			   	$roomvacationresult;
			   	if($status == "vacant" ){
			   		
			   		$this->load->model('hotelreservation');
			   		$rsvdnoofromsarr1 = $this->hotelreservation->getnoofrooms($rsrvid, $db);
			   		$rsvnosofroms1 = $rsvdnoofromsarr1[0];
			   		 
			   		$currentnoofrooms = (int)$rsvnosofroms1['currentlynoofrooms'];
			   		
			   		
			   		if($currentnoofrooms != 0){
			   			
			   			$currentnoofrooms--;
			   			$this->load->model('hotelreservation');
			   			$roomvacationresult = $this->hotelreservation->updatenoofrooms($rsrvid, $currentnoofrooms, $db);
			   		}
			   	}
			   	
			   	$rsvupdrslt;
			   	if($status == "vacant" ){
				   	$this->load->model('hotelreservation');
				   	$rsvdnoofromsarr2 = $this->hotelreservation->getnoofrooms($rsrvid, $db);
				   	$rsvnosofroms2 = $rsvdnoofromsarr2[0];
				   	
				   	if($rsvnosofroms2['currentlynoofrooms'] == "0"){
				   		
				   		
				   		
				   		$rsvstatus = "completed";
				   		$this->load->model('hotelbrancheshasrsvr');
				   		$rsvupdrslt = $this->hotelbrancheshasrsvr->updateReservationStatus($rsrvid, $rsvstatus, $db);
				   	}
			   	}
			   	
			   	//echo json_encode(true);
			   	
			   	$this->load->model('hotelreservation');
			   	$rsvdnoofromsarr3 = $this->hotelreservation->getnoofrooms($rsrvid, $db);
			   	$reservationstats = $rsvdnoofromsarr3[0];
			   	
			   	$this->load->model('hotelbrancheshasrsvr');
			   	$reservationstatus = $this->hotelbrancheshasrsvr->getreservationstatus($rsrvid, $db);
			   	$reservationstatusfinal = $reservationstatus[0];
			   	
		   	echo json_encode(array('romsstats' => $reservationstats, 'reservationstatus' => $reservationstatusfinal));
   
   } //end function
  
   
   
   function showPrintableRsv() {
   	
   	
   	
   	$rsvid = $this->session->userdata('resvID');
   	//$rsvid = "KHAN2016Q3-26";
   	
   	$db = $this->session->userdata('db');
   	
   $data['rsvdata'] = $this::getResvData($rsvid);
   $data['gstdata'] = $this::getGuestsData($rsvid);
   $data['roomsdata'] = $this::getGuestsReservedRooms($rsvid);
   $data['billdata'] = $this::getGuestsBill($rsvid);
   $data['staydata'] = $this::getGuestsStays($rsvid);
   
   	$this->load->helper('form');
   	$this->load->view('reservationPrintableForm',$data);
   } //end function
   
   function showInvoice() {
   	 
   	 
   	 
   	$rsvid = $this->session->userdata('resvID');
   	//$rsvid = "KHAN2016Q3-36";
   	$db = $this->session->userdata('db');
   	$branchid = $this->session->userdata('hotelId');
   	$this->load->model('hotelbranches');
   	$branchdata = $this->hotelbranches->getBranchData($branchid, $db);
   	 
   	 
   	$data['rsvid'] = $rsvid;
   	$data['branch'] = $branchdata;
   	$data['rsvdata'] = $this::getResvData($rsvid);
   	$data['gstdata'] = $this::getGuestsData($rsvid);
   	$data['roomsdata'] = $this::getGuestsReservedRooms($rsvid);
   	$data['billdata'] = $this::getGuestsBill($rsvid);
   	$data['staydata'] = $this::getGuestsStays($rsvid);
   
   	$this->load->helper('form');
   	$this->load->view('invoice',$data);
   
   } //end function
   
   function getResvData($rsvid) {
   	
   	//$rsvid = "KHAN2016Q3-26";
   	$db = $this->session->userdata('db');
   
   	$this->load->model('hotelreservation');
   	$rsvdata = $this->hotelreservation->getResvData($rsvid, $db);
   	return $rsvdata;
   	//echo json_encode($rsvdata);
   	
   } //end function
   
   function getGuestsData($rsvid)
   {
   	
   	//$rsvid = "KHAN2016Q3-26";
   	$db = $this->session->userdata('db');
   	$this->load->model('guesthasreservations');
   	$gstsrsvns = $this->guesthasreservations->getGuestsDataByRsvId($rsvid, $db);
   	
   	   	
   	return $gstsrsvns;
   	
   	//echo json_encode($gstsrsvns);
   } //end function
   
   function getGuestsReservedRooms($rsvid)
   {
   
   	//$rsvid = "FRZMNBR2019Q3-33";
   	$db = $this->session->userdata('db');
   	$this->load->model('reservedrooms');
   	$rsvdRooms = $this->reservedrooms->getRsvdRomByRsvId($rsvid, $db);
   
   	//print_r($rsvdRooms);
   
   	return $rsvdRooms;
   
   	//echo json_encode($rsvdRooms);
   } //end function
   
   function getGuestsBill($rsvid)
   {
   	 
   	//$rsvid = "KHAN2016Q3-26";
   	$db = $this->session->userdata('db');
   	$this->load->model('guestbill');
   	$bill = $this->guestbill->getGuestsBill($rsvid, $db);
   	 
   
   	 
   	return $bill;
   	 
   	//echo json_encode($bill);
   } //end function
   
   function getGuestsStays($rsvid)
   {
   	
	   	//$rsvid = "KHAN2016Q3-26";
	   	$db = $this->session->userdata('db');
	   	$this->load->model('guestshasstays');
	   	$guestStay = $this->guestshasstays->getGuestStayData($rsvid, $db);
	   	 
	   	 
	   	 
	   	return $guestStay;
   	 
   	//echo json_encode($guestStay);
   	
   	
   } //end function
   
   
   function saveallreservationdata(){
  	$branchid = $this->input->post('branchid');
   	$rooms = $this->input->post('selectedroomsforrsv');
   	$guests = $this->input->post('guestsclctn');
   	$reservation = $this->input->post('rsvobj');
   	//$billing = $this->input->post('billingobj');
   	 
   	
   	//$reservation['startdate'] = "2019-09-10 11:00:00";
   	//$reservation['enddate'] = "2019-09-11 10:59:59";
   	
   	
   	
   	$reservationduration = $this::dateTimeDifference($reservation['startdate'], $reservation['enddate']);
   	$durforfin = $this::getduration($reservation['startdate'], $reservation['enddate']);
   	
   	$result = array(
   			'rooms' => $rooms,
   			'guests' => $guests,
   			//'reservation' => $reservation,
   			//'billing' => $billing
   	);
   	
   	$branchid = 1;
   	
   	$results = array();
   	 
   	$resvId = $this::createRsrvNo($branchid);
   	
   	$results['reservationid'] = $resvId;
   	
   	 //$results['insrtidinssn'] = $this::insertRsvIdInSess($resvId);
   
   	 foreach($rooms as $room){
   	  
   	 	$results['saverooms'] = $this::reservHotelRoom($resvId, $room['hotelroomsid'], $room['startdate'], $room['enddate'], $room['roomrate'], $room['cost']);
   	 	
   	 } //end foreach
   	 
   	 $noofrooms = sizeof($rooms);
   	 
   	 foreach($guests as $guest){
   	 
   	 	 $results['saveguests'] = $this::saveGuestData($resvId, $guest['fname'], $guest['lname'], $guest['cellno'], $guest['landline'], $guest['email'], $guest['gender'], $guest['age'], $guest['street'], $guest['city'], $guest['county'], $guest['postcode'], $guest['country'], $guest['cstmRsvdRoms'], $guest['comments']);
   	    	 } //end foreach
   	    	 
   	    	 $cost = $rooms[0]['cost'];
   	    	 
   	    	 $results['savereservation'] = $this::saveReservationData($resvId, $reservation['startdate'], $reservation['enddate'], $reservationduration, $noofrooms, $reservation['startdate'], $reservation['enddate'], $reservation['totaladults'], $reservation['totalchilds'], $reservation['totalinfants'], $cost);
   	    
   	    	 
   	    	  $billing = array();
   	    	  $billing['roomcost'] = $cost;
   	    	  $billing['tax'] = "0";
   	    	  $billing['otherchrgs'] = "0";
   	    	  $billing['subtotal'] = $cost;
   	    	  $billing['rebateperc'] = "0";
   	    	  $billing['rebateamount'] = "0";
   	    	  $billing['grandtotal'] = $cost;
   	    	  $billing['method'] = "Credit Card";
   	    	  $billing['paymentpaid'] = $cost;
   	    	  $billing['balance'] = "0";
   	    	  $billing['rsvduration'] = $durforfin;
   
   	
   	 $results['savebilling'] = $this::saveBillingData($resvId, $billing['roomcost'], $billing['tax'], $billing['otherchrgs'], $billing['subtotal'], $billing['rebateperc'], $billing['rebateamount'], $billing['grandtotal'], $billing['method'], $billing['paymentpaid'], $billing['balance'], $billing['rsvduration']);
    	
   	echo json_encode($results);
   	 //print_r($durforfin);
   } //end function
   
   function saveReservationData($resvid, $stDate, $enDate, $duration, $noofrooms, $arrivaldate, $departdate, $totaladults, $totalchilds, $totalinfants, $totalcost)
   {
   	/*$resvid = $this->input->post('resvid');
   	 $stDate = $this->input->post('startdate');
   	 $enDate = $this->input->post('enddate');
   	 $duration = $this->input->post('duration');
   	 $noofrooms = $this->input->post('noofrooms');
   	 $arrivaldate = $this->input->post('arrivaldate');
   	 $departdate = $this->input->post('departdate');
   	 $totaladults = $this->input->post('totaladults');
   	 $totalchilds = $this->input->post('totalchilds');
   	 $totalcost = $this->input->post('totalcost');*/
   	 
   	/*$db = $this->session->userdata('db');
   	$userid = $this->session->userdata('id');*/
   	
   	$db = "hmsfrazdb";
   	$userid = "public";
   	 
   	$this->load->model('hotelreservation');
   	$result = $this->hotelreservation->updateReservationInfo($resvid, $stDate, $enDate, $duration, $noofrooms, $arrivaldate, $departdate, $totaladults, $totalchilds, $totalinfants, $totalcost, $userid, $db);
   	 
   	return $result;
   	//echo json_encode($result);
   	 
   } //end function
   
   function createRsrvNo($branchid)
   {
   	//$branchid = $this->input->post('branchid');
   	//$branchid = 1;
   	//echo json_encode($branchid);
   	//$db = $this->session->userdata('db');
   
   	$db = "hmsfrazdb";
   	$this->load->model('hotelreservation');
   	$rsvrid = $this->hotelreservation->createReservation($branchid, $db);
   
   	if ($rsvrid) {
   
   		$this->load->model('hotelbrancheshasrsvr');
   		$result = $this->hotelbrancheshasrsvr->saveReservationsOfBranches($branchid, $rsvrid, $db);
   		 return $rsvrid;
   		//echo json_encode($rsvrid) ;
   	} //end if
   	 
   	//echo json_encode($rsvrid);
   	 
   } //end function
   
   function insertRsvIdInSess()
   {
   	$resvId = $this->input->post('resvId');
   	 
   	$this->session->set_userdata(array('resvID'=>$resvId));
   	 
   	return true;
   	//echo json_encode(true);
   	 
   } //end function
   
   function reservHotelRoom($rsvid, $roomid, $startdate, $enddate, $roomrate, $roomcost)
   {
   	/*$rsvid = $this->input->post('reservationid');
   	$roomid = $this->input->post('selectedRooms');
   	$startdate = $this->input->post('startdate');
   	$enddate = $this->input->post('enddate');*/
   
   	//$db = $this->session->userdata('db');
   	//$userid = $this->session->userdata('id');
   
   	$db = "hmsfrazdb";
   	$userid = 'public';
   	
   	//rooms status is updated at reserved rooms table
   	$status = 'reserved';
   
   	$this->load->model('reservedrooms');
   	$rsvdroms = $this->reservedrooms->saveReservedRoom($rsvid, $roomid, $status, $startdate, $enddate, $roomrate, $roomcost, $userid, $db);
   
   	return $rsvdroms; 
   	//echo json_encode(array('data' => $rsvdroms));
   
   } //end function
   
   function saveGuestData($resvid, $fname, $lname, $cellno, $landline, $email, $gender, $age, $street, $city, $county, $postcode, $country, $cstmRsvdRoms, $comments)
   {
   	//fname, lname, cnic, passport, cellno, landline, email, gender, age, ethnicity, city, district, province, country, cstmRsvdRoms, isFamilyMember
  /* 	$resvid = $this->input->post('resvid');
   	$fname = $this->input->post('fname');
   	$lname = $this->input->post('lname');
   	$cnic = $this->input->post('cnic');
   	$passport = $this->input->post('passport');
   	$cellno = $this->input->post('cellno');
   	$landline = $this->input->post('landline');
   	$email = $this->input->post('email');
   	$gender = $this->input->post('gender');
   	$age = $this->input->post('age');
   	$ethnicity = $this->input->post('ethnicity');
   	$city = $this->input->post('city');
   	$district = $this->input->post('district');
   	$province = $this->input->post('province');
   	$country = $this->input->post('country');
   	$cstmRsvdRoms = $this->input->post('cstmRsvdRoms');
   	$isFamilyMember = $this->input->post('isFamilyMember');*/
   	$sesprntid;
   
   
   	//this shall be populated after first entry of parent guest is made
   	$sesprntid = $this->session->userdata('parentgstid');
   	//$sesprntid = 1;
   
   	//$db = $this->session->userdata('db');
   	//$userid = $this->session->userdata('id');
   	$db = "hmsfrazdb";
   	$userid = "public";
   	
   	$this->load->model('guesthasreservations');
   	$parntRsvtions = $this->guesthasreservations->checkParentGuestReservation($resvid, $db);
   	$result;
   	//if first entry of guest is made then he is parent and rest should be child guests
   	if(sizeof($parntRsvtions) > 0)
   	{
   		 
   		$isFamilyMember = 1;
   	} else {
   
   		$isFamilyMember = 0;
   	}
   
   	if($isFamilyMember == 0)
   	{
   		$this->load->model('guests');
   		$parentid = $this->guests->saveParentGuestData($fname, $lname, $cellno, $landline, $email, $gender, $age, $street, $city, $county, $postcode, $country, $isFamilyMember, addslashes($comments), $userid, $db);
   			
   		$this->load->model('guesthasreservations');
   		$gsthsrsv = $this->guesthasreservations->saveGuestReservation($resvid, $parentid, $userid, $db);
   			
   		$this->load->model('guestsstays');
   		$gststayid = $this->guestsstays->saveGuestRoomNo($cstmRsvdRoms, $userid, $db);
   			
   		$this->load->model('guestshasstays');
   		$result = $this->guestshasstays->saveGuestStayRecord($parentid, $gststayid, $resvid, $userid, $db);
   			
   	} else{
   		$this->load->model('guests');
   		$childid = $this->guests->saveChildGuestData($sesprntid, $resvid, $fname, $lname, $cellno, $landline, $email, $gender, $age, $street, $city, $county, $postcode, $country, $isFamilyMember, addslashes($comments), $userid, $db);
   			
   		$this->load->model('guesthasreservations');
   		$gsthsrsv = $this->guesthasreservations->saveGuestReservation($resvid, $childid, $userid, $db);
   			
   		$this->load->model('guestsstays');
   		$gststayid = $this->guestsstays->saveGuestRoomNo($cstmRsvdRoms, $userid, $db);
   
   		$this->load->model('guestshasstays');
   		$result = $this->guestshasstays->saveGuestStayRecord($childid, $gststayid, $resvid, $userid, $db);
   			
   	}
   
   
   return true;
   	//echo json_encode(array('data' => $result));
   
   } //end function

   
   
   function saveBillingData($resvid, $roomcost, $tax, $otherchrgs, $subtotal, $rebateperc, $rebateamount, $grandtotal, $method, $paymentpaid, $balance, $rsvduration) {
   	//resvid, roomcost, tax, otherchrgs, subtotal, rebateperc, rebateamount, grandtotal, method, paymentpaid, balance
   	/*$resvid = $this->input->post('resvid');
   	$roomcost = $this->input->post('roomcost');
   	$tax = $this->input->post('tax');
   	$otherchrgs = $this->input->post('otherchrgs');
   	//$subtotal = $this->input->post('subtotal');
   
   	$rebateperc = $this->input->post('rebateperc');
   	$rebateamount = $this->input->post('rebateamount');
   
   	
   	$method = $this->input->post('method');
   	$paymentpaid = $this->input->post('paymentpaid');*/
   	
   	/*$subtotal = $roomcost + $tax + $otherchrgs;
   	$grandtotal = $subtotal - $rebateamount;
   	$balance = $grandtotal - $paymentpaid;*/
   	 
   	/*$db = $this->session->userdata('db');
   	$userid = $this->session->userdata('id');*/
   	
   	$db = "hmsfrazdb";
   	$userid = "public";
   	 
   	$this->load->model('guestbill');
   	$result = $this->guestbill->saveBillingInfo($resvid, $roomcost, $tax, $otherchrgs, $subtotal, $rebateperc, $rebateamount, $grandtotal, $method, $paymentpaid, $balance, $rsvduration, $userid, $db);
   	 
   	return $result;
   	//echo json_encode($result);
   	 
   } //end function
   
   
} //end class


