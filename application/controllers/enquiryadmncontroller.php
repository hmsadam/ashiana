<?php

class enquiryadmncontroller extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
	

    if( !$this->session->userdata('isLoggedIn') ) {
        redirect('/login/show_login');
    }
  }

	function getenquiries()
	{
		$branchid = $this->input->post('branchid');
		//$branchid = 1;
		$db = $this->session->userdata('db');
		
		$this->load->model('enquiry');
	$enquiries = $this->enquiry->getenquiries($branchid, $db);
	echo json_encode(array('data' => $enquiries));
	}
  
  function enquiryadmnview() {
    
	$this->load->model('hotelbranches');
	$branches = $this->hotelbranches->getHotelsByUser();
		
	$user_id = $this->session->userdata('id');
	$db = $this->session->userdata('db');
	
	$data['branches'] = $branches;
    $data['username'] = $this->session->userdata('name');
   ///log_message('info', 'The purpose of some variable is to provide some value.');
	$this->load->helper('form');
    $this->load->view('enquiryadminview',$data);
  }

    
  
  
  function deleteenquiries()
  {
  	$id = $this->input->post('id');
  	//$branchid = 1;
  	$db = $this->session->userdata('db');
  
  	$this->load->model('enquiry');
  	$result = $this->enquiry->deleteenquiry($id, $db);
  	echo json_encode($result);
  }
      
  
  
  
} //end class
