<?php include("header.php")?>
<?php include("menu.php")?>

<script src="<?php echo base_url();?>assets/js/jquery-ui.js"></script>
  
  <link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery-ui.css" />



<script type="text/javascript">
						var allromsnames = <?php echo json_encode($rooms) ?>;
						
</script>

<body class="nav-md">




 

      
      <!-- page content -->
      <div class="right_col"  role="main">
        <div class="">

          <div class="page-title">
            <div class="title_left">
              <h2>Calendar</h2>
            </div>
            
          </div>
         <div class="clearfix"></div>
          <!-- start of data table -->
          <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title bor0">
                <div class="">
                  <h3 class="head ">Select Month/Year &amp; Room</h3>
                  
                  </div>
                  </div> <!-- end class x_title -->
                  <div class="clearfix"></div>
                
                	<form id="avlblgridForm" data-parsley-validate class="form-horizontal form-label-left">
					<div class="searcharea" style="padding-top:12px;">
                            <div class="form-group  col-md-3 col-sm-3 col-xs-12 padding-left-0">
                              
                              
                                <select id="monthgrd" class="form-control" required="required">
                               
                                <option value="1">January</option>
                                <option selected="selected" value="2">February</option>
                                <option value="3">March</option>
                                <option value="4">April</option>
                                <option value="5">May</option>
                                <option value="6">June</option>
                                <option value="7">July</option>
                                <option value="8">August</option>
                                <option value="9">September</option>
                                <option value="10" >October</option>
                                <option value="11" >November</option>
                                <option value="12">December</option>
                                
                                </select>
                              
                            </div>
                            <div class="form-group col-md-3 col-sm-3 col-xs-12 pl0">
                              
                                <select id="yrgrd" class="form-control" >
                                <option selected disabled >Year</option>
                                <option value="2018">2018</option>
                                <option  value="2019">2019</option>
                                <option selected value="2020">2020</option>
                                <option value="2021">2021</option>
                                <option value="2022">2022</option>
                                <option value="2023">2023</option>
                                <option value="2024">2024</option>
                                <option value="2025">2025</option>
                                <option value="2026">2026</option>
                                <option value="2027">2027</option>
                                <option value="2028">2028</option>
                                <option value="2029">2029</option>
                                <option value="2030">2030</option>
                                
                                
                                </select>
                            </div>
                            
                            <div class="form-group col-md-3 col-sm-3 col-xs-12 pl0">
                                <select id="rooms" class="form-control">
                                
                                <option value="all">All</option>
                                <?php foreach($rooms as $room) {?>
                                <option value="<?php echo $room['hotelroomsid']; ?>"><?php echo $room['roomno'];?></option>
                                <?php }?>
                                </select>
                            </div>
                            
                            <div class="form-group col-md-3 col-sm-3 col-xs-12 pl0">
                                <a href="javascript:void(0);" onClick="explodemonth()" id="save1" class="btn btn-success"> Search</a>
                            </div>
					</div>
                  </form>
                  <div class="row">
                  <div class="calendartop">
                      <div class="leftarrow col-md-4 col-sm-4 col-xs-2">
                        <a href="javascript:void(0);" onClick="previousmonth()" id="previousmonth" class="arrowpre"> </a>
                      </div>
                      <div class="yeardate col-md-4 col-sm-4 col-xs-8">
                        <div class="arrowpre" id="grdmnthyer"></div>
                      </div>
                      <div class="rightarrow col-md-4 col-sm-4 col-xs-2">
                        <a href="javascript:void(0);" onClick="nextmonth()" id="nextmonth" class="arrowfor"> </a>
                      </div>
                    </div>
                    </div>
                
                <!-- Grid divs start -->
                <div class="container">
                <div id="roomgrid" class="container"></div>
                
                </div>
                <!-- Grid divs end -->
                
                
                
              </div>
            </div>

            <br />
            <br />
            <br />

          </div>
		  
		  <!-- end of data table -->
		  
		  <div class="clearfix"></div>
		  
		  
		 

          <script type="text/javascript">
            $(document).ready(function() {
              
            });
          </script>


    </div>
  </div>

  <div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
  </div>

  <script src="<?php echo base_url();?>/assets/js/bootstrap.min.js"></script>

  <!-- bootstrap progress js -->
  <script src="<?php echo base_url();?>assets/js/progressbar/bootstrap-progressbar.min.js"></script>
  <script src="<?php echo base_url();?>assets/js/nicescroll/jquery.nicescroll.min.js"></script>
  <!-- icheck -->
  <script src="<?php echo base_url();?>assets/js/icheck/icheck.min.js"></script>
  <!-- tags -->
  <script src="<?php echo base_url();?>assets/js/tags/jquery.tagsinput.min.js"></script>
  <!-- switchery -->
  <script src="<?php echo base_url();?>assets/js/switchery/switchery.min.js"></script>
  <!-- daterangepicker -->
  <script type="text/javascript" src="<?php echo base_url();?>assets/js/moment/moment.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>assets/js/datepicker/daterangepicker.js"></script>
  <!-- richtext editor -->
  <script src="<?php echo base_url();?>assets/js/editor/bootstrap-wysiwyg.js"></script>
  <script src="<?php echo base_url();?>assets/js/editor/external/jquery.hotkeys.js"></script>
  <script src="<?php echo base_url();?>assets/js/editor/external/google-code-prettify/prettify.js"></script>
  <!-- select2 -->
  <script src="<?php echo base_url();?>assets/js/select/select2.full.js"></script>
  <!-- form validation -->
  <script type="text/javascript" src="<?php echo base_url();?>assets/js/parsley/parsley.min.js"></script>
  <!-- textarea resize -->
  <script src="<?php echo base_url();?>assets/js/textarea/autosize.min.js"></script>
  <script>
    autosize($('.resizable_textarea'));
  </script>
  <!-- Datatables -->
  <script src="<?php echo base_url();?>assets/js/datatables/js/jquery.dataTables.js"></script>
  <script src="<?php echo base_url();?>assets/js/datatables/tools/js/dataTables.tableTools.js"></script>

  
  <!-- pace -->
  <script src="<?php echo base_url();?>assets/js/pace/pace.min.js"></script>
  
  <script src="<?php echo base_url();?>assets/js/custom.js"></script>
  
  <!-- PNotify -->
  <script type="text/javascript" src="<?php echo base_url();?>assets/js/notify/pnotify.core.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>assets/js/notify/pnotify.buttons.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>assets/js/notify/pnotify.nonblock.js"></script>
<script src="<?php echo base_url();?>assets/js/hmsappscripts/customnotificationsscript.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/js/hmsappscripts/mainscript.js" type="text/javascript"></script> 
<script src="<?php echo base_url();?>assets/js/hmsappscripts/gridscript.js" type="text/javascript"></script> 
 
  
 
</body>

</html>
