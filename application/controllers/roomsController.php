<?php

class roomsController extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
	

    if( !$this->session->userdata('isLoggedIn') ) {
        redirect('/login/show_login');
    }
  }

	function getHotelRooms()
	{
		$branchid = $this->input->post('branchid');
		//$branchid = 1;
		$db = $this->session->userdata('db');
		
		$this->load->model('hotelrooms');
	$rooms = $this->hotelrooms->getRoomsByBranch($branchid, $db);
	echo json_encode(array('data' => $rooms));
	}
  
  function roomsShow() {
    
	$this->load->model('hotelbranches');
	$branches = $this->hotelbranches->getHotelsByUser();
		
	$user_id = $this->session->userdata('id');
	$db = $this->session->userdata('db');
	
	$data['branches'] = $branches;
    $data['username'] = $this->session->userdata('name');
   ///log_message('info', 'The purpose of some variable is to provide some value.');
	$this->load->helper('form');
    $this->load->view('roomsView',$data);
  }

    
  function createHotelRoom()
  {
	  $roomcategory = $this->input->post('roomcategory');
	  $hotelfloor = $this->input->post('hotelfloor');
	  $roomno = $this->input->post('roomno');
	  $roomstatus = $this->input->post('roomstatus');
	  $adultbeds = $this->input->post('adultbeds');
	  $childbeds = $this->input->post('childbeds');
	  $infantbeds = $this->input->post('infantbeds');
	  $portablebeds = $this->input->post('portablebeds');
	  	  
	  $branchid = $this->input->post('branchid');
	  $description = $this->input->post('description');
	  
	  $db = $this->session->userdata('db');
	  
	  $userid = $this->session->userdata('id');
	 
	 
	
	  $this->load->model('hotelrooms');
	  $result = $this->hotelrooms->createNewHotelRoom($roomcategory, $hotelfloor, $roomno, $roomstatus, $adultbeds,  $childbeds, $infantbeds, $portablebeds, $branchid, addslashes($description), $db, $userid);
	  
	 if($result == true)
	  {
		  //echo json_encode($result);
		  echo json_encode(true);
	  } else {
		  echo json_encode(false);
		  
	  }
	  
  }   //end function
      
  function updateRoom()
  {
	  $roomid = $this->input->post('roomid');
	  $catgid = $this->input->post('catgid');
	  $floorid = $this->input->post('floorid');
	  $roomno = $this->input->post('roomno');
	  $adultbeds = $this->input->post('adultbeds');
	  $childbeds = $this->input->post('childbeds');
	  $infantbeds = $this->input->post('infantbeds');
	  $portablebeds = $this->input->post('portablebeds');
	  $status = $this->input->post('status');
	  
	  $description = $this->input->post('description');
	  
	  /*$floorid = "1";
	  $floorname = "Ground Floor";
	  $desc = "10 Rooms on the floor";*/
	  
	  $userid = $this->session->userdata('id');
	  $db = $this->session->userdata('db');
	  
	  
	 
	  $this->load->model('hotelrooms');
	  $result = $this->hotelrooms->updateRoom($roomid, $catgid, $floorid, $roomno, $adultbeds, $childbeds, $infantbeds, $portablebeds, $status, addslashes($description), $userid, $db);
	  //log_message('info', "Floor Updated");
	  if($result == true)
	  {
		  //echo json_encode($result);
		  echo json_encode(true);
	  } else {
		  echo json_encode(false);
		  
	  }
	  
	  
  }   //end function
  
  //rooms category script
  function createRoomCatg()
  {
	  $catgname = $this->input->post('catgname');
	  $beds = $this->input->post('beds');
	  $desc = $this->input->post('desc');
	  $branchid = $this->input->post('branchid');
	  
	  $db = $this->session->userdata('db');
	  
	  $userid = $this->session->userdata('id');
	 
	 
	
	  $this->load->model('roomscategories');
	  $result = $this->roomscategories->createRoomCatg($catgname, $beds, $desc, $branchid, $db, $userid);
	  
	 if($result == true)
	  {
		  //echo json_encode($result);
		  echo json_encode(true);
	  } else {
		  echo json_encode(false);
		  
	  }
	  
  }   //end function
  
  function getRoomsCatg()
	{
		$branchid = $this->input->post('branchid');
		
		//$branchid = 3;
		$db = $this->session->userdata('db');
		
		$this->load->model('roomscategories');
	$floors = $this->roomscategories->getFloorByBranch($branchid, $db);
	echo json_encode(array('data' => $floors));
	}

	
	function updateRoomCatg()
  {
	  $catgid = $this->input->post('catgid');
	  $catgname = $this->input->post('catgname');
	  $beds = $this->input->post('beds');
	  $desc = $this->input->post('desc');
	  
	  /*$floorid = "1";
	  $floorname = "Ground Floor";
	  $desc = "10 Rooms on the floor";*/
	  
	  $userid = $this->session->userdata('id');
	  $db = $this->session->userdata('db');
	  
	  
	 
	  $this->load->model('roomscategories');
	  $result = $this->roomscategories->updateRoomCatg($catgid, $catgname, $beds, $desc, $userid, $db);
	  //log_message('info', "Floor Updated");
	  if($result == true)
	  {
		  //echo json_encode($result);
		  echo json_encode(true);
	  } else {
		  echo json_encode(false);
		  
	  }
	  
	  
  }   //end function
	
  function countNoOfRooms()
  {
  	$branchid = $this->input->post('branchid');
  
  	//$branchid = 1;
  	$db = $this->session->userdata('db');
  
  	$this->load->model('hotelrooms');
  	$floors = $this->hotelrooms->countNoOfRooms($branchid, $db);
  	echo json_encode($floors);
  } //end function
  
  function uploadroomimage()
  {
  	$result = false;
  	$data;
  	$picname;
  	$response;
  	
  	$db = $this->session->userdata('db');
  	$this->load->library('upload');
  	
  	$incomingpicname = $_FILES['image']['name'];
  	

  	$this->load->model('roomimages');
  	$allroomimages = $this->roomimages->getallimagenames($db);
  	
  	foreach($allroomimages as $image){
  		if($image['imagefilename'] == $incomingpicname) {
  			echo json_encode('image already uploaded');
  			exit;
  		}
  		
  	} //end foreach
  	 
  	if (isset($_FILES['image']) && !empty($_FILES['image']))
  	{
  		if ($_FILES['image']['error'] != 4)
  		{
  			// Image file configurations
  			$config['upload_path'] = './assets/images/';
  			$config['allowed_types'] = 'jpg|jpeg|png|gif';
  			$config['remove_spaces'] = false;
  			$config['max_size']             = 10000;
  			/*$config['max_width']            = 185;
  			$config['max_height']           = 300;*/
  			$this->upload->initialize($config);
  			$result = $this->upload->do_upload('image');
  			if($result)
  			{
  				$data = $this->upload->data(); //get data about uploaded image/file
  				$roomid = $this->session->userdata('roomid');
  				$userId = $this->session->userdata('id');
  				 
  				$picname = $data['file_name'];
  				 
  				 
  				$this->load->model('roomimages');
  				$isimagesaved = $this->roomimages->createimage($roomid, $picname, $db, $userId);
  				
  				$response = array(
  				 'picname' => $picname,
  				 'isimagesaved' => $isimagesaved
  				 );
  				echo json_encode($response);
  			} else {
  				$error = $this->upload->display_errors();
  				echo json_encode('file size issue');
  			}
  		}
  	} //end out if
  	
  } //end function
  
  function uploadroomvideo()
  {
  	$result = false;
  	$data;
  	$picname;
  	$response;
  	 $error;
  	$db = $this->session->userdata('db');
  	$this->load->library('upload');
  	 
  	$incomingvidname = $_FILES['romvid']['name'];
  	  
  	$this->load->model('hotelrooms');
  	$allroomvideos = $this->hotelrooms->getvideonames($db);
  	 
  	foreach($allroomvideos as $video){
  		if($video['roomvideo'] == $incomingvidname) {
  			$rslt = "Video already exists";
  			echo json_encode($rslt);
  			exit;
  		}
  
  	} //end foreach
  	
  	$roomid = $this->session->userdata('roomid');
  	
  	$this->load->model('hotelrooms');
  	$oldvidnames = $this->hotelrooms->getvideoname($roomid, $db);
  	 
  	$oldvidname = $oldvidnames[0]['roomvideo'];
  	 
  	if($oldvidname != ""){
  	
  		$path_to_file = './assets/images/'.$oldvidname.'';
  		$result = unlink($path_to_file);
  	}
  	
  	
  	if (isset($_FILES['romvid']) && !empty($_FILES['romvid']))
  	{
  		if ($_FILES['romvid']['error'] != 4)
  		{
  			// Image file configurations
  			$config['upload_path'] = './assets/images/';
  			$config['allowed_types'] = 'wmv|mp4|avi|mov';
  			$config['remove_spaces'] = false;
  			$config['max_size']             = 100000; //100mb
  			
  			$this->upload->initialize($config);
  			$result = $this->upload->do_upload('romvid');
  			if($result)
  			{
  				$data = $this->upload->data(); //get data about uploaded image/file
  				//$roomid = $this->session->userdata('roomid');
  				$userId = $this->session->userdata('id');
  				
  				$vidname = $data['file_name'];
  				$this->load->model('hotelrooms');
  				$isvidsaved = $this->hotelrooms->uploadvideopath($roomid, $vidname, $db);
  				$response = array(
  						'picname' => $vidname,
  						'isimagesaved' => $isvidsaved,
  						'income' => $incomingvidname
  				);
  				echo json_encode('file uploaded');
  			} else {
  				$error = $this->upload->display_errors();
  				echo json_encode('file size issue');
  			}
  		}
  	} //end out if
  	 
  } //end function
  
  function getrroomvideo()
  {
  	$roomid = $this->input->post('roomid');
  	$db = $this->session->userdata('db');
  	
  	//$roomid = 1;
  	$this->load->model('hotelrooms');
  	$vidname = $this->hotelrooms->getvideoname($roomid, $db);
  	
  	
  	echo json_encode($vidname['0']['roomvideo']);
  } //end function
  
  function videotestfunction(){
  	/*$incomingvidname = "Kab Talak";
  	$db = $this->session->userdata('db');
  	$this->load->model('hotelrooms');
  	$allroomvideos = $this->hotelrooms->getvideonames($db);
  	$incomingvidname = "abc";
  	foreach($allroomvideos as $video){
  		if($video['roomvideo'] == $incomingvidname) {
  			echo json_encode(false);
  			
  		} else {
  			echo json_encode(true);
  		}
  	
  	}*/
  	$db = $this->session->userdata('db');
  	$roomid = 1;
  	$this->load->model('hotelrooms');
  	$oldvidnames = $this->hotelrooms->getvideoname($roomid, $db);
  	
  	$oldvidname = $oldvidnames[0]['roomvideo'];
  	
  	if($oldvidname != ""){
  		
	  	$path_to_file = './assets/images/'.$oldvidname.'';
	  	$result = unlink($path_to_file);  	
  	}
  	
  	echo json_encode($oldvidnames[0]['roomvideo']);
  } //end function
  
  
  function insertromidinnsess()
  {
  	$roomid = $this->input->post('roomid');
  	 
  	$this->session->set_userdata(array('roomid'=>$roomid));
  	 
  	//return true;
  	echo json_encode(true);
  } //end function
  
  
  function getroomimages(){
  	
  	$roomid = $this->input->post('roomid');
  	$db = $this->session->userdata('db');
  	
  	//$roomid = 2;
  	 
  	$this->load->model('roomimages');
  	$allroomimages = $this->roomimages->getimagesbyroomid($roomid, $db);
  	
  	echo json_encode($allroomimages);
  	
  } //end function
  
  function deleteroomimage(){
  	 
  	$imageid = $this->input->post('imageid');
  	$imagename = $this->input->post('imagename');
  	$db = $this->session->userdata('db');
  	 
  	//$roomid = 2;
  
  	$this->load->model('roomimages');
  	$allroomimages = $this->roomimages->deleteroomimage($imageid, $db);
  	 
  	$path_to_file = './assets/images/'.$imagename.'';
  	unlink($path_to_file); //delete old pic
  	
  	echo json_encode(true);
  	 
  } //end function
  
    
  function saveroomrate()
  {
  	$roomid = $this->input->post('roomid');
  	$roomrate = $this->input->post('roomrate');
  	$ratefrom = $this->input->post('ratefrom');
  	$rateto = $this->input->post('rateto');
  	$db = $this->session->userdata('db');
  	$userid = $this->session->userdata('id');
  	
  	/*$roomid = "1";
  	$roomrate = "500";
  	$ratefrom = "2019-08-31 11:00:00";
  	$rateto = "2019-09-15 10:59:59";
  	 */
  	
  	$this->load->model('roomrates');
  	$allrates = $this->roomrates->getratesbyroomid($roomid, $db);
  	
  
  	$rangeexists = false;
  	foreach ($allrates as $key => $value)
  	{
  		if(!empty($value['ratefrom']) || !empty($value['rateto'])){
  			if((strtotime($ratefrom) >= strtotime($value['ratefrom'])) && (strtotime($ratefrom) <= strtotime($value['rateto'])))
  			{
  				$rangeexists = true; //no duplicate rate allowed
  			}
  		}
  	} //end foreach
  	
  	//echo json_encode($rangeexists);
  	 
  	if($rangeexists)
  	{
  		echo json_encode(false);
  		
  	} else {
  		
  		$this->load->model('roomrates');
  		$result = $this->roomrates->createrate($roomid, $roomrate, $ratefrom, $rateto, $db, $userid);  		
  		echo json_encode(true);  
  	}
  	 
  }   //end function
  
  function changeyear(){
  	
  	$year = "2019";
  	$db = $this->session->userdata('db');
  	
  	$this->load->model('roomrates');
  	$result = $this->roomrates->updateyear($year, $db);
  	echo json_encode($result);
  }
  
  function getroomrates(){
  	 
  	$roomid = $this->input->post('roomid');
  	$db = $this->session->userdata('db');
  	 
  	//$roomid = 2;
  
  	$this->load->model('roomrates');
  	$roomrates = $this->roomrates->getratesbyroomid($roomid, $db);
  	 
  	
  	echo json_encode(array('data' => $roomrates));
  } //end function
  
  function deleteroomrate(){
  
  	$rateid = $this->input->post('rateid');
  	
  	$db = $this->session->userdata('db');
  
  	//$roomid = 2;
  
  	$this->load->model('roomrates');
  	$allroomimages = $this->roomrates->deleteroomrate($rateid, $db);
  
  	 
  	echo json_encode(true);
  
  } //end function
	
	
	
} //end class
