<?php ?>
<?php include("header.php")?>
<?php include("menu.php")?>

  <script src="<?php echo base_url();?>assets/js/jquery-ui.js"></script>
  <link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css">  
  <link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery-ui.css">

<script src="<?php echo base_url();?>assets/js/hmsappscripts/tablescript.js" type="text/javascript"></script> 
<body class="nav-md">

      <!-- page content -->
      <div class="right_col" role="main">
        <div class="">
          <div class="page-title">
            <div class="title_left">
              <h3>
                    Invoice
                    <small>
                        Some examples to get you started
                    </small>
                </h3>
            </div>

            <div class="title_right">
              <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                  <input type="text" class="form-control" placeholder="Search for...">
                  <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Go!</button>
                        </span>
                </div>
              </div>
            </div>
          </div>
          <div class="clearfix"></div>

          <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Daily active users <small>Sessions</small></h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a href="#"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a href="#"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <table id="example" class="table table-striped responsive-utilities jambo_table">
                    <thead>
                      <tr class="headings">
                        
                        <th>Client Group</th>
                        <th>Owner</th>
                        <th>Cell</th>
                        <th>Database</th>
                        <th>Start Date</th>
                        <th>Service Type</th>
                        <th>Rate</th>
                        <th>Action</th>
                      </tr>
                    </thead>
				
						
						

					
                    					
					

                  </table>
                </div>
              </div>
            </div>

            <br />
            <br />
            <br />

          </div>
        </div>
		<!-- update client group start-->
		<div id="dialog-form" title="Update Client Information">
  <p class="validateTips">All form fields are required.</p>
 
  <form>
    <fieldset>
		<input type="hidden" id="groupId" value="W3Schools">
      <label for="groupName1">Group Name</label>
      <input type="text" name="groupName1" id="groupName1" value="Group Name" class="text ui-widget-content ui-corner-all">
      <label for="ownerName1">Owner Name</label>
      <input type="text" name="ownerName1" id="ownerName1" value="URL" class="text ui-widget-content ui-corner-all">
      <label for="cellNo1">Cell No.</label>
      <input type="text" name="cellNo1" id="cellNo1" value="Principal" class="text ui-widget-content ui-corner-all">
	  <label for="landLine1">Land Line No.</label>
      <input type="text" name="landLine1" id="landLine1" value="Administrator" class="text ui-widget-content ui-corner-all">
	  <label for="ntn1">NTN No.</label>
      <input type="text" name="ntn1" id="ntn1" value="Administrator" class="text ui-widget-content ui-corner-all">
	  <label for="cnic1">CNIC No.</label>
      <input type="text" name="cnic1" id="cnic1" value="Administrator" class="text ui-widget-content ui-corner-all">
	  <label for="address1">Address</label>
      <input type="text" name="address1" id="address1" value="Administrator" class="text ui-widget-content ui-corner-all">
	  <label for="dbName1">Database Name</label>
      <input type="text" name="dbName1" id="dbName1" value="Administrator" class="text ui-widget-content ui-corner-all">
	  <label for="startDate1">Start Date</label>
      <input type="text" name="startDate1" id="startDate1" value="Administrator" class="text ui-widget-content ui-corner-all">
	  <label for="endDate1">End Date</label>
      <input type="text" name="endDate1" id="endDate1" value="Administrator" class="text ui-widget-content ui-corner-all">
	  <label for="serviceType1">Service Type</label>
      <select class="large" id="serviceType1">
								<option>- Select Service Type - </option>
								<option value="Monthly">Monthly</option>
								<option value="Quarterly">Quarterly</option>
								<option value="halfYearly">Half Yearly</option>
								<option value="yearly">Yearly</option>
								</select>
	  
	  <label for="rate1">Rate</label>
      <input type="text" name="rate1" id="rate1" value="Administrator" class="text ui-widget-content ui-corner-all">
	  
 
      <!-- Allow form submission with keyboard without duplicating the dialog button -->
      
    </fieldset>
  </form>
</div>
		<!-- update client group end-->
		
        <!-- footer content -->
        <footer>
          <div class="copyright-info">
            <p class="pull-right">Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
            </p>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->

      </div>
      <!-- /page content -->
    </div>

  </div>

  <div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
  </div>

  <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>

  <!-- bootstrap progress js -->
  <script src="<?php echo base_url();?>assets/js/progressbar/bootstrap-progressbar.min.js"></script>
  <script src="<?php echo base_url();?>assets/js/nicescroll/jquery.nicescroll.min.js"></script>
  <!-- icheck -->
  <script src="<?php echo base_url();?>assets/js/icheck/icheck.min.js"></script>

  <script src="<?php echo base_url();?>assets/js/custom.js"></script>


  <!-- Datatables -->
  <script src="<?php echo base_url();?>assets/js/datatables/js/jquery.dataTables.js"></script>
  <script src="<?php echo base_url();?>assets/js/datatables/tools/js/dataTables.tableTools.js"></script>

  <!-- pace -->
  <script src="<?php echo base_url();?>assets/js/pace/pace.min.js"></script>
  <script>
        $(document).ready(function() {
      $('input.tableflat').iCheck({
        checkboxClass: 'icheckbox_flat-green',
        radioClass: 'iradio_flat-green'
      });
	  
	  
    });

    var asInitVals = new Array();
    $(document).ready(function() {
      
    });
  </script>
</body>

</html>
