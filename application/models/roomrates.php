<?php

class roomrates extends CI_Model {

  
	//this function is just for reference and not being used anywhere in the application
  
  function createrate($roomid, $rate, $from, $to, $db, $userid)
  {
	
    $data['roomrate'] = $rate;
    $data['ratefrom'] = $from;
    $data['rateto'] = $to;
    $data['hotelrooms_hotelroomsid'] = $roomid;
    
   $data['createdon'] = date("Y-m-d H:i:s" ,time());
    
    $data['createdby'] = $userid; 
    
	
	$this->$db->trans_start();	
	$this->$db->insert('roomrates',$data);
	$insert_id = $this->$db->insert_id();
	$this->$db->trans_complete();
	
	if ($this->$db->trans_status() === FALSE) {
    # Something went wrong.
    $this->$db->trans_rollback();
    return false;
	
	} 
	else {
		# Everything is Perfect. 
		# Committing data to the database.
		$this->$db->trans_commit();
		return true;
	}
  } //end function
  
  function updateyear($year, $db){
  	//UPDATE table_name SET date_field_name = CONCAT("2013", RIGHT(date_field_name,15));
  	
  	$query = $this->$db->query('UPDATE roomrates SET ratefrom = CONCAT("'.$year.'", RIGHT(ratefrom,15))');

  	$query = $this->$db->query('UPDATE roomrates SET rateto = CONCAT("'.$year.'", RIGHT(rateto,15))');
  	
  	return $query;
  	
  } //end function
  
  
  
  function getratesbyroomid($roomid, $db)
  {
	$this->$db->trans_start();
	$this->$db->select('roomratesid, roomrate, ratefrom, rateto, hotelrooms_hotelroomsid');
	$this->$db->from('roomrates');
	$this->$db->where('hotelrooms_hotelroomsid', $roomid);
    $roomimgs = $this->$db->get()->result_array();
	$this->$db->trans_complete();
	  return $roomimgs;
	  
  } //end function
  
  function getrngbsdromrate($ratefrom, $rateto, $db){
  	$this->$db->trans_start();
  	$this->$db->select('roomratesid, roomrate, ratefrom, rateto, hotelrooms_hotelroomsid');
  	$this->$db->from('roomrates');
  	//$this->$db->where('(t.endtime BETWEEN "' . $datein . '" AND "' . $dateout . '")');
  	 
  	//$this->$db->where('ratefrom >=', '(NOW() + INTERVAL 1 DAY) or rateto <= (NOW() + INTERVAL 2 DAY)', false);
  	
  	
  	//$this->$db->where('( "'. $age . ' BETWEEN ar.agevaluefrom AND ar.agevalueto ")');
  	//$this->$db->where('('.$ratefrom.'>= roomrates.ratefrom AND '.$rateto.' <= roomrates.rateto)');
  	//$this->$db->where('(ratefrom >= '.$ratefrom.' AND  rateto <= '.$rateto.'  )');
  	$this->$db->where('( "'. $ratefrom . '" BETWEEN ratefrom AND rateto  ) OR ( "'. $rateto . '" BETWEEN ratefrom AND rateto)');
  	$this->$db->where('(ratefrom BETWEEN "' . $ratefrom . '" AND "' . $rateto . '") OR (rateto BETWEEN "' . $ratefrom . '" AND "' . $rateto . '")');
  	
  	//$this->$db->where('hotelrooms_hotelroomsid', $roomid);
  	//$this->$db->where('roomrates.ratefrom >=', $ratefrom);
  	//$this->$db->where('roomrates.rateto <=', $rateto);
  	$rates = $this->$db->get()->result_array();
  	$this->$db->trans_complete();
  	return $rates;
  } //end function
  
  function deleteroomrate($id, $db)
  {
  	$this->$db->trans_start();
  	$this->$db->where('roomratesid', $id);
  	$this->$db->delete('roomrates');
  	$this->$db->trans_complete();
  	return true;
  		
  } //end function
  
  
  
 
  
} //end model class

