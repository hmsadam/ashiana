<?php

class labbranches extends CI_Model {

  
	//this function is just for reference and not being used anywhere in the application
  
  function createNewLabBranch($labname, $labtitle, $shortname, $address, $landline, $startdate, $groupid,  $username, $userid, $db)
  {
	$data['labname'] = $labname;
    $data['labtitle'] = $labtitle;
    $data['shortname'] = $shortname;
    $data['address'] = $address;
    $data['landlineno'] = $landline;
    $data['datecreated'] = date("Y-m-d H:i:s" ,time());
    $data['createdbyname'] = $username; 
    $data['createdbyuserid'] = $userid; 
    $data['clientgroups_clientgroupsid'] = $groupid; 
    $data['startdate'] = $startdate; 
	
	$this->$db->trans_start();	
	$this->$db->insert('labbranches',$data);
	$insert_id = $this->$db->insert_id();
	$this->$db->trans_complete();
	return $insert_id;
  } //end function
  
  function getAllBranches($db)
  {
		$this->$db->trans_start();
		$this->$db->from('labbranches');
		
      $branches = $this->$db->get()->result_array();
	  $this->$db->trans_complete();
	  return $branches;
	  
  } //end function
  
  function updateLabBranch($branchid, $labname, $labtitle, $shortname, $address, $landline, $startdate, $username, $userid, $db)
  {
	$data['labname'] = $labname;
    $data['labtitle'] = $labtitle;
    $data['shortname'] = $shortname;
    $data['address'] = $address;
    $data['landlineno'] = $landline;
    $data['dateupdated'] = date("Y-m-d H:i:s" ,time());
    $data['updatedbyname'] = $username; 
    $data['updatedbyuserid'] = $userid; 
    $data['startdate'] = $startdate; 
	
	$this->$db->trans_start();	
	$this->$db->where('labbranchid', $branchid);
	$this->$db->update('labbranches', $data);
	$this->$db->trans_complete();
	return true;
  } //end function
  
  function getLabsByUser()
  {
	  //this function is not updated for all controllers for multiple DBs
	  $db = $this->session->userdata('db');
	  $this->$db->trans_start();
		$this->$db->from('labbranches');
      $labs = $this->$db->get()->result_array();
		$this->$db->trans_complete();
		
		$this->load->model('usershaslabbranches');
		$labUsers = $this->usershaslabbranches->getLabUsers();
		$allLabs = array();
		foreach ($labUsers as $labIds) {
						//$variable = $schIds['school_school_id'];
						foreach ($labs as $lab) {
							if ($labIds['labbranches_labbranchid'] == $lab['labbranchid']) { 
								$allLabs[$lab['labbranchid']] = $lab['labname'];
							} //end if
							
						} // end inner foreach
						
					} //end outer foreach
					return $allLabs;
	  
  } //end function
  
  function getLabShortName($labid, $db)
  {
		$this->$db->trans_start();
		$this->$db->select('shortname');
		$this->$db->from('labbranches');
		$this->$db->where('labbranchid', $labid);
      $shortname = $this->$db->get()->result_array();
	  $this->$db->trans_complete();
	  return $shortname;
	  
  } //end function
  
  function getLabTitle($labid, $db)
  {
		$this->$db->trans_start();
		$this->$db->select('labTitle');
		$this->$db->from('labbranches');
		$this->$db->where('labbranchid', $labid);
      $title = $this->$db->get()->result_array();
	  $this->$db->trans_complete();
	  return $title;
	  
  }
  
  function getDBTwoUsers()
  {
		$labid = 1;
		//$dbname = "db2";
		$dbname = $this->session->userdata('db');
		$this->$dbname->select('shortname');
		$this->$dbname->from('labbranches');
		$this->$dbname->where('labbranchid', $labid);
     $shortname = $this->$dbname->get()->result_array();
	  return $shortname;
  } //end function
  
  function getDBIstUsers()
  {
		$labid = 1;
		$this->db1->select('shortname');
		$this->db1->from('labbranches');
		$this->db1->where('labbranchid', $labid);
      $shortname = $this->db1->get()->result_array();
	  return $shortname;
  } //end function
		
  
} //end model class
