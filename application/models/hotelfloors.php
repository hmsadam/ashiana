<?php

class hotelfloors extends CI_Model {

  
	//this function is just for reference and not being used anywhere in the application
  
  function createNewHotelFloor($floorname, $desc, $branchid, $db, $userid)
  {
	$data['floorname'] = $floorname;
    $data['description'] = $desc;
    $data['hotelbranches_hotelbranchesid'] = $branchid;
    
   $data['createdon'] = date("Y-m-d H:i:s" ,time());
    
    $data['createdby'] = $userid; 
    
	
	$this->$db->trans_start();	
	$this->$db->insert('hotelfloors',$data);
	$insert_id = $this->$db->insert_id();
	$this->$db->trans_complete();
	
	if ($this->$db->trans_status() === FALSE) {
    # Something went wrong.
    $this->$db->trans_rollback();
    return false;
	
	} 
	else {
		# Everything is Perfect. 
		# Committing data to the database.
		$this->$db->trans_commit();
		return true;
	}
  } //end function
  
  function getFloorByBranch($branchid, $db)
  {
	$this->$db->trans_start();
	$this->$db->from('hotelfloors');
	$this->$db->where('hotelbranches_hotelbranchesid', $branchid);
    $floors = $this->$db->get()->result_array();
	$this->$db->trans_complete();
	  return $floors;
	  
  } //end function
  
  function updateHotelFloor($floorid, $floorname, $desc, $userid, $db)
  {
	$data['floorname'] = $floorname;
    $data['description'] = $desc;
    $data['lastmodifiedon'] = date("Y-m-d H:i:s" ,time());
     
   $data['lastmodifiedby'] = $userid; 
    
	
	$this->$db->trans_start();	
	$this->$db->where('hotelfloorsid', $floorid);
	$this->$db->update('hotelfloors', $data);
	$this->$db->trans_complete();
	
	if ($this->$db->trans_status() === FALSE) {
    # Something went wrong.
    $this->$db->trans_rollback();
    return false;
	
	} 
	else {
		# Everything is Perfect. 
		# Committing data to the database.
		$this->$db->trans_commit();
		return true;
	}
	
  } //end function
  
 
  
} //end model class
