<?php

class avlblgridController extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
	

    if( !$this->session->userdata('isLoggedIn') ) {
        redirect('/login/show_login');
    }
  }

	function getroomrsrvtns()
	{
		$rooms = $this->input->post('rooms');
		$branchid = $this->input->post('branchid');
		$startdate = $this->input->post('startdate');
		$enddate = $this->input->post('enddate');
		
		//$db = $this->session->userdata('db');
		
		/*
		 $branchid = 1;
		 $status = "active";
		 $startdate = '2019-07-01';
		 $enddate = '2019-08-31';*/
		 //$rooms = 'all';
		 //$rooms = '1'; 
		$activeRsrvs = array();
		$db = $this->session->userdata('db');
		
		if($rooms == 'all'){
		$this->load->model('hotelrooms');
		$allRooms = $this->hotelrooms->getRoomsByBranchForReservation($branchid, $db);
	
		$this->load->model('reservedrooms');
		$rsrvdRooms = $this->reservedrooms->getRsvdRoomForGrid($branchid, $startdate, $enddate, $db);
		
			foreach($rsrvdRooms as $rsvroom){
				foreach ($allRooms as $room){
					if($room['hotelroomsid'] == $rsvroom['hotelrooms_hotelroomsid']){
						$activeRsrvs[] = $rsvroom; 
					}
				}	//end inner foreach	
			} //end foreach
		} else {
			$this->load->model('reservedrooms');
			$rsrvdRooms = $this->reservedrooms->getRsvdRoomForGrid($branchid, $startdate, $enddate, $db);
			foreach($rsrvdRooms as $rsvroom){
				if($rooms == $rsvroom['hotelrooms_hotelroomsid']){
					$activeRsrvs[] = $rsvroom;
				}
			}
		}//end if
		
		
		
		
	echo json_encode($activeRsrvs);
	}
  
  function availabilitgridshow() {
    
  	$db = $this->session->userdata('db');
  	$branchid = 1;
	$this->load->model('hotelbranches');
	$branches = $this->hotelbranches->getHotelsByUser();
		
	$this->load->model('hotelrooms');
	$rooms = $this->hotelrooms->getRoomsByBranch($branchid, $db);
	
	$user_id = $this->session->userdata('id');
	
	$data['rooms'] = $rooms;
	$data['branches'] = $branches;
    $data['username'] = $this->session->userdata('name');
  
	$this->load->helper('form');
    $this->load->view('avlbltygridiew',$data);
  }

    
  function createNewHotelFloor()
  {
	  $floorname = $this->input->post('floorname');
	  $desc = $this->input->post('desc');
	  $branchid = $this->input->post('branchid');
	  
	  $db = $this->session->userdata('db');
	  
	  $userid = $this->session->userdata('id');
	 
	 
	
	  $this->load->model('hotelfloors');
	  $result = $this->hotelfloors->createNewHotelFloor($floorname, $desc, $branchid, $db, $userid);
	  
	 if($result == true)
	  {
		  //echo json_encode($result);
		  echo json_encode(true);
	  } else {
		  echo json_encode(false);
		  
	  }
	  
  }   //end function
      
  function updateHotelFloor()
  {
	  $floorid = $this->input->post('floorid');
	  $floorname = $this->input->post('floorname');
	  $desc = $this->input->post('desc');
	  
	  /*$floorid = "1";
	  $floorname = "Ground Floor";
	  $desc = "10 Rooms on the floor";*/
	  
	  $userid = $this->session->userdata('id');
	  $db = $this->session->userdata('db');
	  
	  
	 
	  $this->load->model('hotelfloors');
	  $result = $this->hotelfloors->updateHotelFloor($floorid, $floorname, $desc, $userid, $db);
	  //log_message('info', "Floor Updated");
	  if($result == true)
	  {
		  //echo json_encode($result);
		  echo json_encode(true);
	  } else {
		  echo json_encode(false);
		  
	  }
	  
	  
  }   //end function
  
  
} //end class
